'use strict'

const { model: { User, Certificate } } = require('../../libs/mongodb')
const { getUpdateFields } = require('../admin/sanitizer')
const { upload } = require('../../libs/storage')
const certnet = require('../../contracts/certnet')
const { ZERO_ADDRESS, ROLES } = require('../../constants')

const updateIssuerInfo = async (req, res) => {
  const { id } = req.params
  const updateFields = getUpdateFields(req)
  try {
    await User.updateOne({
      _id: id
    }, updateFields)
    res.json({
      ok: true
    })
  } catch (ex) {
    res.json({
      ok: false,
      error: ex.message
    })
  }
}

const getAllIssuers = async (req, res) => {
  try {
    const allUsers = (await User.find({
      roles: ROLES.ISSUER
    })).map((user) => user.toJSONObj())
    res.json({
      ok: true,
      error: null,
      issuers: allUsers
    })
  } catch (ex) {
    res.json({
      ok: false,
      error: ex.message,
      issuers: []
    })
  }
}

const getIssuerById = async (req, res) => {
  try {
    const issuer = await User.findOne({
      _id: req.params.id,
      roles: ROLES.ISSUER
    })
    const issuerObj = issuer.toJSONObj()
    res.json({
      ok: true,
      error: null,
      issuer: issuerObj
    })
  } catch (ex) {
    res.json({
      ok: false,
      error: ex.message,
      issuer: []
    })
  }
}

const changeIssuerStatus = async (req, res) => {
  const { issuerId } = req.params
  const { active } = req.query || null
  try {
    await User.updateOne({
      _id: issuerId,
      roles: ROLES.ISSUER
    }, {
      $set: {
        issuableStatus: active === 'true' ? 1 : 0
      }
    })
    res.json({
      ok: true
    })
  } catch (ex) {
    res.json({
      ok: false,
      errorCode: 500,
      error: ex.message
    })
  }
}

const uploadImage = async (req, res) => {
  const { path } = req.body
  const file = req.file
  if (!file) {
    return res.json({
      ok: false,
      error: 'File not found'
    })
  }
  const { publicUrl } = await upload(file.buffer, path, {
    public: true
  })
  return res.json({
    ok: true,
    url: publicUrl
  })
}

const addEthereumAddress = async (req, res) => {
  const { issuerId } = req.params
  const { address } = req.body
  try {
    const issuer = await User.findOne({
      _id: issuerId,
      roles: ROLES.ISSUER
    })
    if (!issuer) {
      return res.json({
        ok: false,
        error: 'Issuer not found'
      })
    }
    const remoteData = await certnet.getInstance().methods.issuers(address).call()
    if (remoteData && remoteData.issuerAddress !== ZERO_ADDRESS) {
      if (remoteData.email === issuer.email) {
        await User.updateOne({
          _id: issuerId
        }, {
          $push: {
            ethereumAccounts: {
              address
            }
          }
        })
      }
    }
    return res.json({
      ok: true,
      error: null
    })
  } catch (ex) {
    return res.json({
      ok: false,
      error: ex.message
    })
  }
}

const removeEthereumAddress = async (req, res) => {
  const { issuerId } = req.params
  const { address } = req.body
  try {
    const issuer = await User.findOne({
      _id: issuerId,
      roles: ROLES.ISSUER
    })
    if (!issuer) {
      return res.json({
        ok: false,
        error: 'Issuer not found'
      })
    }
    // TODO: check if address is owned by another issuer and remove smartcontract logic
    const remoteData = await certnet.getInstance().methods.issuers(address).call()
    if (remoteData && remoteData.issuerAddress === ZERO_ADDRESS) {
      await User.updateOne({
        _id: issuerId
      }, {
        $pull: {
          ethereumAccounts: {
            address
          }
        }
      })
    }
    return res.json({
      ok: true,
      error: null
    })
  } catch (ex) {
    return res.json({
      ok: false,
      error: ex.message
    })
  }
}

const checkEthereumAddressNotUsed = async (req, res) => {
  const { address } = req.params
  try {
    const certs = await Certificate.find({
      'signature.signedBy': address,
      'metadata.revoked': false
    })
    if (certs.length > 0) {
      return res.json({
        ok: false,
        error: `Address ${address} is used to issue some certificates, please revoke these certificates first.`
      })
    }
    return res.json({
      ok: true,
      error: null
    })
  } catch (ex) {
    return res.json({
      ok: false,
      error: ex.message
    })
  }
}

module.exports = {
  updateIssuerInfo,
  getAllIssuers,
  getIssuerById,
  changeIssuerStatus,
  uploadImage,
  addEthereumAddress,
  removeEthereumAddress,
  checkEthereumAddressNotUsed
}

