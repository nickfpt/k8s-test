'use strict'

const _ = require('lodash')

const UPDATE_VALID_KEYS = ['fullname', 'contactEmail', 'address', 'website', 'taxAccount',
  'establishedDate', 'ownerName', 'phone', 'website', 'description', 'isActive', 'ethereumAccounts', 'logo', 'issuableStatus']

exports.getUpdateFields = (req) => _.pick(req.body, UPDATE_VALID_KEYS)

