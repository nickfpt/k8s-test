'use strict'

const _ = require('lodash')
const MerkleTools = require('merkle-tools')
const { computeHash } = require('../../utils/common')
const amqp = require('../../libs/amqp')
const { EMAIL_TO_RECIPIENT } = require('../../constants/templates')
const { CERTNET_EMAIL, EVENT, EVENT_OUTCOME, EVENT_IMPORTANCE } = require('../../constants')
const { model: { Certificate, User, Template, Batch } } = require('../../libs/mongodb')
const { logNewEvent } = require('../../controllers/eventLogger')

let createDrawVisualCertJob = () => {}
let createCertIssueMailJob = () => {}
amqp.on('ready', () => {
  amqp.exchange(amqp.EXCHANGE_NAME, {}, (exchange) => {
    createDrawVisualCertJob = async (issuerId, batchId, map, visualTemplateId) => {
      console.log(`Create job draw visual certificate for batch ${batchId}`)
      const issuer = await User.findById(issuerId)
      const batch = await Batch.findById(batchId)
      if (issuer && batch) {
        const { fullname, email } = issuer
        const { title } = batch
        exchange.publish(
          amqp.QUEUE.CREATE_VISUAL_CERTIFICATE,
          {
            issuerId,
            issuerName: fullname,
            issuerEmail: email,
            batchId,
            batchTitle: title,
            map,
            templateId: visualTemplateId
          }
        )
      }
    }
    createCertIssueMailJob = (recipient, recipientName, issuerName, certName, certId) => {
      console.log(`Create job send mail to ${recipient}`)
      exchange.publish(
        amqp.QUEUE.EMAIL_TO_RECIPIENT,
        {
          sender: CERTNET_EMAIL,
          recipient,
          subject: `Certificate from ${issuerName}`,
          content: EMAIL_TO_RECIPIENT(certName, issuerName, certId, recipientName)
        }
      )
    }
  })
})

const buildMerkleTreeFromCertificates = async (certificates) => {
  const merkleTools = new MerkleTools()
  const treeInfo = {
    proofs: {},
    hash: {}
  }
  certificates.forEach((cert) => {
    const hash = computeHash(cert)
    merkleTools.addLeaf(hash)
    treeInfo.hash[cert._id] = hash
  })
  merkleTools.makeTree()
  treeInfo.root = merkleTools.getMerkleRoot().toString('hex')
  certificates.forEach((cert, index) => {
    treeInfo.proofs[cert._id] = merkleTools.getProof(index)
  })
  return treeInfo
}

const signCertificatesWithMerkleData = async (batchId, proofs, hash, merkleRoot) => {
  const certificates = await Certificate.find({ 'metadata.batchId': batchId })
  const updateTasks = []
  await Batch.updateOne(
    { _id: batchId },
    {
      $set: {
        merkleRoot
      }
    }
  )
  certificates.forEach((cert) => {
    const id = cert._id
    const proof = proofs[id]
    const signature = {
      proof,
      merkleRoot,
      targetHash: hash[id]
    }
    updateTasks.push(Certificate.updateOne(
      { _id: id },
      {
        $set: { signature }
      }
    ))
  })
  await Promise.all(updateTasks)
}

const addTransactionId = async (batchId, transactionId, sender) => {
  const certificates = await Certificate.find({ 'metadata.batchId': batchId })
  const issueTime = new Date()
  await Batch.updateOne(
    { _id: batchId },
    {
      $set: {
        quantity: certificates.length,
        isSigned: true,
        issueTime
      }
    }
  )
  const updateTasks = []
  certificates.forEach((cert) => {
    const id = cert._id
    updateTasks.push(Certificate.updateOne(
      { _id: id },
      {
        $set: {
          'signature.transactionId': transactionId,
          'signature.signedBy': sender,
          'metadata.isSigned': true,
          'metadata.issueTime': issueTime,
          'metadata.revoked': false
        }
      }
    ))
  })
  await Promise.all(updateTasks)
}

module.exports.buildMerkleTree = async (req, res) => {
  const { batchId } = req.params
  const issuerId = req.user._id
  try {
    const certificates = await Certificate.find({ 'metadata.batchId': batchId, issuerId })
    if (certificates.length === 0) {
      return res.json({
        ok: false,
        error: 'Batch is empty'
      })
    }
    const { root: merkleTreeRoot, proofs, hash } = await buildMerkleTreeFromCertificates(certificates)
    await signCertificatesWithMerkleData(batchId, proofs, hash, merkleTreeRoot)
    return res.json({
      ok: true,
      error: null,
      merkleTreeRoot
    })
  } catch (ex) {
    return res.json({
      ok: false,
      error: ex
    })
  }
}

const sendEmailToRecipient = async (batchId) => {
  const fields = {
    'content.recipientInfo.email': 1,
    'content.recipientInfo.name': 1,
    'content.issuerInfo.name': 1,
    'content.certName': 1,
    _id: 1
  }
  try {
    const certificates = (await Certificate.find({ 'metadata.batchId': batchId }).select(fields)).map((cert) => cert.toObject())
    certificates.forEach((certificate) => {
      const recipient = certificate.content.recipientInfo.email
      const recipientName = certificate.content.recipientInfo.name
      const { certName } = certificate.content
      const issuerName = certificate.content.issuerInfo.name
      const certId = certificate._id
      createCertIssueMailJob(recipient, recipientName, issuerName, certName, certId)
    })
  } catch (ex) {
    console.error(ex)
  }
}

module.exports.signBatch = async (req, res) => {
  const {
    batchId, transactionId, sender, map, visualTemplateId
  } = req.body
  try {
    await addTransactionId(batchId, transactionId, sender)
    const batch = await Batch.findById(batchId)
    const { issuerId } = batch
    if (map && visualTemplateId) {
      createDrawVisualCertJob(issuerId, batchId, map, visualTemplateId)
    } else {
      sendEmailToRecipient(batchId)
    }
    res.json({
      ok: true,
      error: null
    })
    logNewEvent(EVENT.ISSUE, req.user._id, EVENT.ISSUE, `Batch ${batch.title}`, EVENT_OUTCOME.SUCCESS, `TxId: ${transactionId}`, EVENT_IMPORTANCE.HIGH)
  } catch (err) {
    res.json({
      ok: false,
      error: err
    })
    logNewEvent(EVENT.ISSUE, req.user._id, EVENT.ISSUE, 'Batch', EVENT_OUTCOME.FAIL, err.message, EVENT_IMPORTANCE.HIGH)
  }
}

module.exports.createUnsignedCerts = async (req, res) => {
  try {
    const issuerId = req.user._id
    const {
      batchId, templateId, header, data
    } = req.body
    const template = await Template.findById(templateId)
    const issuer = await User.findById(issuerId)
    if (!template) {
      return res.json({
        ok: false,
        error: 'Template not found'
      })
    }
    const templateContent = template.content
    const rawCerts = data.map((row) => _.zipObject(header, row))
    const unsignedCerts = rawCerts.map((cert) => {
      const unsignedCert = {
        issuerId,
        metadata: {
          batchId,
          isSigned: false,
          tags: []
        },
        content: {
          issuerInfo: {
            id: issuerId,
            logo: issuer.logo,
            name: issuer.fullname,
            ethereumAccounts: issuer.ethereumAccounts
          }
        }
      }
      Object.keys(cert).forEach((header) => {
        const path = templateContent.get(header)
        if (path) {
          _.set(unsignedCert, path, cert[header])
        }
      })
      return unsignedCert
    })
    await Certificate.insertMany(unsignedCerts)
    return res.json({
      ok: true,
      error: null
    })
  } catch (err) {
    return res.json({
      ok: false,
      error: err
    })
  }
}
