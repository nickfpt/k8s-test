'use strict'

const issueManagement = require('./issueManagement')
const templateManagement = require('./templateManagement')
const batchManagement = require('./batchManagement')
const requestManagement = require('./requestManagement')

module.exports = {
  issueManagement,
  templateManagement,
  batchManagement,
  requestManagement
}
