'use strict'

const isEmpty = require('lodash/isEmpty')
const { model: { Template } } = require('../../libs/mongodb')
const { logNewEvent } = require('../../controllers/eventLogger')
const { EVENT, EVENT_OUTCOME, EVENT_IMPORTANCE } = require('../../constants')

module.exports.createTemplate = async (req, res) => {
  const issuerId = req.user._id
  const { template } = req.body
  const { name } = template
  try {
    const templateWithName = await Template.findOne({ issuerId, name })
    if (templateWithName !== null) {
      res.json({
        ok: false,
        error: 'Template with this name already exists'
      })
    } else {
      const templateToInsert = new Template({
        ...template,
        issuerId
      })
      await templateToInsert.save()
      res.json({
        ok: true,
        error: null
      })
      logNewEvent(EVENT.CREATE_TEMPLATE, req.user._id, EVENT.CREATE_TEMPLATE, `Template ${name}`, EVENT_OUTCOME.SUCCESS, '', EVENT_IMPORTANCE.MEDIUM)
    }
  } catch (ex) {
    res.json({
      ok: false,
      error: ex
    })
  }
}

module.exports.listTemplateByIssuer = async (req, res) => {
  try {
    const issuerId = req.user._id
    const templates = await Template.find({ issuerId })
    res.json({
      ok: true,
      templates
    })
  } catch (ex) {
    res.json({
      ok: false,
      error: ex
    })
  }
}

module.exports.getTemplateById = async (req, res) => {
  try {
    const { templateId } = req.params
    const issuerId = req.user._id
    const templates = await Template.findOne({ issuerId, _id: templateId })
    res.json({
      ok: true,
      templates
    })
  } catch (ex) {
    res.json({
      ok: false,
      error: ex
    })
  }
}

module.exports.deleteTemplateById = async (req, res) => {
  const { templateId } = req.params
  const issuerId = req.user._id
  try {
    const template = await Template.findOneAndRemove({ issuerId, _id: templateId })
    logNewEvent(EVENT.DELETE_TEMPLATE, req.user._id, EVENT.DELETE_TEMPLATE, `Template ${template.name}`, EVENT_OUTCOME.SUCCESS, JSON.stringify(template), EVENT_IMPORTANCE.MEDIUM)
    res.json({
      ok: true,
      error: null
    })
  } catch (err) {
    res.json({
      ok: false,
      error: err
    })
  }
}

module.exports.updateTemplateById = async (req, res) => {
  const { templateId } = req.params
  const issuerId = req.user._id
  const { template } = req.body
  if (!template || isEmpty(template)) {
    return res.json({
      ok: false,
      error: 'template content is empty'
    })
  }
  if (template.issuerId) {
    return res.json({
      ok: false,
      error: 'Cannot update owner of the template'
    })
  }
  try {
    const { name } = template
    const currentTemplate = await Template.findById(templateId)
    if (currentTemplate.name !== name) {
      const templateWithName = await Template.findOne({ issuerId, name })
      if (templateWithName !== null) {
        return res.json({
          ok: false,
          error: 'Template with this name already exists'
        })
      }
      await Template.findOneAndUpdate({ _id: templateId, issuerId }, template)
      return res.json({
        ok: true,
        error: null
      })
    }
    await Template.findOneAndUpdate({ _id: templateId, issuerId }, template)
    return res.json({
      ok: true,
      error: null
    })
  } catch (err) {
    return res.json({
      ok: false,
      error: err
    })
  }
}
