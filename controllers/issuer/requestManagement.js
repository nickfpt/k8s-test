'use strict'

const _ = require('lodash')
const amqp = require('../../libs/amqp')
const { EMAIL_ACCEPT_TO_REQUESTER, EMAIL_REJECT_TO_REQUESTER } = require('../../constants/templates')
const { model: { BeIssuerRequest, User } } = require('../../libs/mongodb')
const { BECOME_ISSUER_STATUS, ROLES, CERTNET_EMAIL } = require('../../constants')

const REQUEST_BECOME_ISSUER_BODY_SCHEMA = {
  fullname: {
    notEmpty: {
      errorMessage: 'Must not be empty'
    },
    isLength: {
      options: [{ max: 50 }]
    }
  },
  description: {
    notEmpty: {
      errorMessage: 'Must not be empty'
    },
    isLength: {
      options: [{ max: 500 }],
      errorMessage: 'Too long. Maximum is 500 characters.'
    }
  },
  address: {
    notEmpty: {
      errorMessage: 'Must not be empty'
    }
  },
  phone: {
    notEmpty: {
      errorMessage: 'Must not be empty'
    }
  },
  contactEmail: {
    notEmpty: {
      errorMessage: 'Must not be empty'
    }
  },
  taxAccount: {
    notEmpty: {
      errorMessage: 'Must not be empty'
    }
  },
  establishedDate: {
    notEmpty: {
      errorMessage: 'Must not be empty'
    }
  },
  ownerName: {
    notEmpty: {
      errorMessage: 'Must not be empty'
    }
  },
  website: {
    notEmpty: {
      errorMessage: 'Must not be empty'
    }
  },
  ethereumAccount: {
    notEmpty: {
      errorMessage: 'Must not be empty'
    }
  }
}

let createAcceptRequestMailJob = () => {}
let createRejectRequestMailJob = () => {}
amqp.on('ready', () => {
  amqp.exchange(amqp.EXCHANGE_NAME, {}, (exchange) => {
    createAcceptRequestMailJob = (recipient, requesterName) => {
      exchange.publish(
        amqp.QUEUE.EMAIL_TO_RECIPIENT,
        {
          sender: CERTNET_EMAIL,
          recipient,
          subject: 'Request to become an Issuer approved',
          content: EMAIL_ACCEPT_TO_REQUESTER(requesterName)
        }
      )
    }
    createRejectRequestMailJob = (recipient, requesterName) => {
      exchange.publish(
        amqp.QUEUE.EMAIL_TO_RECIPIENT,
        {
          sender: CERTNET_EMAIL,
          recipient,
          subject: 'Request to become an Issuer rejected',
          content: EMAIL_REJECT_TO_REQUESTER(requesterName)
        }
      )
    }
  })
})

const getRequestToBeIssuer = async (user) => {
  const request = await BeIssuerRequest.findOne({
    issuerId: user._id,
    status: BECOME_ISSUER_STATUS.PENDING
  })
  return request
}

module.exports.sendRequestToBeIssuer = async (req, res) => {
  req.checkBody(REQUEST_BECOME_ISSUER_BODY_SCHEMA)
  const error = req.validationErrors()
  if (error) {
    const errors = {}
    error.forEach((err) => {
      errors[err.param] = {
        message: err.msg,
        message_code: `error.form_validation.${_.snakeCase(err.msg)}`
      }
    })
    res.json({
      ok: false,
      errorCode: 400,
      error: errors
    })
  }
  try {
    const request = await getRequestToBeIssuer(req.user)
    if (request) {
      res.status(401)
      res.json({
        ok: false,
        errorCode: 401,
        error: 'Request already exist'
      })
    } else {
      const newRequest = new BeIssuerRequest(Object.assign({}, {
        issuerId: req.user._id,
        requesterEmail: req.user.email,
        ...req.body,
        status: BECOME_ISSUER_STATUS.PENDING
      }))
      await newRequest.save()
      res.json({
        ok: true,
        requestInfo: {
          ...req.body,
          status: BECOME_ISSUER_STATUS.PENDING
        }
      })
    }
  } catch (ex) {
    res.json({
      ok: false,
      errorCode: 500,
      error: ex.message
    })
  }
}


module.exports.getRequestToBeIssuer = async (req, res) => {
  const { showAll } = req.query || null
  let requests = []
  try {
    if (showAll === 'true') {
      requests = await BeIssuerRequest.find({})
    } else {
      requests = await BeIssuerRequest.find({
        status: BECOME_ISSUER_STATUS.PENDING
      })
    }
    res.json({
      ok: true,
      requests
    })
  } catch (ex) {
    res.json({
      ok: false,
      errorCode: 500,
      error: ex.message
    })
  }
}

const INVALID_UPDATE_FIELDS = [
  'status',
  'issuerId',
  'requesterEmail'
]

module.exports.updateRequestToBeIssuer = async (req, res) => {
  try {
    const { requestId } = req.params
    const newRequest = Object.assign({}, {
      ...req.body
    })
    INVALID_UPDATE_FIELDS.forEach((field) => {
      delete newRequest[field]
    })
    const updatedRequest = await BeIssuerRequest.findOneAndUpdate({
      _id: requestId
    }, newRequest, {
      new: true
    })
    res.json({
      ok: true,
      updatedRequest
    })
  } catch (ex) {
    res.json({
      ok: false,
      errorCode: 500,
      error: ex.message
    })
  }
}

module.exports.acceptRequestToBeIssuer = async (req, res) => {
  try {
    const { requestId } = req.params
    const request = await BeIssuerRequest.findOne({
      _id: requestId,
      status: BECOME_ISSUER_STATUS.PENDING
    })
    if (!request) {
      res.json({
        ok: false,
        errorCode: 404,
        error: 'Request not found'
      })
    } else {
      const {
        fullname, description, address, phone, contactEmail, taxAccount, establishedDate, ownerName, website, logo, ethereumAccount
      } = request
      await User.findOneAndUpdate({
        _id: request.issuerId
      }, {
        $set: {
          fullname,
          description,
          address,
          phone,
          contactEmail,
          taxAccount,
          establishedDate,
          ownerName,
          website,
          logo,
          ethereumAccounts: [
            {
              address: ethereumAccount
            }
          ],
          issuableStatus: 1
        },
        $push: { roles: ROLES.ISSUER }
      })
      await BeIssuerRequest.findOneAndUpdate({
        _id: requestId
      }, {
        $set: {
          status: BECOME_ISSUER_STATUS.ACCEPT
        }
      })
      res.json({
        ok: true
      })
      createAcceptRequestMailJob(request.requesterEmail, request.fullname)
    }
  } catch (ex) {
    res.json({
      ok: false,
      errorCode: 500,
      error: ex.message
    })
  }
}

module.exports.rejectRequestToBeIssuer = async (req, res) => {
  try {
    const { requestId } = req.params
    const request = await BeIssuerRequest.findOne({
      _id: requestId,
      status: BECOME_ISSUER_STATUS.PENDING
    })
    if (!request) {
      res.json({
        ok: false,
        errorCode: 404,
        error: 'Request not found'
      })
    } else {
      await BeIssuerRequest.findOneAndUpdate({
        _id: requestId
      }, {
        $set: {
          status: BECOME_ISSUER_STATUS.REJECT
        }
      })
      res.json({
        ok: true
      })
      createRejectRequestMailJob(request.requesterEmail, request.fullname)
    }
  } catch (ex) {
    res.json({
      ok: false,
      errorCode: 500,
      error: ex.message
    })
  }
}
