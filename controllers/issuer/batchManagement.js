'use strict'

const amqp = require('../../libs/amqp')
const { model: { Batch, Certificate } } = require('../../libs/mongodb')
const certnetContract = require('../../contracts/certnet')
const { REVOCATION_EMAIL_TO_RECIPIENT } = require('../../constants/templates')
const { CERTNET_EMAIL } = require('../../constants')

let createCertRevocationMailJob = () => {}
amqp.on('ready', () => {
  amqp.exchange(amqp.EXCHANGE_NAME, {}, (exchange) => {
    createCertRevocationMailJob = (recipient, recipientName, issuerName, certName, certId) => {
      console.log(`Create job send revoke mail to ${recipient}`)
      exchange.publish(
        amqp.QUEUE.EMAIL_TO_RECIPIENT,
        {
          sender: CERTNET_EMAIL,
          recipient,
          subject: `Certificate Revocation from ${issuerName}`,
          content: REVOCATION_EMAIL_TO_RECIPIENT(certName, issuerName, certId, recipientName)
        }
      )
    }
  })
})

module.exports.createBatch = async (req, res) => {
  const issuerId = req.user._id
  const { batch } = req.body
  const { title } = batch
  const batchWithName = await Batch.findOne({ issuerId, title })
  if (batchWithName !== null) {
    if (batchWithName.isSigned) {
      res.json({
        ok: false,
        error: 'Batch with this title already exists'
      })
    } else {
      const batchToUpdate = {
        ...(batchWithName._doc),
        ...batch
      }
      await Batch.findOneAndUpdate({ _id: batchWithName._id, issuerId }, batchToUpdate)
      res.json({
        ok: true,
        batchId: batchWithName._id,
        error: null
      })
    }
  } else {
    const batchToInsert = new Batch({
      ...batch,
      issuerId,
      revoked: false
    })
    const newBatch = await batchToInsert.save()
    res.json({
      ok: true,
      batchId: newBatch._id,
      error: null
    })
  }
}

module.exports.listBatchByIssuer = async (req, res) => {
  try {
    const issuerId = req.user._id
    const batches = await Batch.find({ issuerId, isSigned: true })
    return res.json({
      ok: true,
      batches
    })
  } catch (ex) {
    return res.json({
      ok: false,
      error: ex.message
    })
  }
}

module.exports.listCertByBatchId = async (req, res) => {
  const { batchId } = req.params
  const issuerId = req.user._id
  const batch = await Batch.findById(batchId)
  if (!batch) {
    return res.json({
      ok: false,
      error: 'Batch not found'
    })
  }
  const certs = await Certificate.find({ issuerId, 'metadata.batchId': batchId, 'metadata.isSigned': true })
  return res.json({
    ok: true,
    error: null,
    certs,
    headers: batch.header
  })
}

const sendRevocationEmailBatch = async (batchId) => {
  const certificates = await Certificate.find({ 'metadata.batchId': batchId })
  certificates.forEach((certificate) => {
    certificate = certificate.toObject()
    const certId = certificate._id
    const { recipientInfo: { email, name } } = certificate.content
    const { issuerInfo: { name: issuerName } } = certificate.content
    const { certName } = certificate.content
    createCertRevocationMailJob(email, name, issuerName, certName, certId)
  })
}

module.exports.revokeBatch = async (req, res) => {
  const { batchId } = req.params
  try {
    const batch = await Batch.findById(batchId)
    if (batch.revoked === false) {
      const merkleRoot = batch.merkleRoot
      const batchDataOnEtherNetwork = await certnetContract.getInstance().methods.issuedBatches(`0x${merkleRoot}`).call()
      if (!batchDataOnEtherNetwork.revoked) {
        return res.json({
          ok: false,
          error: 'Batch is not revoked on CertNet smart contract'
        })
      }
      await Promise.all([
        Batch.updateOne({
          _id: batchId
        }, {
          $set: {
            revoked: true
          }
        }),
        Certificate.updateMany({
          'metadata.batchId': batchId
        }, {
          $set: {
            'metadata.revoked': true
          }
        })
      ])
      await sendRevocationEmailBatch(batchId)

      return res.json({
        ok: true,
        error: null
      })
    }
    return res.json({
      ok: false,
      error: 'Batch is already revoked'
    })
  } catch (ex) {
    return res.json({
      ok: false,
      error: ex.message
    })
  }
}

module.exports.revokeCert = async (req, res) => {
  const { certId } = req.params
  try {
    let certificate = await Certificate.findById(certId)
    if (!certificate) {
      return res.json({
        ok: false,
        error: 'Certificate not found'
      })
    }
    certificate = certificate.toObject()
    if (certificate.metadata.revoked === false) {
      const targetHash = `0x${certificate.signature.targetHash}`
      const certificateDataOnEtherNetwork = await certnetContract.getInstance().methods.revokedCertificates(targetHash).call()
      if (certificateDataOnEtherNetwork.certificateHash !== targetHash) {
        return res.json({
          ok: false,
          error: 'Certificate is not revoked on CertNet smart contract'
        })
      }
      await Certificate.updateOne({
        _id: certId
      }, {
        $set: {
          'metadata.revoked': true
        }
      })
      const { recipientInfo: { email, name } } = certificate.content
      const { issuerInfo: { name: issuerName } } = certificate.content
      const { certName } = certificate.content
      createCertRevocationMailJob(email, name, issuerName, certName, certId)

      return res.json({
        ok: true,
        error: null
      })
    }
    return res.json({
      ok: false,
      error: 'Certificate is already revoked'
    })
  } catch (ex) {
    console.error(ex)
    return res.json({
      ok: false,
      error: ex.message
    })
  }
}
