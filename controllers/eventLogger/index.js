'use strict'

const { EVENT_IMPORTANCE, EVENT_OUTCOME } = require('../../constants')
const { model: { Event } } = require('../../libs/mongodb')

const logNewEvent = async (title, actor, verb, object = null, outcome = EVENT_OUTCOME.SUCCESS, note = '', importance = EVENT_IMPORTANCE.LOW) => {
  try {
    const event = new Event({
      title,
      actor,
      verb,
      object,
      outcome,
      note,
      importance
    })
    await event.save()
  } catch (ex) {
    console.error(ex)
  }
}

const listEvents = async (req, res) => {
  const actorIndex = req.user._id
  try {
    const events = await Event.find({
      actor: actorIndex
    })
    res.json({
      ok: true,
      events
    })
  } catch (ex) {
    res.json({
      ok: true,
      error: ex.message
    })
  }
}

module.exports = {
  logNewEvent,
  listEvents
}

