'use strict'

const moment = require('moment')
const _ = require('lodash')
const UUID = require('uuid/v1')
const randomString = require('randomstring')
const redis = require('../../libs/redis')
const { model: { Certificate, User, Batch } } = require('../../libs/mongodb')
const { TOKEN_LENGTH, MAX_EXPIRED_DAY, INFINITY_DURATION } = require('../../constants')

async function generateUniqueToken () {
  let token = UUID()
  if (await redis.sismember(redis.SET.ACTIVE_TOKENS, token)) {
    token = randomString.generate({
      length: TOKEN_LENGTH,
      capitalization: 'lowercase'
    })
  }
  return token
}

async function generateVerifyLink (req, res) {
  const recipientId = req.user._id
  const { certList, duration } = req.body
  if (certList.length === 0) {
    res.json({
      ok: false,
      error: 'Certificate list is empty.'
    })
    return
  }
  try {
    const token = await generateUniqueToken()
    const experiedDay = duration || 7
    if (experiedDay > MAX_EXPIRED_DAY) {
      res.json({
        ok: false,
        error: 'Duration must be no more than 2 weeks.'
      })
      return
    }
    const getCertNameTasks = []
    certList.forEach((certId) => {
      getCertNameTasks.push((async () => {
        const cert = await Certificate.findById(certId)
        if (!cert) {
          return null
        }
        return cert.content.certName
      })())
    })
    const certNames = await Promise.all(getCertNameTasks)
    const expiredTime = moment().add(experiedDay, 'day').toDate()
    await redis.hmset(`${redis.HASH.VERIFICATION_TOKEN}:${token}`, {
      certList: JSON.stringify(certList),
      certNames: JSON.stringify(certNames),
      expiredTime,
      owner: recipientId,
      verificationTime: 0
    })
    await redis.sadd(`${redis.SET.USER}:${recipientId}`, token)
    await redis.sadd(redis.SET.ACTIVE_TOKENS, token)
    res.json({
      ok: true,
      token,
      certNames,
      expiredTime
    })
  } catch (ex) {
    res.json({
      ok: false,
      error: ex.message
    })
  }
}

async function generatePermanentToken (cert) {
  const token = await generateUniqueToken()
  const certList = [cert._id]
  const certNames = [cert.content.certName]
  const expiredTime = moment().add(INFINITY_DURATION, 'year').toDate()
  await redis.hmset(`${redis.HASH.VERIFICATION_TOKEN}:${token}`, {
    certList: JSON.stringify(certList),
    certNames: JSON.stringify(certNames),
    expiredTime,
    verificationTime: 0
  })
  return token
}

async function generateVerifyLinkForGuest (req, res) {
  const { certId, duration } = req.body
  const token = await generateUniqueToken()
  const experiedDay = duration || 7
  if (experiedDay > MAX_EXPIRED_DAY) {
    res.json({
      ok: false,
      error: 'Duration must be no more than 2 weeks.'
    })
    return
  }
  try {
    const cert = await Certificate.findById(certId)
    if (!cert) {
      res.json({
        ok: false,
        error: 'Certificate does not exist.'
      })
      return
    }
    const { certName } = cert.content
    const expiredTime = moment().add(experiedDay, 'day').toDate()
    await redis.hmset(`${redis.HASH.VERIFICATION_TOKEN}:${token}`, {
      certList: JSON.stringify([certId]),
      certNames: JSON.stringify([certName]),
      expiredTime
    })
    await redis.sadd(redis.SET.ACTIVE_TOKENS, token)
    res.json({
      ok: true,
      token,
      certName,
      expiredTime
    })
  } catch (ex) {
    res.json({
      ok: false,
      error: ex.message
    })
  }
}

async function getAllTokensByUser (req, res) {
  const recipientId = req.user._id
  const tokens = await redis.smembers(`${redis.SET.USER}:${recipientId}`)
  const pipeline = redis.pipeline()
  tokens.forEach((token) => {
    pipeline.hgetall(`${redis.HASH.VERIFICATION_TOKEN}:${token}`)
  })
  const tokenDetails = await pipeline.exec()
  tokenDetails.forEach(([, { certNames, expiredTime, verificationTime }], index) => {
    tokens[index] = {
      token: tokens[index],
      certNames: certNames ? JSON.parse(certNames) : [],
      expiredTime,
      verificationTime
    }
  })
  res.json({
    ok: true,
    tokens,
    error: null
  })
}

async function disableToken (req, res) {
  const { token } = req.params
  const recipientId = req.user._id
  if (!recipientId || !token) {
    res.json({
      ok: true,
      error: 'Missing recipientId or Token'
    })
  }
  await redis.del(`${redis.HASH.VERIFICATION_TOKEN}:${token}`)
  await redis.srem(`${redis.SET.USER}:${recipientId}`, token)
  await redis.srem(redis.SET.ACTIVE_TOKENS, token)
  res.json({
    ok: true,
    error: null
  })
}

async function getCertByEthereumAddress (req, res) {
  const { ethAdd } = req.params
  const recipientId = req.user._id
  try {
    const recipient = await User.findById(recipientId)
    if (!recipient) {
      res.json({
        ok: false,
        error: 'Recipient does not exist'
      })
    } else {
      const ethAccounts = recipient.ethereumAccounts
      const indexEthAccount = _.findIndex(ethAccounts, (o) => o.address === ethAdd)
      if (indexEthAccount === -1) {
        res.json({
          ok: false,
          error: 'User does not own the address'
        })
      }
      const certificates = await Certificate.find({ 'content.recipientInfo.ethereumAddress': ethAdd })
      res.json({
        ok: true,
        certificates,
        error: null
      })
    }
  } catch (err) {
    res.json({
      ok: false,
      error: err.message
    })
  }
}

async function getCertByUserId (req, res) {
  const recipientId = req.user._id
  try {
    const recipient = await User.findById(recipientId)
    if (!recipient) {
      res.json({
        ok: false,
        error: 'Recipient does not exist'
      })
    } else {
      const ethAccounts = recipient.recipientEthereumAccounts.toObject()
      const getCertTasks = []
      const getHeaderTasks = []
      ethAccounts.forEach((acc) => {
        const { address } = acc
        getCertTasks.push(Certificate.find({ 'content.recipientInfo.ethereumAddress': address, 'metadata.isSigned': true }))
      })
      let certificates = _.flatten(await Promise.all(getCertTasks))
      certificates.forEach((certificate) => {
        const { batchId } = certificate.metadata
        getHeaderTasks.push((async () => {
          const batch = await Batch.findById(batchId)
          const { header } = batch
          return header
        })())
      })
      const headers = await Promise.all(getHeaderTasks)
      certificates = certificates.map((cert, index) => ({
        ...(cert.toObject()),
        header: headers[index]
      }))
      res.json({
        ok: true,
        certificates,
        error: null
      })
    }
  } catch (err) {
    res.json({
      ok: false,
      error: err.message
    })
  }
}

async function getCertByCertId (req, res) {
  const { certId } = req.params
  try {
    const certificate = await Certificate.findById(certId)
    if (!certificate) {
      res.json({
        ok: false,
        error: 'Certificate not found'
      })
    } else {
      const { batchId } = certificate.metadata
      const batch = await Batch.findById(batchId)
      const { header } = batch
      res.json({
        ok: true,
        error: null,
        certificate: {
          ...(certificate.toObject()),
          header
        }
      })
    }
  } catch (err) {
    res.json({
      ok: false,
      error: err.message
    })
  }
}

async function updateEthereumAddresses (req, res) {
  const { addresses } = req.body
  const recipientId = req.user._id
  try {
    await User.updateOne({
      _id: recipientId
    }, {
      $set: {
        recipientEthereumAccounts: addresses
      }
    })
    res.json({
      ok: true
    })
  } catch (ex) {
    res.json({
      ok: false,
      error: ex
    })
  }
}

module.exports = {
  generateVerifyLink,
  generateVerifyLinkForGuest,
  getAllTokensByUser,
  disableToken,
  getCertByEthereumAddress,
  getCertByUserId,
  getCertByCertId,
  updateEthereumAddresses,
  generatePermanentToken
}

