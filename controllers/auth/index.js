'use strict'

const jwt = require('jsonwebtoken')
const _ = require('lodash')
const bcrypt = require('bcryptjs')
const UUID = require('uuid/v1')
const randomString = require('randomstring')
const { isEmail } = require('validator')
const amqp = require('../../libs/amqp')
const { EMAIL_TO_REGISTER, EMAIL_TO_RECOVER_PASSWORD } = require('../../constants/templates')
const { isPasswordSatisfyPolicy } = require('../../utils/common')
const { model: { User } } = require('../../libs/mongodb')
const config = require('../../config')
const { ROLES, NO_REPLY_EMAIL, HOSTNAME, EVENT, EVENT_OUTCOME, EVENT_IMPORTANCE, JWT_TIME_TO_LIVE } = require('../../constants')
const { logNewEvent } = require('../../controllers/eventLogger')
const { ACTIVATE_ACCOUNT_TEMPLATE } = require('../../constants/templates')

function generateAccessToken (user) {
  const payload = _.pick(user, ['_id', 'roles', 'ethereumAccounts', 'issuableStatus', 'email'])
  payload.ethereumAccounts = payload.ethereumAccounts.map((acc) => acc.address)
  const token = jwt.sign(payload, config.JWT_SECRET, {
    expiresIn: JWT_TIME_TO_LIVE
  })

  return {
    token,
    ttl: JWT_TIME_TO_LIVE
  }
}

const authErrorResponse = {
  ok: false,
  errorCode: 401,
  error: 'Invalid credentials'
}

let createActivateAccountMailJob = () => {}
let createRecoverPasswordMailJob = () => {}
amqp.on('ready', () => {
  amqp.exchange(amqp.EXCHANGE_NAME, {}, (exchange) => {
    createActivateAccountMailJob = (recipient, link) => {
      console.log(`Create job to send active link to user ${recipient}`)
      exchange.publish(
        amqp.QUEUE.EMAIL_TO_RECIPIENT,
        {
          sender: NO_REPLY_EMAIL,
          recipient,
          subject: 'Confirmation instructions',
          content: EMAIL_TO_REGISTER(link)
        }
      )
    }
    createRecoverPasswordMailJob = (recipient, newPassword) => {
      console.log(`Create recover password job to user ${recipient}`)
      exchange.publish(
        amqp.QUEUE.EMAIL_TO_RECIPIENT,
        {
          sender: NO_REPLY_EMAIL,
          recipient,
          subject: 'Recover password',
          content: EMAIL_TO_RECOVER_PASSWORD(newPassword)
        }
      )
    }
  })
})

const auth = {
  login: async (req, res) => {
    const email = req.body.email || ''
    const password = req.body.password || ''

    if (email === '' || password === '') {
      res.json(authErrorResponse)
      return
    }

    let validUser
    try {
      validUser = await auth.validateAndGetUser(email, password)
    } catch (ex) {
      res.status(500)
      res.json({
        ok: false,
        errorCode: 500,
        error: ex
      })
    }
    if (validUser && validUser.isActive) {
      logNewEvent(EVENT.LOGIN, validUser._id, EVENT.LOGIN, null, EVENT_OUTCOME.SUCCESS)
      res.json({
        ok: true,
        tokenData: generateAccessToken(validUser),
        userData: {
          id: validUser._id,
          roles: validUser.roles,
          logo: validUser.logo,
          issuable: validUser.issuableStatus === 1
        }
      })
    } else if (validUser && !validUser.isActive) {
      res.json({
        ok: false,
        errorCode: 402,
        error: 'Account is not activated.'
      })
    } else {
      logNewEvent(EVENT.LOGIN, null, EVENT.LOGIN, null, EVENT_OUTCOME.FAIL, 'Invalid credentials', '', EVENT_IMPORTANCE.HIGH)
      res.json(authErrorResponse)
    }
  },

  hashPassword: async (password) => {
    const saltRound = parseInt(config.PASSWORD_SALT_ROUND, 10)
    const hashedPassword = await bcrypt.hash(password, saltRound)
    return hashedPassword
  },

  register: async (req, res) => {
    const email = req.body.email || ''
    const password = req.body.password || ''

    const checkExistEmail = await User.findOne({ email })
    if (!isEmail(email) || checkExistEmail) {
      return res.json({
        ok: false,
        errorCode: 401,
        error: 'Invalid email'
      })
    }
    if (!isPasswordSatisfyPolicy(password)) {
      return res.json({
        ok: false,
        errorCode: 401,
        error: 'Password does not satisfy password policy'
      })
    }

    const hashedPassword = await auth.hashPassword(password)
    const activeToken = UUID()
    const newUser = new User({
      email,
      password: hashedPassword,
      roles: [ROLES.RECIPIENT],
      activeToken,
      isActive: false,
      acceptTokenAfter: new Date()
    })
    await newUser.save()
    createActivateAccountMailJob(email, `${HOSTNAME}/activate/${activeToken}`)
    return res.json({
      ok: true
    })
  },

  async validateAndGetUser (email, password) {
    try {
      const user = await User.findOne({ email })
      if (!user) {
        return null
      }
      const verifyPassword = await user.verifyPassword(password)
      if (verifyPassword) {
        return user
      }
    } catch (ex) {
      console.error(ex)
      return null
    }
    return null
  },

  activateAccount: async (req, res) => {
    const { token } = req.params
    try {
      const user = await User.findOne({ activeToken: token, isActive: false })
      if (!user) {
        res.set('Content-Type', 'text/html')
        return res.send(Buffer.from(ACTIVATE_ACCOUNT_TEMPLATE.INVALID_TOKEN))
      }
      await User.updateOne({ activeToken: token }, {
        $set: {
          isActive: true,
          acceptTokenAfter: new Date()
        }
      })
      res.set('Content-Type', 'text/html')
      return res.send(Buffer.from(ACTIVATE_ACCOUNT_TEMPLATE.REDIRECT))
    } catch (ex) {
      return res.json({
        ok: false,
        error: ex
      })
    }
  },

  recoverPassword: async (req, res) => {
    const { email } = req.params
    // TODO: Update logic to disable recover password of admin accounts.
    if (email === 'admin@gmail.com') {
      return res.json({
        ok: false,
        errorCode: 401,
        error: 'User not found'
      })
    }
    try {
      const user = await User.findOne({ email, isActive: true })
      if (user) {
        const newPassword = randomString.generate({
          length: 20
        })
        const hashedPassword = await auth.hashPassword(newPassword)
        await User.updateOne({
          email
        }, {
          $set: {
            password: hashedPassword
          }
        })
        createRecoverPasswordMailJob(email, newPassword)
        return res.json({
          ok: true,
          error: null
        })
      }
      return res.json({
        ok: false,
        errorCode: 401,
        error: 'User not found'
      })
    } catch (ex) {
      return res.json({
        ok: false,
        error: null
      })
    }
  },

  logout: async (req, res) => {
    const { _id } = req.user
    await User.updateOne({ _id }, {
      $set: {
        acceptTokenAfter: new Date()
      }
    })
    res.json({
      ok: true
    })
  },
  generateAccessToken
}

module.exports = auth
