'use strict'

const { model: { User } } = require('../../libs/mongodb')
const { ROLES } = require('../../constants')

const publicFields = [
  'contactEmail',
  'fullname',
  'establishedData',
  'ownerName',
  'phone',
  'taxAccount',
  'ethereumAccounts',
  'logo',
  'address',
  'website',
  'description'
].join(' ')

const getAllValidIssuers = async (req, res) => {
  const validIssuers = await User.find({
    issuableStatus: 1,
    roles: ROLES.ISSUER,
    isActive: true
  }, publicFields)
  res.json({
    ok: true,
    error: null,
    issuers: validIssuers
  })
}

const getIndividualValidIssuer = async (req, res) => {
  const { id } = req.params
  try {
    const validUsers = await User.findOne({
      _id: id,
      issuableStatus: 1,
      roles: ROLES.ISSUER,
      isActive: true
    }, publicFields)
    if (!validUsers) {
      return res.json({
        ok: false,
        error: 'Issuer not found'
      })
    }
    return res.json({
      ok: true,
      error: null,
      issuers: validUsers
    })
  } catch (ex) {
    return res.json({
      ok: false,
      error: ex.message
    })
  }
}

module.exports = {
  getAllValidIssuers,
  getIndividualValidIssuer
}
