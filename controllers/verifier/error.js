'use strict'

module.exports = class VerifierError extends Error {
  constructor (stepCode, message) {
    super()
    this.message = message
    this.stepCode = stepCode
  }
}
