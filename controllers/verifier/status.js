'use strict'

module.exports = {
  FAILURE: 'failure',
  STARTING: 'starting',
  SUCCESS: 'success'
}
