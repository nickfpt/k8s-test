/* eslint-disable class-methods-use-this */

'use strict'

const _ = require('lodash')
const MerkleTools = require('merkle-tools')
const { computeHash } = require('../../utils/common')
const { model: { User } } = require('../../libs/mongodb')
const certnet = require('../../contracts/certnet')
const { ZERO_HASH } = require('../../constants')
const STEPS = require('./steps')
const VerifierError = require('./error')
const VERIFICATION_STATUSES = require('./status')

function isTransactionIdValid (transactionId) {
  if (typeof transactionId === 'string' && transactionId.length > 0) {
    return transactionId
  }
  throw new VerifierError(
    STEPS.VALIDATE_TX_ID,
    'Cannot verify this certificate without a transaction ID to compare against.'
  )
}

function ensureHashesEqual (actual, expected) {
  if (actual !== expected) {
    throw new VerifierError(
      STEPS.COMPARE_HASH,
      'Computed hash does not match remote hash'
    )
  }
}

function ensureValidSignature (signature) {
  const merkleTools = new MerkleTools()
  const { targetHash, merkleRoot } = signature
  const proof = signature.proof.map((s) => {
    const step = s
    delete step._id
    return step
  })
  const result = merkleTools.validateProof(proof, targetHash, merkleRoot)
  if (result) return result
  throw new VerifierError(
    STEPS.VALIDATE_SIGNATURE,
    'Invalid Signature. Cannot form the Merkle root from the Target hash and Proof'
  )
}

function ensureAuthenticity (issuerProfile, issuerAddress, createdTimestamp) {
  if (issuerProfile.issuableStatus === 3) {
    throw new VerifierError(
      STEPS.CHECK_AUTHENTICITY,
      'Issuer is banned'
    )
  }
  const issueAccountIndex = _.findIndex(
    issuerProfile.ethereumAccounts,
    (acc) => acc.address.toLowerCase() === issuerAddress.toLowerCase()
  )

  if (issueAccountIndex === -1) {
    throw new VerifierError(
      STEPS.CHECK_AUTHENTICITY,
      'Issuer address does not match the transaction sender address'
    )
  }

  const ethereumAccountsValidDate = issuerProfile.ethereumAccounts[issueAccountIndex].importDate
  if (ethereumAccountsValidDate && ethereumAccountsValidDate.getTime() > createdTimestamp.getTime()) {
    throw new VerifierError(
      STEPS.CHECK_AUTHENTICITY,
      'Issuer\' ethereum account is not valid on issued day'
    )
  }
  return true
}

async function ensureNotRevoked (batchInfo, targetHash) {
  if (batchInfo.revoked) {
    throw new VerifierError(
      STEPS.CHECK_REVOCATION,
      'Certificate\'s batch was revoked'
    )
  }
  const revokedCertificate = await certnet.getInstance().methods.revokedCertificates(`0x${targetHash}`).call()
  if (revokedCertificate.certificateHash !== ZERO_HASH) {
    throw new VerifierError(
      STEPS.CHECK_REVOCATION,
      'Certificate was revoked'
    )
  }
}

async function getIssuerProfile (issuerId) {
  if (issuerId) {
    const issuer = await User.findById(issuerId)
    if (issuer) return issuer
  }
  throw new VerifierError(STEPS.GET_ISSUER_PROFILE, 'Unable to get issuer profile')
}

async function fetchIssuedBatch (merkleRoot) {
  try {
    return await certnet.getInstance().methods.issuedBatches(`0x${merkleRoot}`).call()
  } catch (ex) {
    throw new VerifierError(STEPS.FETCH_ISSUED_BATCH, 'Cannot fetch data from blockchain network')
  }
}

module.exports = class Verifier {

  constructor ({ certificate }) {
    this.issuerId = certificate.issuerId
    this.transactionId = certificate.signature.transactionId
    this.signature = certificate.signature
    const certToVerify = certificate
    certToVerify.metadata.isSigned = false
    delete certToVerify.metadata.issueTime
    delete certToVerify.signature
    this.certificate = certToVerify
    this._stepsStatuses = []
  }

  async verify () {
    await this._doAction(STEPS.VALIDATE_TX_ID, () => isTransactionIdValid(this.transactionId))
    // compute local hash
    const localHash = await this._doAction(STEPS.COMPUTE_LOCAL_HASH, () => computeHash(this.certificate))

    // fetch batch info from smart contract
    const batchInfo = await this._doAction(
      STEPS.FETCH_ISSUED_BATCH,
      () => fetchIssuedBatch(this.signature.merkleRoot)
    )
    // get issuer profile
    const issuerProfile = await this._doAction(
      STEPS.GET_ISSUER_PROFILE,
      () => getIssuerProfile(this.issuerId)
    )

    // ensure authenticity
    const createdTimestamp = new Date(parseInt(batchInfo.createdTimestamp, 10) * 1000)

    await this._doAction(
      STEPS.CHECK_AUTHENTICITY,
      () => ensureAuthenticity(issuerProfile, batchInfo.issuerAddress, createdTimestamp)
    )

    // ensure hash in signature is not altered
    await this._doAction(STEPS.COMPARE_HASH, () => ensureHashesEqual(localHash, this.signature.targetHash))

    // ensure valid signature
    await this._doAction(
      STEPS.VALIDATE_SIGNATURE,
      () => ensureValidSignature(this.signature)
    )

    // check revocation status
    await this._doAction(
      STEPS.CHECK_REVOCATION,
      () => ensureNotRevoked(batchInfo, this.signature.targetHash)
    )

    // check expery

    const erroredStep = this._stepsStatuses.find((step) => step.status === VERIFICATION_STATUSES.FAILURE)
    return erroredStep ? this._failed(erroredStep) : this._succeed()
  }

  async _doAction (step, action) {
    if (this._isFailing()) {
      return null
    }

    try {
      const result = await action()
      if (step) {
        this._stepsStatuses.push({ code: step, status: VERIFICATION_STATUSES.SUCCESS })
      }
      return result
    } catch (err) {
      if (step) {
        this._stepsStatuses.push({
          code: step,
          status: VERIFICATION_STATUSES.FAILURE,
          errorMessage: err.message
        })
        return null
      }
    }
    return null
  }

  _failed (erroredStep) {
    return {
      certId: this.certificate._id,
      code: STEPS.FINAL,
      status: VERIFICATION_STATUSES.FAILURE,
      errorMessage: erroredStep.errorMessage,
      erroredStep: erroredStep.code
    }
  }

  _isFailing () {
    return this._stepsStatuses.some((step) => step.status === VERIFICATION_STATUSES.FAILURE)
  }

  _succeed () {
    return { certId: this.certificate._id, code: STEPS.FINAL, status: VERIFICATION_STATUSES.SUCCESS }
  }
}

