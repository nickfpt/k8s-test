'use strict'

module.exports = {
  VALIDATE_TX_ID: 'validate_transaction_id',
  COMPUTE_LOCAL_HASH: 'compute_local_hash',
  FETCH_REMOTE_TX: 'fetch_remote_transaction',
  FETCH_ISSUED_BATCH: 'fetch_issued_batch',
  GET_ISSUER_PROFILE: 'get_issuer_profile',
  COMPARE_HASH: 'compare_hash',
  CHECK_MERKLE_ROOT: 'check_merkle_root',
  VALIDATE_SIGNATURE: 'validate_signature',
  CHECK_AUTHENTICITY: 'check_authenticity',
  CHECK_EXPIRATION: 'check_expiration',
  CHECK_REVOCATION: 'check_revocation',
  FINAL: 'final'
}
