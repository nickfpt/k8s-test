'use strict'

const { model: { Certificate, Batch } } = require('../../libs/mongodb')
const redis = require('../../libs/redis')
const Verifier = require('./verifier')

const verifyCertById = async (certificateId) => {
  const certificate = await Certificate.findById(certificateId)
  if (!certificate) {
    return {
      status: 'failure',
      code: 'Get certificate from DB',
      error: 'Certificate not found'
    }
  }
  const verifier = new Verifier({ certificate: certificate.toObject() })
  const { batchId } = certificate.metadata
  const [result, batch] = await Promise.all([verifier.verify(), Batch.findById(batchId)])
  const { header } = batch
  return {
    ...result,
    certificate: {
      ...certificate.toObject(),
      header
    }
  }
}

module.exports.verifyCertById = verifyCertById

module.exports.verifyToken = async (req, res) => {
  const { token } = req.params
  const dataToVerify = await redis.hgetall(`${redis.HASH.VERIFICATION_TOKEN}:${token}`)
  if (Object.keys(dataToVerify).length === 0) {
    res.json({
      valid: false,
      errorCode: 1,
      error: 'Token is invalid or expired'
    })
    return
  }
  await redis.hincrby(`${redis.HASH.VERIFICATION_TOKEN}:${token}`, 'verificationTime', 1)
  const certList = JSON.parse(dataToVerify.certList)
  const verificationTasks = []
  certList.forEach((certId) => {
    verificationTasks.push(verifyCertById(certId))
  })
  const results = await Promise.all(verificationTasks)
  const valid = results.every((result) => result.status === 'success')
  res.json({
    valid,
    error: null,
    errorCode: null,
    results
  })
}
