'use strict'

const { model: { User, BeIssuerRequest } } = require('../../libs/mongodb')
const { BECOME_ISSUER_STATUS } = require('../../constants')
const { hashPassword } = require('../../controllers/auth')
const { isPasswordSatisfyPolicy } = require('../../utils/common')

const getCurrentUser = async (req, res) => {
  const { user } = req
  const request = await BeIssuerRequest.findOne({
    issuerId: user._id,
    status: BECOME_ISSUER_STATUS.PENDING
  })
  const requested = request !== null
  res.json({
    ...await user.toJSONObj(),
    requestToBeIssuer: requested
  })
}

const verifyPassword = async (id, password) => {
  try {
    const user = await User.findById(id)
    if (user) {
      return user.verifyPassword(password)
    }
    return false
  } catch (ex) {
    return false
  }
}

const changePassword = async (req, res) => {
  const oldPassword = req.body.oldPassword || ''
  const newPassword = req.body.newPassword || ''
  if (oldPassword === '' || newPassword === '') {
    return res.json({
      ok: false,
      errorCode: 422,
      error: 'Must provide old password and new password'
    })
  }

  if (oldPassword === newPassword) {
    return res.json({
      ok: false,
      errorCode: 422,
      error: 'New password can not be the same as the old one'
    })
  }

  try {
    if (await verifyPassword(req.user._id, oldPassword)) {
      if (!isPasswordSatisfyPolicy(newPassword)) {
        return res.json({
          ok: false,
          errorCode: 401,
          error: 'New password does not satisfy password policy'
        })
      }
      const newPasswordHashed = await hashPassword(newPassword)
      await User.updateOne({
        _id: req.user._id
      }, {
        password: newPasswordHashed
      })
      return res.json({
        ok: true
      })
    }
    return res.json({
      ok: false,
      errorCode: 401,
      error: 'Old password is not correct'
    })
  } catch (ex) {
    return res.json({
      ok: false,
      errorCode: 500,
      error: ex.message
    })
  }
}

module.exports = {
  getCurrentUser,
  changePassword,
  verifyPassword
}

