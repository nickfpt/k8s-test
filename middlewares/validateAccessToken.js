'use strict'

const jwt = require('jsonwebtoken')
const { model: { User } } = require('../libs/mongodb')
const config = require('../config')

async function validateTokenDataAndGetUser (tokenData) {
  try {
    const user = await User.findById(tokenData._id)
    if (user && user.acceptTokenAfter) {
      if (tokenData.iat * 1000 < user.acceptTokenAfter) {
        return null
      }
      return user
    }
    return null
  } catch (ex) {
    console.error(ex)
    return null
  }
}

module.exports = async (req, res, next) => {
  const token = (req.body && req.body.access_token)
    || (req.query && req.query.access_token)
    || req.headers['x-access-token']
    || req.headers['X-Access-Token']
  if (token) {
    try {
      jwt.verify(token, config.JWT_SECRET, async (err, decodedToken) => {
        if (err) {
          if (err.name === 'TokenExpiredError') {
            res.json({
              ok: false,
              errorCode: 440,
              error: 'Token Expired'
            })
          } else if (err.name === 'JsonWebTokenError') {
            res.json({
              ok: false,
              errorCode: 441,
              error: 'Invalid Token'
            })
          } else {
            res.status(500)
            res.json({
              ok: false,
              errorCode: 500,
              error: 'Oops something went wrong'
            })
          }
        } else {
          const user = await validateTokenDataAndGetUser(decodedToken)
          if (user) {
            if (!user.isActive) {
              res.json({
                ok: false,
                errorCode: 442,
                error: 'Account is not activated'
              })
            } else {
              req.user = user
              next()
            }
          } else {
            res.status(441)
            res.json({
              ok: false,
              errorCode: 441,
              error: 'Invalid user'
            })
          }
        }
      })
    } catch (ex) {
      res.json({
        ok: false,
        errorCode: 500,
        error: ex
      })
    }
  } else {
    res.json({
      ok: false,
      errorCode: 401,
      error: 'Invalid token'
    })
  }
}
