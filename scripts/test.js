/* eslint-disable */

'use strict'

// this file is for testing other functions purpose only

// const path = require('path')
// const _ = require('lodash')
// const XLSX = require('xlsx')
// const MerkleTools = require('merkle-tools')
// const { verifyCertById } = require('../controllers/verifier')
//
const mongoose = require('mongoose')
// const { model: { Template, User, Batch } } = require('../libs/mongodb')
const config = require('../config')
// const connectionString = `mongodb://${config.mongodb.host}:${config.mongodb.port}/certnet`
// mongoose.connect(connectionString, { useNewUrlParser: true })

const jwt = require('jsonwebtoken')
// const amqp = require('../libs/amqp')
const { sendEmail } = require('../libs/emailer')
const { sendEmailToRecipient } = require('../controllers/issuer/issueManagement')
const { EMAIL_TO_RECIPIENT } = require('../constants/templates')

const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms))

async function main () {
  // amqp.on('ready', () => {
  //   console.log('amqp ready')
  //   amqp.exchange(amqp.EXCHANGE_NAME, {}, (exchange) => {
  //     exchange.publish(
  //       amqp.QUEUE.EMAIL_TO_RECIPIENT,
  //       {
  //         sender: 'admin@fpt.edu.vn',
  //         recipient: 'nicksmd331995@gmail.com',
  //         subject: 'Certificate from FPT University',
  //         content: EMAIL_TO_RECIPIENT('Bachelor Degree', 'FPT University', '12381798791', 'Hoang Dinh Quang')
  //       }
  //     )
  //   })
  // })
  // console.log('start')
  // await wait(5000)
  // await sendEmailToRecipient('5be2549a854aae094542a091')
  // console.log('end')
  // const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YmUxMTAwMDVmZTMyOGFhNWRhNWExZDIiLCJpYXQiOjE1NDE0NzY5NTksImV4cCI6MTU0MTczNjE1OX0.dl2hQIb1J0PBLxiubhOdI9oc0mRwj43FQA1L9iFEArk'
  // const decodedToken = await jwt.verify(token, config.JWT_SECRET)
  // console.log({ decodedToken })
  const cert = '-----BEGIN EC PRIVATE KEY-----\n' +
    'MHcCAQEEINTjaaYTYxYS0cnqIYYjvdRD9UbnvxOhJ6tkGUt/dyj7oAoGCCqGSM49\n' +
    'AwEHoUQDQgAEqfLKadWYESjWIwJvj0TXyUJcyf8qDGMRPK0HlSS++ZLREFUQgiz1\n' +
    'JfrwvncIa1AfT+T6AZIDfNomAuIAc7gG2A==\n' +
    '-----END EC PRIVATE KEY-----'

  const publicKey = '-----BEGIN PUBLIC KEY-----\n' +
    'MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEqfLKadWYESjWIwJvj0TXyUJcyf8q\n' +
    'DGMRPK0HlSS++ZLREFUQgiz1JfrwvncIa1AfT+T6AZIDfNomAuIAc7gG2A==\n' +
    '-----END PUBLIC KEY-----'

  // jwt.sign({ foo: 'bar' }, cert, { algorithm: 'ES256' }, function(err, token) {
  //   console.log(err)
  //   console.log(token);
  // });
  var start = new Date().getTime();

  jwt.verify('eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJmb28iOiJiYXIiLCJpYXQiOjE1NDE5NTgxNjR9.4prTAstprs8T5wyZorWeN4qAVOD5qSvYKoReq9gr13C7irA6VHUqe82tQTbD2jiH46f6mK6-JA5TVNn6I3gwLw',
    publicKey, { algorithms: ['ES256'] }, function (err, payload) {
      console.log(payload)
    });

  var end = new Date().getTime();
  var time = end - start;
  console.log(time)
}


main()

