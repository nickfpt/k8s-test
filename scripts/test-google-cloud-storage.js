// Run this script with current directory = project directory

'use strict'

require('dotenv').config()
const gcs = require('../libs/storage')

async function uploadText () {
  try {
    const data = Buffer.from('Hello world!')
    const { file, publicUrl } = await gcs.upload(data, 'test.txt', {
      public: true
    })
    const [metadata] = await file.getMetadata()
    console.log('Successfully uploaded text!')
    console.log('File path:', metadata.name)
    console.log('Created time:', metadata.timeCreated)
    console.log('File size:', metadata.size, 'bytes')
    console.log('Public URL:', publicUrl)
  } catch (err) {
    console.error(err)
  }
}

async function uploadImage () {
  try {
    const { file, publicUrl } = await gcs.uploadImage('./scripts/test-image.jpg', 'images/private-image.jpg', {
      crop: {
        x: 100,
        y: 100,
        width: 600,
        height: 800
      },
      resize: {
        width: 300,
        height: 400
      },
      public: false
    })
    const [metadata] = await file.getMetadata()
    console.log('Successfully uploaded image!')
    console.log('File path:', metadata.name)
    console.log('Created time:', metadata.timeCreated)
    console.log('File size:', metadata.size, 'bytes')
    console.log('Public URL:', publicUrl)
  } catch (err) {
    console.error(err)
  }
}

async function main () {
  await uploadText()
  await uploadImage()
}

main()
