'use strict'

const Jimp = require('jimp')
const pixelWidth = require('string-pixel-width')

const fileName = `${__dirname}/cert.png`
const newFileName = `${__dirname}/new_cert.png`
// const qrFile = `${__dirname}/i_love_qr.png`
const imageCaption = 'Barack Obama'
let loadedImage

const width = pixelWidth('Barack Obama', { size: 128 })
console.log(width)
const center = 1100

// const qr = require('qr-image')
//
// const qrImage = qr.imageSync('http://www.certnet.site/home/fa63rt00krfd3rk', {
//   size: 6,
//   margin: 3
// })
// Jimp.read(fileName)
//   .then(async (image) => {
//     loadedImage = image
//     const qr = await Jimp.read(qrImage)
//     await loadedImage.composite(qr, 290, 1170).write(newFileName)
//     console.log('done')
//   })
//   .catch((err) => {
//     console.error(err)
//   })

Jimp.read(fileName)
  .then((image) => {
    loadedImage = image
    return Jimp.loadFont(Jimp.FONT_SANS_128_BLACK)
  })
  .then((font) => {
    loadedImage.print(font, center - (width / 2), 600, imageCaption)
      .write(newFileName)
  })
  .catch((err) => {
    console.error(err)
  })

