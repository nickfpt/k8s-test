'use strict'

const mongodb = require('./components/mongodb')
const redis = require('./components/redis')
const common = require('./components/common')
const auth = require('./components/auth')
const amqp = require('./components/amqp')
const mailgun = require('./components/mailgun')
const googleCloudStorage = require('./components/google-cloud-storage')

module.exports = Object.assign({}, common, mongodb, redis, auth, amqp, googleCloudStorage, mailgun)
