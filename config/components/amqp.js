'use strict'

const joi = require('joi')

const envVarsSchema = joi.object({
  RABBITMQ_URI: joi.string()
    .uri({ scheme: 'amqp' }),
  RABBITMQ_HOST: joi.string(),
  RABBITMQ_USER: joi.string().default(''),
  RABBITMQ_PASSWORD: joi.string().default('')
}).xor('RABBITMQ_URI', 'RABBITMQ_HOST')
  .with('RABBITMQ_HOST', ['RABBITMQ_USER', 'RABBITMQ_PASSWORD'])
  .unknown()
  .required()

const { error, value: envVars } = joi.validate(process.env, envVarsSchema)
if (error) {
  throw new Error(`Config validation error: ${error.message}`)
}

const {
  RABBITMQ_URI, RABBITMQ_HOST, RABBITMQ_USER, RABBITMQ_PASSWORD
} = envVars

const config = {
  rabbitmq: {
    uri: RABBITMQ_URI || `amqp://${RABBITMQ_USER}${RABBITMQ_PASSWORD ? `:${RABBITMQ_PASSWORD}` : ''}@${RABBITMQ_HOST}`
  }
}

module.exports = config
