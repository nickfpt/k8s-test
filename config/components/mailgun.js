'use strict'

const { DEFAULT_API_KEY, DEFAULT_DOMAIN } = require('../../constants').MAILGUN

if (!process.env.MAILGUN_API_KEY) {
  process.env.MAILGUN_API_KEY = DEFAULT_API_KEY
}

if (!process.env.MAILGUN_DOMAIN) {
  process.env.MAILGUN_DOMAIN = DEFAULT_DOMAIN
}

const config = {
  mailgun: {
    apiKey: process.env.MAILGUN_API_KEY,
    domain: process.env.MAILGUN_DOMAIN
  }
}

module.exports = config
