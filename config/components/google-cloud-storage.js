'use strict'

const { DEFAULT_CREDENTIALS_FILE, DEFAULT_BUCKET_NAME } = require('../../constants').GOOGLE_CLOUD_STORAGE

if (!process.env.GOOGLE_APPLICATION_CREDENTIALS) {
  process.env.GOOGLE_APPLICATION_CREDENTIALS = DEFAULT_CREDENTIALS_FILE
}

if (!process.env.GOOGLE_CLOUD_STORAGE_BUCKET_NAME) {
  process.env.GOOGLE_CLOUD_STORAGE_BUCKET_NAME = DEFAULT_BUCKET_NAME
}

const config = {
  googleCloudStorage: {
    credentials: process.env.GOOGLE_APPLICATION_CREDENTIALS,
    bucketName: process.env.GOOGLE_CLOUD_STORAGE_BUCKET_NAME
  }
}

module.exports = config
