'use strict'

const joi = require('joi')

const envVarsSchema = joi.object({
  REDIS_URI: joi.string()
    .uri({ scheme: 'redis' }),
  REDIS_HOST: joi.string(),
  REDIS_PORT: joi.number().default(6379),
  REDIS_PASSWORD: joi.string().default(''),
  REDIS_DATA_RETENTION_IN_MS: joi.number()
    .default(86400000),
  REDIS_DB: joi.number().default(0)
}).xor('REDIS_URI', 'REDIS_HOST')
  .with('REDIS_HOST', ['REDIS_PORT', 'REDIS_PASSWORD'])
  .unknown()
  .required()

const { error, value: envVars } = joi.validate(process.env, envVarsSchema)
if (error) {
  throw new Error(`Config validation error: ${error.message}`)
}

const {
  REDIS_URI, REDIS_HOST, REDIS_PORT, REDIS_PASSWORD, REDIS_DB
} = envVars

const config = {
  redis: {
    uri: REDIS_URI || `redis://${REDIS_PASSWORD ? `:${REDIS_PASSWORD}` : ''}@${REDIS_HOST}:${REDIS_PORT}/${REDIS_DB}`,
    dataRetention: envVars.REDIS_DATA_RETENTION_IN_MS
  }
}

module.exports = config
