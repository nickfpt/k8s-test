'use strict'

const joi = require('joi')

const envVarsSchema = joi.object({
  MONGO_USER: joi.string()
    .default(''),
  MONGO_PASSWORD: joi.string()
    .default(''),
  MONGO_HOST: joi.string()
    .default('localhost'),
  MONGO_PORT: joi.string()
    .default('27017')
}).unknown()
  .required()

const { error, value: envVars } = joi.validate(process.env, envVarsSchema)
if (error) {
  throw new Error(`Config validation error: ${error.message}`)
}

const config = {
  mongodb: {
    user: envVars.MONGO_USER,
    password: envVars.MONGO_PASSWORD,
    host: envVars.MONGO_HOST,
    port: envVars.MONGO_PORT
  }
}

module.exports = config
