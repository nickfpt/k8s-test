'use strict'

const joi = require('joi')

const envVarsSchema = joi.object({
  JWT_SECRET: joi.string().required(),
  PASSWORD_SALT_ROUND: joi.string().required()
}).unknown()
  .required()

const { error, value: envVars } = joi.validate(process.env, envVarsSchema)
if (error) {
  throw new Error(`Config validation error: ${error.message}`)
}

const config = {
  JWT_SECRET: envVars.JWT_SECRET,
  PASSWORD_SALT_ROUND: envVars.PASSWORD_SALT_ROUND
}

module.exports = config
