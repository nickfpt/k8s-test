'use strict'

const common = require('./components/common')
const amqp = require('./components/amqp')
const mailgun = require('./components/mailgun')

module.exports = Object.assign({}, common, amqp, mailgun)
