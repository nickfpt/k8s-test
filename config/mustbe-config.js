'use strict'

const { ROLES } = require('../constants')

module.exports = function (config) {
  config.routeHelpers((rh) => {
    // get the current user from the request object
    rh.getUser((req, cb) => {
      // return cb(err); if there is an error
      cb(null, req.user)
    })

    // what do we do when the user is not authorized?
    rh.notAuthorized((req, res) => {
      res.json({
        ok: false,
        errorCode: 403,
        error: 'Not Authorized'
      })
    })
  })

  config.activities((activities) => {
    // configure an activity with an authorization check
    activities.can(ROLES.ADMIN, (identity, params, cb) => {
      identity.user.verifyRole(ROLES.ADMIN).then((isValid) => cb(null, isValid))
    })

    activities.can(ROLES.ISSUER, (identity, params, cb) => {
      identity.user.verifyRole(ROLES.ISSUER).then((isValid) => cb(null, isValid))
    })

    activities.can(ROLES.RECIPIENT, (identity, params, cb) => {
      identity.user.verifyRole(ROLES.RECIPIENT).then((isValid) => cb(null, isValid))
    })
  })
}
