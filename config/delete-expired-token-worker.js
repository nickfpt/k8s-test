'use strict'

const common = require('./components/common')
const redis = require('./components/redis')

module.exports = Object.assign({}, common, redis)
