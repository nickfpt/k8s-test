'use strict'

const common = require('./components/common')
const amqp = require('./components/amqp')
const redis = require('./components/redis')
const storage = require('./components/google-cloud-storage')
const mongodb = require('./components/mongodb')

module.exports = Object.assign({}, common, amqp, redis, storage, mongodb)
