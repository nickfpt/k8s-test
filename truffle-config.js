'use strict'

const path = require('path')
const dotenv = require('dotenv')
const Joi = require('joi')
const { getHDWalletProvider, infura } = require('./utils/ethereum')

const DEVELOPMENT_NETWORK_CONFIG = {
  host: 'localhost',
  port: 8545,
  network_id: 1234
}

const envVarsSchema = Joi.object({
  CONTRACTS_BUILD_DIRECTORY: Joi.string(),
  MNEMONIC: Joi.string(),
  INFURA_API_KEY: Joi.string().hex().length(32),
  DEPLOYMENT_GAS_LIMIT: Joi.string().regex(/^[1-9]\d*$/),
  DEPLOYMENT_GAS_PRICE: Joi.string().regex(/^[1-9]\d*$/)
})

function infuraProviderFunc (network, apiKey, mnemonic) {
  return () => getHDWalletProvider({
    mnemonic,
    httpEndpoint: infura.getHttpEndpoint(network, apiKey)
  })
}

function loadEnvironmentVariables () {
  dotenv.config()
  const { error, value: envVars } = Joi.validate(process.env, envVarsSchema, { allowUnknown: true, stripUnknown: true })
  if (error) {
    throw error
  }
  return {
    buildDirectory: envVars.CONTRACTS_BUILD_DIRECTORY,
    mnemonic: envVars.MNEMONIC,
    apiKey: envVars.INFURA_API_KEY,
    gas: parseInt(envVars.DEPLOYMENT_GAS_LIMIT, 10),
    gasPrice: parseInt(envVars.DEPLOYMENT_GAS_PRICE, 10)
  }
}

function generateConfig ({ buildDirectory, mnemonic, apiKey, gas, gasPrice }) {
  const config = {
    contracts_build_directory: buildDirectory ? path.resolve(buildDirectory) : undefined,
    networks: {
      development: {}
    }
  }
  Object.assign(config.networks.development, DEVELOPMENT_NETWORK_CONFIG, { gas, gasPrice })
  const infuraNetworks = infura.getSupportedNetworks()
  infuraNetworks.forEach((network) => {
    config.networks[network] = {
      provider: infuraProviderFunc(network, apiKey, mnemonic),
      network_id: infura.getNetworkId(network),
      gas,
      gasPrice
    }
  })
  return config
}

function logEnvironmentVariables ({ buildDirectory, mnemonic, apiKey, gas, gasPrice }) {
  console.log('Using configuration from .env:')
  console.log()
  if (buildDirectory) console.log('Contracts build directory:', buildDirectory)
  if (mnemonic) console.log('Mnemonic:', mnemonic)
  if (apiKey) console.log('Infura API key:', apiKey)
  if (gas) console.log('Deployment gas limit:', gas)
  if (gasPrice) console.log('Deployment gas price:', gasPrice)
  console.log()
}

const envVars = loadEnvironmentVariables()
logEnvironmentVariables(envVars)
const config = generateConfig(envVars)

module.exports = config
