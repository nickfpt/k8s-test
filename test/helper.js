'use strict'

function toString (value) {
  if (value === undefined) {
    return 'undefined'
  }
  if (value === null) {
    return 'null'
  }
  switch (typeof (value)) {
    case 'string': return `"${value}"`
    case 'number':
    case 'boolean': return value.toString()
    case 'object':
      if (Array.isArray(value) || value.constructor === Object) {
        return JSON.stringify(value)
      }
      return Object.prototype.toString.call(value)
    default:
      return Object.prototype.toString.call(value)
  }
}

module.exports = {
  toString
}
