'use strict'

const User = require('../libs/mongodb/models/user')
const Batch = require('../libs/mongodb/models/batch')
const Template = require('../libs/mongodb/models/template')
const { generateAccessToken } = require('../controllers/auth')

function toString (value) {
  if (value === undefined) {
    return 'undefined'
  }
  if (value === null) {
    return 'null'
  }
  switch (typeof (value)) {
    case 'string': return `"${value}"`
    case 'number':
    case 'boolean': return value.toString()
    case 'object':
      if (Array.isArray(value) || value.constructor === Object) {
        return JSON.stringify(value)
      }
      return Object.prototype.toString.call(value)
    default:
      return Object.prototype.toString.call(value)
  }
}

async function generateAdminJWT () {
  const admin = {
    _id: '5beb7b5d2193a407180206a4',
    roles: [
      'admin'
    ],
    email: 'admin@gmail.com',
    password: '$2a$10$adNSChpcV4gLY1dtlRbf9eIgEEgBHYXEsV/vDdLe1fYg5COe6apF2',
    activeToken: '43288e90-e7ad-11e8-8c11-4b3c1f5a3d47',
    isActive: true,
    acceptTokenAfter: '2018-11-14T01:33:17.689Z',
    ethereumAccounts: [
      {
        importDate: '2018-11-06T17:28:24.612Z',
        _id: '5be1cf389a406d109d7e36a6',
        address: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF',
        isDefaultAccount: true
      }
    ]
  }
  await new User(admin).save()
  return generateAccessToken(admin).token
}

async function generateIssuerJWT (issuer) {
  return generateAccessToken(issuer).token
}

async function createIssuerUser (email = 'admin@harvard.com', isActive = true) {
  const issuerData = {
    roles: ['issuer'],
    email,
    acceptTokenAfter: '2018-11-14T01:33:17.689Z',
    contactEmail: 'admin@harvard.com',
    password: '$2a$10$5r5A.O4LGbrbDWLDK7x9nOQ5I0PCH3eBTCsBty4wjWgJYK87aAN6e',
    ownerName: 'Nguyen Xuan Tien',
    taxAccount: '12315113123123123',
    isActive,
    logo: 'https://banner2.kisspng.com/20180816/jac/kisspng-logo-university-clip-art-harvard-research-corporat-test-mindkind-institute-executive-coaching-amp-5b759c5c29a8d0.0167442715344343961706.jpg',
    phone: '0961231223',
    establishedDate: new Date(),
    fullname: 'Harvard University',
    address: 'Cambridge, Massachusetts, United States',
    issuableStatus: 1,
    activeToken: '12313312',
    ethereumAccounts: [],
    website: 'https://www.harvard.edu/',
    description: 'Harvard University is a private Ivy League research university in Cambridge, Massachusetts, with about 6,700 undergraduate students and about 15,250 post graduate students. Established in 1636 and named for its first benefactor, clergyman John Harvard, Harvard is the United States\' oldest institution of higher learning,[9] and its history, influence, and wealth have made it one of the world\'s most prestigious universities.'
  }
  const newIssuer = new User(issuerData)
  const issuer = await newIssuer.save()
  return issuer
}

async function createRecipientUser (email = 'recipient@gmail.com') {
  const issuerData = {
    roles: ['recipient'],
    email,
    password: '12345@asw46',
    isActive: true,
    activeToken: '12313312'
  }
  const newIssuer = new User(issuerData)
  return newIssuer.save()
}

async function createABatch (title, issuerId, isSigned = true, revoked = false) {
  const batchData = {
    quantity: 1,
    tags: [],
    isSigned,
    revoked,
    title,
    description: '',
    issuerId,
    __v: 0,
    header: ['No', 'Certificate name', 'Roll number', 'Recipient Name',
      'Email', 'DoB', 'Ethereum Address', 'PoB',
      'Gender', 'Nationality', 'Race', 'Major',
      'Graduation Year', 'Grade', 'Mode of Study', 'Cert Id', 'Decision Id'],
    merkleRoot: '4ef50f2deb6965c2751e91573404c7c04ef211e91b1762c571297664e6f5eb6d',
    issueTime: ('2018-12-15T01:50:33.852Z')
  }
  return new Batch(batchData).save()
}

async function createATemplate (issuerId) {
  const templateData = {
    content: {
      'Roll number': 'content.recipientInfo.rollNumber',
      'Ethereum Address': 'content.recipientInfo.ethereumAddress',
      'Recipient Name': 'content.recipientInfo.name',
      DoB: 'content.recipientInfo.dob',
      PoB: 'content.recipientInfo.pob',
      Email: 'content.recipientInfo.email',
      Gender: 'content.recipientInfo.gender',
      Nationality: 'content.recipientInfo.nationality',
      Race: 'content.recipientInfo.race',
      'Decision Id': 'content.graduationDecisionId',
      'Cert Id': 'content.certId',
      'Certificate name': 'content.certName',
      Major: 'content.major',
      'Graduation Year': 'content.graduationYear',
      'Mode of Study': 'content.studyMode',
      'Graduation Grade': 'content.graduationGrade'
    },
    description: '',
    issuerId,
    name: 'My new template'
  }
  return new Template(templateData).save()
}

module.exports = {
  toString,
  generateAdminJWT,
  generateIssuerJWT,
  createIssuerUser,
  createRecipientUser,
  createABatch,
  createATemplate
}
