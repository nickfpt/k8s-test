/* eslint-disable no-undef */

'use strict'

const chai = require('chai')
const sinon = require('sinon')
const request = require('super-request')
const server = require('../../../micro-services/web/server')
const { createIssuerUser, generateIssuerJWT } = require('../../factory')
const User = require('../../../libs/mongodb/models/user')
const profile = require('../../../controllers/profile')

const expect = chai.expect

afterEach(() => {
  sinon.restore()
})

describe('/', async () => {
  it('return correct response for health check API', async () => {
    const response = await request(server.listen())
      .get('/')
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body).to.deep.equal({
      status: 'It is ok!'
    })
  })
})

describe('/api/v1/profile/me', async () => {
  const url = '/api/v1/profile/me'
  let issuerToken
  afterEach(async () => {
    await User.remove({})
  })

  it('should return current issuer', async () => {
    const issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.email).to.equal(issuer.toObject().email)
  })

  it('should return current issuer', async () => {
    const issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.email).to.equal(issuer.toObject().email)
  })
})

describe('/api/v1/profile/changePassword', async () => {
  const url = '/api/v1/profile/changePassword'
  let issuerToken
  afterEach(async () => {
    await User.remove({})
  })

  it('empty password', async () => {
    const issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        oldPassword: '',
        newPassword: '123123'
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
    expect(body.errorCode).to.equal(422)
  })

  it('empty password', async () => {
    const issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        oldPassword: '123123',
        newPassword: ''
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
    expect(body.errorCode).to.equal(422)
  })

  it('old password equal new password', async () => {
    const issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        oldPassword: '123123',
        newPassword: '123123'
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
    expect(body.errorCode).to.equal(422)
  })

  it('old password equal new password', async () => {
    const issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        oldPassword: '123xx123',
        newPassword: '123xx123'
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
    expect(body.errorCode).to.equal(422)
  })

  it('new password is invalid', async () => {
    const issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        oldPassword: '123x123',
        newPassword: '123123'
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
    expect(body.errorCode).to.equal(401)
  })

  it('old password is not match', async () => {
    const issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        oldPassword: '123123Aa',
        newPassword: '123123Aaa'
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('new password does not satisfy password policy', async () => {
    const issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
    sinon.stub(profile, 'verifyPassword')
    profile.verifyPassword.callsFake(true)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        oldPassword: '123123Aa',
        newPassword: '123123aa'
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.errorCode).to.equal(401)
  })

  it('new password does not satisfy password policy', async () => {
    const issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
    sinon.stub(profile, 'verifyPassword')
    profile.verifyPassword.callsFake(true)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        oldPassword: '123123Aa',
        newPassword: 'aa'
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.errorCode).to.equal(401)
  })

  it('new password does not satisfy password policy', async () => {
    const issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
    sinon.stub(profile, 'verifyPassword')
    profile.verifyPassword.callsFake(true)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        oldPassword: '123123Aa',
        newPassword: '111'
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.errorCode).to.equal(401)
  })
})
