/* eslint-disable no-undef */

'use strict'

const chai = require('chai')
const sinon = require('sinon')
const request = require('super-request')
const server = require('../../../micro-services/web/server')
const { createIssuerUser, generateIssuerJWT } = require('../../factory')
const User = require('../../../libs/mongodb/models/user')

const expect = chai.expect

afterEach(() => {
  sinon.restore()
})

describe('/login', async () => {
  const url = '/login'
  before(async () => {
    await createIssuerUser()
  })

  after(async () => {
    await User.remove({})
  })

  it('should login successfully', async () => {
    const response = await request(server.listen())
      .post(url)
      .json(true)
      .body({
        email: 'admin@harvard.com',
        password: '123123Aa'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('login failed as email is empty', async () => {
    const response = await request(server.listen())
      .post(url)
      .json(true)
      .body({
        email: '',
        password: '123123Aa'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('login failed as password is empty', async () => {
    const response = await request(server.listen())
      .post(url)
      .json(true)
      .body({
        email: 'admin@harvard.com',
        password: ''
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('login failed as wrong password', async () => {
    const response = await request(server.listen())
      .post(url)
      .json(true)
      .body({
        email: 'admin@harvard.com',
        password: '111'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('login failed as user account is not exist', async () => {
    const response = await request(server.listen())
      .post(url)
      .json(true)
      .body({
        email: 'adminx@harvard.com',
        password: '123123Aa'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })
})


describe('/register', async () => {
  const url = '/register'
  before(async () => {
    await createIssuerUser()
  })

  after(async () => {
    await User.remove({})
  })

  it('register fail as email is not valid', async () => {
    const response = await request(server.listen())
      .post(url)
      .json(true)
      .body({
        email: 'admin',
        password: '123123Aa'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('register fail as password is weak', async () => {
    const response = await request(server.listen())
      .post(url)
      .json(true)
      .body({
        email: 'admin@gmail.com',
        password: '123123'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('register fail as password is weak', async () => {
    const response = await request(server.listen())
      .post(url)
      .json(true)
      .body({
        email: 'admin@gmail.com',
        password: 'abcd'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('register fail as password is weak', async () => {
    const response = await request(server.listen())
      .post(url)
      .json(true)
      .body({
        email: 'admin@gmail.com',
        password: ''
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('register fail as email is empty', async () => {
    const response = await request(server.listen())
      .post(url)
      .json(true)
      .body({
        email: '',
        password: ''
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('register fail as email is existed', async () => {
    await createIssuerUser('a@harvard.com')
    const response = await request(server.listen())
      .post(url)
      .json(true)
      .body({
        email: 'a@harvard.com',
        password: ''
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('register fail as email is existed', async () => {
    await createIssuerUser('x@harvard.com')
    const response = await request(server.listen())
      .post(url)
      .json(true)
      .body({
        email: 'x@harvard.com',
        password: ''
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('register successully', async () => {
    const response = await request(server.listen())
      .post(url)
      .json(true)
      .body({
        email: 'admin123@harvard.com',
        password: '123123Aa'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })
})


describe('/activate/:token', async () => {
  const url = '/activate/:token'
  before(async () => {
    await createIssuerUser()
  })

  after(async () => {
    await User.remove({})
  })

  it('activate fail as token is not exist', async () => {
    await request(server.listen())
      .get(url.replace(':token', '521e7b00-fcf0-11e8-a0b2-a56619c37ed6'))
      .json(true)
      .expect(200)
      .end()
    expect(true).to.equal(true)
  })

  it('activate fail as token is invalid', async () => {
    await request(server.listen())
      .get(url.replace(':token', 'xxx'))
      .json(true)
      .expect(200)
      .end()
    expect(true).to.equal(true)
  })
})

describe('/api/v1/logout', async () => {
  const url = '/api/v1/logout'
  let issuer
  let issuerToken
  before(async () => {
    issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
  })

  after(async () => {
    await User.remove({})
  })

  it('logout successfully', async () => {
    await request(server.listen())
      .get(url)
      .json(true)
      .expect(200)
      .headers({
        'x-access-token': issuerToken
      })
      .end()
    expect(true).to.equal(true)
  })

  it('logout failed as token is wrong', async () => {
    await request(server.listen())
      .get(url)
      .json(true)
      .headers({
        'x-access-token': 'xxx'
      })
      .expect(200)
      .end()
    expect(true).to.equal(true)
  })
})
