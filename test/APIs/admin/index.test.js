/* eslint-disable no-undef */

'use strict'

const chai = require('chai')
const sinon = require('sinon')
const request = require('super-request')
const server = require('../../../micro-services/web/server')
const { generateAdminJWT, createIssuerUser, createRecipientUser } = require('../../factory')
const User = require('../../../libs/mongodb/models/user')
const Certificate = require('../../../libs/mongodb/models/certificate')

const expect = chai.expect

afterEach(() => {
  sinon.restore()
})

describe('/', async () => {
  it('return correct response for health check API', async () => {
    const response = await request(server.listen())
      .get('/')
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body).to.deep.equal({
      status: 'It is ok!'
    })
  })
})

describe('/api/v1/admin/users/:id', async () => {
  const url = '/api/v1/admin/users/:id'
  let issuer
  let adminToken
  before(async () => {
    issuer = await createIssuerUser()
    adminToken = await generateAdminJWT()
  })

  after(async () => {
    await User.remove({})
  })

  it('should update fullname', async () => {
    const response = await request(server.listen())
      .post(url.replace(':id', issuer._id))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .body({
        fullname: 'FPT University'
      })
      .expect(200)
      .end()
    const { body } = response
    const updatedIssuer = await User.findById(issuer._id)
    expect(updatedIssuer.fullname).to.equal('FPT University')
    expect(body).to.deep.equal({
      ok: true
    })
  })

  it('should not update password', async () => {
    const response = await request(server.listen())
      .post(url.replace(':id', issuer._id))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .body({
        password: '13123asda'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body).to.deep.equal({
      ok: true
    })
  })

  it('should throw 404 error', async () => {
    await request(server.listen())
      .post(url.replace(':id', ''))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .body({
        website: 'facebook.com'
      })
      .expect(404)
      .end()
  })

  it('should return false status', async () => {
    const response = await request(server.listen())
      .post(url.replace(':id', 'xxx'))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .body({
        address: 'Tokyo, Japan'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body).to.have.property('ok', false)
  })

  it('should update contactEmail', async () => {
    const response = await request(server.listen())
      .post(url.replace(':id', issuer._id))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .body({
        contactEmail: 'nicksmd@gmail.com'
      })
      .expect(200)
      .end()
    const { body } = response
    const updatedIssuer = await User.findById(issuer._id)
    expect(updatedIssuer.contactEmail).to.equal('nicksmd@gmail.com')
    expect(body).to.deep.equal({
      ok: true
    })
  })

  it('should return false status', async () => {
    const response = await request(server.listen())
      .post(url.replace(':id', 'xxx'))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .body({
        address: 'Tokyo, Japan'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body).to.have.property('ok', false)
  })
})

describe('/api/v1/admin/users', async () => {
  const url = '/api/v1/admin/users'
  let adminToken
  before(async () => {
    adminToken = await generateAdminJWT()
  })

  after(async () => {
    await User.remove({})
  })

  it('should return all issuer', async () => {
    await createIssuerUser()
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.issuers.length).to.equal(1)
  })

  it('should return 2 issuer', async () => {
    await createIssuerUser('admin2@harvard.com')
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.issuers.length).to.equal(2)
  })

  it('should return 2 issuer', async () => {
    await createRecipientUser()
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.issuers.length).to.equal(2)
  })
})

describe('/api/v1/admin/users/:id', async () => {
  const url = '/api/v1/admin/users/:id'
  let adminToken
  beforeEach(async () => {
    adminToken = await generateAdminJWT()
  })

  afterEach(async () => {
    await User.remove({})
  })

  it('should return correct issuer', async () => {
    const issuer = await createIssuerUser()
    const response = await request(server.listen())
      .get(url.replace(':id', issuer._id))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.issuer.email).to.equal(issuer.email)
  })

  it('should return no issuer', async () => {
    const response = await request(server.listen())
      .get(url.replace(':id', '5be1cf389a406d109d7e369b'))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.issuer.email).to.equal(undefined)
  })

  it('should return correct issuer', async () => {
    const issuer = await createIssuerUser()
    const response = await request(server.listen())
      .get(url.replace(':id', issuer._id))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.issuer.email).to.equal(issuer.email)
  })
})

describe('/api/v1/admin/changeIssuerStatus/:issuerId', async () => {
  const url = '/api/v1/admin/changeIssuerStatus/:issuerId'
  let adminToken
  before(async () => {
    adminToken = await generateAdminJWT()
  })

  after(async () => {
    await User.remove({})
  })

  it('should change issuable status to 0', async () => {
    const issuer = await createIssuerUser()
    const response = await request(server.listen())
      .get(url.replace(':issuerId', `${issuer._id}?active=true`))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
    const updatedIssuer = await User.findById(issuer._id)
    expect(updatedIssuer.issuableStatus).to.equal(1)
  })

  it('should change issuable status to 1', async () => {
    const issuer = await createIssuerUser('quang@gmail.com')
    const response = await request(server.listen())
      .get(url.replace(':issuerId', `${issuer._id}?active=false`))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
    const updatedIssuer = await User.findById(issuer._id)
    expect(updatedIssuer.issuableStatus).to.equal(0)
  })

  it('should change issuable status to 0', async () => {
    const issuer = await createIssuerUser('quang1@gmail.com')
    const response = await request(server.listen())
      .get(url.replace(':issuerId', `${issuer._id}`))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
    const updatedIssuer = await User.findById(issuer._id)
    expect(updatedIssuer.issuableStatus).to.equal(0)
  })

  it('should return error', async () => {
    const response = await request(server.listen())
      .get(url.replace(':issuerId', 'xxx'))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })
})

describe('/api/v1/admin/addETHAddress/:issuerId', async () => {
  const url = '/api/v1/admin/addETHAddress/:issuerId'
  let adminToken
  let issuer
  before(async () => {
    adminToken = await generateAdminJWT()
    issuer = await createIssuerUser()
  })

  after(async () => {
    await User.remove({})
  })

  it('should add a Ethereum address', async () => {
    const response = await request(server.listen())
      .post(url.replace(':issuerId', `${issuer._id}`))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .body({
        address: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should not add a invalid Ethereum address', async () => {
    const response = await request(server.listen())
      .post(url.replace(':issuerId', `${issuer._id}`))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .body({
        address: '0x22e63d3F87b82696F293BadadbdC989b2682AF'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not add a invalid Ethereum address', async () => {
    const response = await request(server.listen())
      .post(url.replace(':issuerId', `${issuer._id}`))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .body({
        address: '0x23e63d3F87b82696F293B0D3F32bdC989b2682AF'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should return error issuer not found', async () => {
    const response = await request(server.listen())
      .post(url.replace(':issuerId', '5be1cf389a406d109d7e369b'))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .body({
        address: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
    expect(body.error).to.equal('Issuer not found')
  })

  it('should return error', async () => {
    const response = await request(server.listen())
      .post(url.replace(':issuerId', 'xxx'))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .body({
        address: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })
})

describe('/api/v1/admin/removeETHAddress/:issuerId', async () => {
  const url = '/api/v1/admin/removeETHAddress/:issuerId'
  let adminToken
  let issuer
  before(async () => {
    adminToken = await generateAdminJWT()
    issuer = await createIssuerUser()
  })

  after(async () => {
    await User.remove({})
  })

  it('should remove a Ethereum address', async () => {
    const response = await request(server.listen())
      .post(url.replace(':issuerId', `${issuer._id}`))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .body({
        address: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should return error issuer not found', async () => {
    const response = await request(server.listen())
      .post(url.replace(':issuerId', '5be1cf389a406d109d7e369b'))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .body({
        address: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
    expect(body.error).to.equal('Issuer not found')
  })

  it('should return error', async () => {
    const response = await request(server.listen())
      .post(url.replace(':issuerId', 'xxx'))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .body({
        address: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not remove an invalid Ethereum address', async () => {
    const response = await request(server.listen())
      .post(url.replace(':issuerId', `${issuer._id}`))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .body({
        address: '0x22e63d3F87b82696F293BadadbdC989b2682AF'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should return error issuer not found', async () => {
    const response = await request(server.listen())
      .post(url.replace(':issuerId', '5be1cf389a406d109d7e369b'))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .body({
        address: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
    expect(body.error).to.equal('Issuer not found')
  })

  it('should not remove an invalid Ethereum address', async () => {
    const response = await request(server.listen())
      .post(url.replace(':issuerId', `${issuer._id}`))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .body({
        address: '0x23e63d3F87b82696F293B0D3F32bdC989b2682AF'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })
})

describe('/api/v1/admin/checkIfETHAddressIsNotUsed/:address', async () => {
  const url = '/api/v1/admin/checkIfETHAddressIsNotUsed/:address'
  let adminToken
  before(async () => {
    adminToken = await generateAdminJWT()
    await createIssuerUser()
    const certificate = new Certificate({
      issuerId: '5be6da82358b0347769a5bc8',
      metadata: {
        isSigned: true,
        revoked: false,
        _id: ('5c0c9f0f0acbe4087f06698e'),
        batchId: ('5c0c9f0c0acbe4087f06698c'),
        issueTime: ('2018-12-09T04:50:51.305Z')
      },
      content: {
      },
      signature: {
        _id: ('5c0c9f0f0acbe4087f0669ab'),
        proof: [
          {
            _id: ('5c0c9f0f0acbe4087f0669ad'),
            right: '71891927c01282ad35551d628485177a36ed4d7b824d64b439d00e5c40ee3cbe'
          },
          {
            _id: ('5c0c9f0f0acbe4087f0669ac'),
            right: '3fb87c86aa77d622e404788202dd95db409c983a5dcb7937bb02ac24bf0e9eb2'
          }
        ],
        merkleRoot: '1187daa574bf0948423c2cc196287ae4878e7fb14b8821d0ed31e0b9aa3fde0e',
        targetHash: '3470b9b0ea7a7064a85b51b46398293759ef6872171827bde5e61b3341cc8033',
        signedBy: '0x28A59b5a32bF9A8f7a3Bff80Cabe758A3b2f6818',
        transactionId: '0x76a9f6a45b8f042df3b3a7f138d776eb9271a24b504169ff9e6201c5e79654bc'
      }
    })
    await certificate.save()
  })

  after(async () => {
    await User.remove({})
    await Certificate.remove({})
  })

  it('should return false', async () => {
    const response = await request(server.listen())
      .get(url.replace(':address', '0x28A59b5a32bF9A8f7a3Bff80Cabe758A3b2f6818'))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should return true', async () => {
    const response = await request(server.listen())
      .get(url.replace(':address', '0x22A59b5a32bF9A8f7a3Bff80Cabe758A3b2f6818'))
      .headers({
        'x-access-token': adminToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })
})
