/* eslint-disable no-undef */

'use strict'

const chai = require('chai')
const sinon = require('sinon')
const request = require('super-request')
const server = require('../../../../micro-services/web/server')
const { generateIssuerJWT, createATemplate, createIssuerUser } = require('../../../factory')
const User = require('../../../../libs/mongodb/models/user')
const Template = require('../../../../libs/mongodb/models/template')

const expect = chai.expect

afterEach(() => {
  sinon.restore()
})

describe('/api/v1/issuer/templates', async () => {
  const url = '/api/v1/issuer/templates'
  let issuerToken
  let issuer
  before(async () => {
    issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
  })

  after(async () => {
    await User.remove({})
  })

  afterEach(async () => {
    await Template.remove({})
  })

  it('should list all template of issuer', async () => {
    await createATemplate(issuer._id)
    await createATemplate(issuer._id)
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.templates.length).to.equal(2)
  })

  it('should list all template of issuer', async () => {
    await createATemplate(issuer._id)
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.templates.length).to.equal(1)
  })

  it('should list all template of issuer', async () => {
    await createATemplate(issuer._id)
    await createATemplate(issuer._id)
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.templates.length).to.equal(2)
  })

  it('should list all template of issuer', async () => {
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.templates.length).to.equal(0)
  })

  it('should list all template of issuer', async () => {
    await createATemplate(issuer._id)
    await createATemplate(issuer._id)
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.templates.length).to.equal(2)
  })
})

describe('/api/v1/issuer/template/:templateId', async () => {
  const url = '/api/v1/issuer/template/:templateId'
  let issuerToken
  let issuer
  before(async () => {
    issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
  })

  after(async () => {
    await User.remove({})
  })

  afterEach(async () => {
    await Template.remove({})
  })

  it('should list 0 template', async () => {
    const response = await request(server.listen())
      .get(url.replace(':templateId', '5bcda53c469157963e4db624'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.templates).to.equal(null)
  })

  it('should list template by its id', async () => {
    const template = await createATemplate(issuer._id)
    const response = await request(server.listen())
      .get(url.replace(':templateId', template._id))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should list 0 template', async () => {
    const response = await request(server.listen())
      .get(url.replace(':templateId', '5bcda53c469157963e4db624'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.templates).to.equal(null)
  })

  it('invalud template id', async () => {
    const response = await request(server.listen())
      .get(url.replace(':templateId', 'xxx'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })
})

describe('/api/v1/issuer/deleteTemplate/:templateId', async () => {
  const url = '/api/v1/issuer/deleteTemplate/:templateId'
  let issuerToken
  let issuer
  before(async () => {
    issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
  })

  after(async () => {
    await User.remove({})
  })

  afterEach(async () => {
    await Template.remove({})
  })

  it('should delete template by its id', async () => {
    const template = await createATemplate(issuer._id)
    const response = await request(server.listen())
      .get(url.replace(':templateId', template._id))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should delete 0 template', async () => {
    const response = await request(server.listen())
      .get(url.replace(':templateId', '5bcda53c469157963e4db624'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('invalid template id', async () => {
    const response = await request(server.listen())
      .get(url.replace(':templateId', 'xxx'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })
})

describe('/api/v1/issuer/template', async () => {
  const url = '/api/v1/issuer/template'
  let issuerToken
  let issuer
  before(async () => {
    issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
  })

  after(async () => {
    await User.remove({})
  })

  afterEach(async () => {
    await Template.remove({})
  })

  it('should create a new template', async () => {
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        template: {
          content: {
            'Roll number': 'content.recipientInfo.rollNumber',
            'Ethereum Address': 'content.recipientInfo.ethereumAddress',
            'Recipient Name': 'content.recipientInfo.name',
            DoB: 'content.recipientInfo.dob',
            PoB: 'content.recipientInfo.pob',
            Email: 'content.recipientInfo.email',
            Gender: 'content.recipientInfo.gender',
            Nationality: 'content.recipientInfo.nationality',
            Race: 'content.recipientInfo.race',
            'Decision Id': 'content.graduationDecisionId',
            'Cert Id': 'content.certId',
            'Certificate name': 'content.certName',
            Major: 'content.major',
            'Graduation Year': 'content.graduationYear',
            'Mode of Study': 'content.studyMode',
            'Graduation Grade': 'content.graduationGrade'
          },
          name: 'My new template 2'
        }
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should create a new template', async () => {
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        template: {
          content: {
            'Roll number': 'content.recipientInfo.rollNumber',
            'Ethereum Address': 'content.recipientInfo.ethereumAddress',
            'Recipient Name': 'content.recipientInfo.name',
            DoB: 'content.recipientInfo.dob',
            PoB: 'content.recipientInfo.pob',
            Email: 'content.recipientInfo.email',
            Gender: 'content.recipientInfo.gender',
            Nationality: 'content.recipientInfo.nationality',
            Race: 'content.recipientInfo.race',
            'Decision Id': 'content.graduationDecisionId',
            'Cert Id': 'content.certId',
            'Certificate name': 'content.certName',
            Major: 'content.major',
            'Graduation Year': 'content.graduationYear',
            'Mode of Study': 'content.studyMode',
            'Graduation Grade': 'content.graduationGrade'
          },
          name: 'My new template 2',
          issuerId: '5bcda53c469157963e4db624'
        }
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should not create a template with no name', async () => {
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        template: {
          content: {
            'Roll number': 'content.recipientInfo.rollNumber',
            'Ethereum Address': 'content.recipientInfo.ethereumAddress',
            'Recipient Name': 'content.recipientInfo.name',
            DoB: 'content.recipientInfo.dob',
            PoB: 'content.recipientInfo.pob',
            Email: 'content.recipientInfo.email',
            Gender: 'content.recipientInfo.gender',
            Nationality: 'content.recipientInfo.nationality',
            Race: 'content.recipientInfo.race',
            'Decision Id': 'content.graduationDecisionId',
            'Cert Id': 'content.certId',
            'Certificate name': 'content.certName',
            Major: 'content.major',
            'Graduation Year': 'content.graduationYear',
            'Mode of Study': 'content.studyMode',
            'Graduation Grade': 'content.graduationGrade'
          }
        }
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not create a template with existed name', async () => {
    await createATemplate(issuer._id)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        template: {
          content: {
            'Roll number': 'content.recipientInfo.rollNumber',
            'Ethereum Address': 'content.recipientInfo.ethereumAddress',
            'Recipient Name': 'content.recipientInfo.name',
            DoB: 'content.recipientInfo.dob',
            PoB: 'content.recipientInfo.pob',
            Email: 'content.recipientInfo.email',
            Gender: 'content.recipientInfo.gender',
            Nationality: 'content.recipientInfo.nationality',
            Race: 'content.recipientInfo.race',
            'Decision Id': 'content.graduationDecisionId',
            'Cert Id': 'content.certId',
            'Certificate name': 'content.certName',
            Major: 'content.major',
            'Graduation Year': 'content.graduationYear',
            'Mode of Study': 'content.studyMode',
            'Graduation Grade': 'content.graduationGrade'
          },
          name: 'My new template'
        }
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })
})

describe('/api/v1/issuer/updateTemplate/:templateId', async () => {
  const url = '/api/v1/issuer/updateTemplate/:templateId'
  let issuerToken
  let issuer
  before(async () => {
    issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
  })

  after(async () => {
    await User.remove({})
  })

  afterEach(async () => {
    await Template.remove({})
  })

  it('should not update issuerId of a template', async () => {
    const template = await createATemplate(issuer._id)
    const response = await request(server.listen())
      .post(url.replace(':templateId', template._id))
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        template: {
          issuerId: 'adasdas'
        }
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not update issuerId of a template', async () => {
    const template = await createATemplate(issuer._id)
    const response = await request(server.listen())
      .post(url.replace(':templateId', template._id))
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        template: {
          issuerId: '5c145d8c11d38d0b9617692b'
        }
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should update name of a template', async () => {
    const template = await createATemplate(issuer._id)
    const response = await request(server.listen())
      .post(url.replace(':templateId', template._id))
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        template: {
          name: 'This is a new name'
        }
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should update content of a template', async () => {
    const template = await createATemplate(issuer._id)
    const response = await request(server.listen())
      .post(url.replace(':templateId', template._id))
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        template: {
          content: {
            'Roll number': 'content.recipientInfo.rollNumber'
          }
        }
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should update both name and content of a template', async () => {
    const template = await createATemplate(issuer._id)
    const response = await request(server.listen())
      .post(url.replace(':templateId', template._id))
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        template: {
          content: {
            'Roll number': 'content.recipientInfo.rollNumber'
          },
          name: 'This is a new name'
        }
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should update existed name', async () => {
    const template = await createATemplate(issuer._id)
    const response = await request(server.listen())
      .post(url.replace(':templateId', template._id))
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        template: {
          content: {
            'Roll number': 'content.recipientInfo.rollNumber'
          },
          name: 'My new template'
        }
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should update existed name', async () => {
    const template = await createATemplate(issuer._id)
    const response = await request(server.listen())
      .post(url.replace(':templateId', template._id))
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        template: {
          name: 'My new template'
        }
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should update name of the template', async () => {
    const template = await createATemplate(issuer._id)
    const response = await request(server.listen())
      .post(url.replace(':templateId', template._id))
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        template: {
          content: {
            'Roll number': 'content.recipientInfo.rollNumber'
          },
          name: 'My new template 1'
        }
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should not update template with empty value', async () => {
    const template = await createATemplate(issuer._id)
    const response = await request(server.listen())
      .post(url.replace(':templateId', template._id))
      .headers({
        'x-access-token': issuerToken
      })
      .body({})
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not update template with empty value', async () => {
    const template = await createATemplate(issuer._id)
    const response = await request(server.listen())
      .post(url.replace(':templateId', template._id))
      .headers({
        'x-access-token': issuerToken
      })
      .body({
        template: {}
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })
})

describe('/api/v1/issuer/template/:templateId', async () => {
  const url = '/api/v1/issuer/template/:templateId'
  let issuerToken
  let issuer
  before(async () => {
    issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
  })

  after(async () => {
    await User.remove({})
  })

  afterEach(async () => {
    await Template.remove({})
  })

  it('should list 0 template', async () => {
    const response = await request(server.listen())
      .get(url.replace(':templateId', '5bcda53c469157963e4db624'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.templates).to.equal(null)
  })

  it('should list template by its id', async () => {
    const template = await createATemplate(issuer._id)
    const response = await request(server.listen())
      .get(url.replace(':templateId', template._id))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should list 0 template', async () => {
    const response = await request(server.listen())
      .get(url.replace(':templateId', '5bcda53c469157963e4db624'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.templates).to.equal(null)
  })

  it('invalud template id', async () => {
    const response = await request(server.listen())
      .get(url.replace(':templateId', 'xxx'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })
})
