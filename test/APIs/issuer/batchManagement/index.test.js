/* eslint-disable no-undef */

'use strict'

const chai = require('chai')
const sinon = require('sinon')
const request = require('super-request')
const server = require('../../../../micro-services/web/server')
const { generateIssuerJWT, createABatch, createIssuerUser } = require('../../../factory')
const User = require('../../../../libs/mongodb/models/user')
const Batch = require('../../../../libs/mongodb/models/batch')
const Certificate = require('../../../../libs/mongodb/models/certificate')
const certnetContract = require('../../../../contracts/certnet')

const expect = chai.expect

afterEach(() => {
  sinon.restore()
})

describe('/api/v1/issuer/batch', async () => {
  const url = '/api/v1/issuer/batch'
  let issuerToken
  let issuer
  before(async () => {
    issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
  })

  after(async () => {
    await User.remove({})
    await Batch.remove({})
  })

  it('should create a new batch', async () => {
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batch: {
          title: 'Summer 2030',
          tags: ['Hoalac-campus'],
          header: ['No', 'Certificate name', 'Roll number', 'Recipient Name',
            'Email', 'DoB', 'Ethereum Address', 'PoB',
            'Gender', 'Nationality', 'Race', 'Major',
            'Graduation Year', 'Grade', 'Mode of Study', 'Cert Id', 'Decision Id']
        }
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should not create a new batch', async () => {
    await createABatch('Summer 2031', issuer._id)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batch: {
          title: 'Summer 2031',
          tags: ['Hoalac-campus'],
          header: ['No', 'Certificate name', 'Roll number', 'Recipient Name',
            'Email', 'DoB', 'Ethereum Address', 'PoB',
            'Gender', 'Nationality', 'Race', 'Major',
            'Graduation Year', 'Grade', 'Mode of Study', 'Cert Id', 'Decision Id']
        }
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not create a new batch', async () => {
    await createABatch('Summer 2031', issuer._id)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batch: {
          title: 'Summer 2031',
          tags: ['Hoalac-campus'],
          header: ['No', 'Certificate name', 'Roll number', 'Recipient Name',
            'Email', 'DoB', 'Ethereum Address', 'PoB',
            'Gender', 'Nationality', 'Race', 'Major',
            'Graduation Year', 'Grade', 'Mode of Study', 'Cert Id', 'Decision Id']
        }
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not create a new batch', async () => {
    await createABatch('Summer 2032', issuer._id, false)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batch: {
          title: 'Summer 2032',
          tags: ['Hoalac-campus'],
          header: ['No', 'Certificate name', 'Roll number', 'Recipient Name',
            'Email', 'DoB', 'Ethereum Address', 'PoB',
            'Gender', 'Nationality', 'Race', 'Major',
            'Graduation Year', 'Grade', 'Mode of Study', 'Cert Id', 'Decision Id']
        }
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should not create a new batch', async () => {
    await createABatch('Summer 2032', issuer._id, false)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batch: {
          title: 'Summer 2032',
          tags: ['Hoalac-campus'],
          header: ['No', 'Certificate name', 'Roll number', 'Recipient Name',
            'Email', 'DoB', 'Ethereum Address', 'PoB',
            'Gender', 'Nationality', 'Race', 'Major',
            'Graduation Year', 'Grade', 'Mode of Study', 'Cert Id', 'Decision Id']
        }
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should not create a new batch', async () => {
    await createABatch('Summer 2034', issuer._id)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batch: {
          title: 'Summer 2033',
          tags: ['Hoalac-campus'],
          header: ['No', 'Certificate name', 'Roll number', 'Recipient Name',
            'Email', 'DoB', 'Ethereum Address', 'PoB',
            'Gender', 'Nationality', 'Race', 'Major',
            'Graduation Year', 'Grade', 'Mode of Study', 'Cert Id', 'Decision Id']
        }
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })
})

describe('/api/v1/issuer/batches', async () => {
  const url = '/api/v1/issuer/batches'
  let issuerToken
  let issuer
  before(async () => {
    issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
  })

  after(async () => {
    await User.remove({})
  })

  afterEach(async () => {
    await Batch.remove({})
  })

  it('should list all batches created by issuer', async () => {
    await createABatch('Summer 1', issuer._id)
    await createABatch('Summer 2', issuer._id)
    await createABatch('Summer 3', issuer._id)
    await createABatch('Summer 4', issuer._id)
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.batches.length).to.equal(4)
  })

  it('should list all batches created by issuer', async () => {
    await createABatch('Summer 1', issuer._id, false)
    await createABatch('Summer 2', issuer._id, false)
    await createABatch('Summer 3', issuer._id)
    await createABatch('Summer 4', issuer._id)
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.batches.length).to.equal(2)
  })

  it('should list all batches created by issuer', async () => {
    await createABatch('Summer 1', issuer._id)
    await createABatch('Summer 2', issuer._id, false)
    await createABatch('Summer 3', issuer._id)
    await createABatch('Summer 4', issuer._id)
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.batches.length).to.equal(3)
  })

  it('should list all batches created by issuer', async () => {
    await createABatch('Summer 1', issuer._id)
    await createABatch('Summer 2', issuer._id, false)
    await createABatch('Summer 3', issuer._id)
    await createABatch('Summer 4', issuer._id)
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.batches.length).to.equal(3)
  })

  it('should list all batches created by issuer', async () => {
    await createABatch('Summer 1', issuer._id)
    await createABatch('Summer 2', issuer._id, false)
    await createABatch('Summer 3', issuer._id)
    await createABatch('Summer 4', issuer._id)
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.batches.length).to.equal(3)
  })
})

describe('/api/v1/issuer/batch/:batchId', async () => {
  const url = '/api/v1/issuer/batch/:batchId'
  let issuerToken
  let issuer
  before(async () => {
    issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
  })

  after(async () => {
    await User.remove({})
  })

  afterEach(async () => {
    await Batch.remove({})
  })

  it('should list the batch', async () => {
    const batch = await createABatch('Summer 1', issuer._id)
    const response = await request(server.listen())
      .get(url.replace(':batchId', batch._id))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.certs.length).to.equal(0)
  })

  it('should list the batch', async () => {
    const batch = await createABatch('Summer 2', issuer._id, false)
    const response = await request(server.listen())
      .get(url.replace(':batchId', batch._id))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.certs.length).to.equal(0)
  })

  it('should return 0 batch', async () => {
    const response = await request(server.listen())
      .get(url.replace(':batchId', '5c0c9dbc0acbe4087f066989'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should return 0 batch', async () => {
    const response = await request(server.listen())
      .get(url.replace(':batchId', '5c0c9dbc0acbe4087f066989'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should return 0 batch', async () => {
    const response = await request(server.listen())
      .get(url.replace(':batchId', '8c0c9dbc0acbe4087f066989'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should list the batch', async () => {
    const batch = await createABatch('Summer 1', issuer._id)
    const response = await request(server.listen())
      .get(url.replace(':batchId', batch._id))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.certs.length).to.equal(0)
  })
})

describe('/api/v1/issuer/revokeBatch/:batchId', async () => {
  const url = '/api/v1/issuer/revokeBatch/:batchId'
  let issuerToken
  let issuer
  before(async () => {
    issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
  })

  after(async () => {
    await User.remove({})
  })

  afterEach(async () => {
    await Batch.remove({})
  })

  it('should not revoke the already revoked batch', async () => {
    const batch = await createABatch('Summer 1', issuer._id, true, true)
    const response = await request(server.listen())
      .get(url.replace(':batchId', batch._id))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should revoke the batch', async () => {
    const response = await request(server.listen())
      .get(url.replace(':batchId', 'xxx'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not revoke the batch is not revoke on ETH network', async () => {
    const batch = await createABatch('Summer 2', issuer._id, true)
    sinon.stub(certnetContract.getInstance().methods, 'issuedBatches')
    certnetContract.getInstance().methods.issuedBatches
      .withArgs('0x4ef50f2deb6965c2751e91573404c7c04ef211e91b1762c571297664e6f5eb6d').callsFake(() => ({
        async call () {
          return {
            revoked: false
          }
        }
      }))
    const response = await request(server.listen())
      .get(url.replace(':batchId', batch._id))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should revoke the batch', async () => {
    const batch = await createABatch('Summer 2', issuer._id, true)
    sinon.stub(certnetContract.getInstance().methods, 'issuedBatches')
    certnetContract.getInstance().methods.issuedBatches
      .withArgs('0x4ef50f2deb6965c2751e91573404c7c04ef211e91b1762c571297664e6f5eb6d').callsFake(() => ({
        async call () {
          return {
            revoked: true
          }
        }
      }))
    const response = await request(server.listen())
      .get(url.replace(':batchId', batch._id))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should revoke the batch', async () => {
    const batch = await createABatch('Summer 2', issuer._id, true)
    sinon.stub(certnetContract.getInstance().methods, 'issuedBatches')
    certnetContract.getInstance().methods.issuedBatches
      .withArgs('0x4ef50f2deb6965c2751e91573404c7c04ef211e91b1762c571297664e6f5eb6d').callsFake(() => ({
        async call () {
          return {
            revoked: true
          }
        }
      }))
    const response = await request(server.listen())
      .get(url.replace(':batchId', batch._id))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('invalid batchId', async () => {
    const response = await request(server.listen())
      .get(url.replace(':batchId', 'xxx'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('invalid batchId', async () => {
    const response = await request(server.listen())
      .get(url.replace(':batchId', '965c2751e91573404c7c04ef211e91b1762'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not revoke the already revoked batch', async () => {
    const batch = await createABatch('Summer 1', issuer._id, true, true)
    const response = await request(server.listen())
      .get(url.replace(':batchId', batch._id))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not revoke the batch is not existed on ETH network', async () => {
    const batch = await createABatch('Summer 2', issuer._id, true)
    sinon.stub(certnetContract.getInstance().methods, 'issuedBatches')
    certnetContract.getInstance().methods.issuedBatches
      .withArgs('0x4ef50f2deb6965c2751e91573404c7c04ef211e91b1762c571297664e6f5eb6d').callsFake(() => ({
        async call () {
          return undefined
        }
      }))
    const response = await request(server.listen())
      .get(url.replace(':batchId', batch._id))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not revoke the batch is not existed on ETH network', async () => {
    const batch = await createABatch('Summer 2', issuer._id, true)
    sinon.stub(certnetContract.getInstance().methods, 'issuedBatches')
    certnetContract.getInstance().methods.issuedBatches
      .withArgs('0x4ef50f2deb6965c2751e91573404c7c04ef211e91b1762c571297664e6f5eb6d').callsFake(() => ({
        async call () {
          return null
        }
      }))
    const response = await request(server.listen())
      .get(url.replace(':batchId', batch._id))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not revoke the batch is not existed on ETH network', async () => {
    const batch = await createABatch('Summer 2', issuer._id, true)
    const response = await request(server.listen())
      .get(url.replace(':batchId', batch._id))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not revoke the batch is not existed on ETH network', async () => {
    const batch = await createABatch('Summer 2', issuer._id, true)
    sinon.stub(certnetContract.getInstance().methods, 'issuedBatches')
    certnetContract.getInstance().methods.issuedBatches
      .withArgs('0x4ef50f2deb6965c2751e91573404c7c04ef211e91b1762c571297664e6f5eb6d').callsFake(() => ({
        async call () {
          return null
        }
      }))
    const response = await request(server.listen())
      .get(url.replace(':batchId', batch._id))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })
})


describe('/api/v1/issuer/revokeCertificate/:certId', async () => {
  const url = '/api/v1/issuer/revokeCertificate/:certId'
  let issuerToken
  let issuer
  before(async () => {
    issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
  })

  after(async () => {
    await User.remove({})
  })

  afterEach(async () => {
    await Batch.remove({})
  })

  it('should not revoke not existed certificate', async () => {
    const response = await request(server.listen())
      .get(url.replace(':certId', '5c0c9dbc0acbe4087f066989'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('invalid cert id', async () => {
    await request(server.listen())
      .get(url.replace(':certId', '###'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(404)
      .end()
  })

  it('should not revoke already revoked certificate', async () => {
    sinon.stub(Certificate, 'findById')
    Certificate.findById.withArgs('5c0ca0d40acbe4087f0669f1').callsFake(() => ({
      toObject: () => ({
        metadata: {
          revoked: true
        }
      })
    }))
    const response = await request(server.listen())
      .get(url.replace(':certId', '5c0ca0d40acbe4087f0669f1'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not revoke not existed certificate', async () => {
    const response = await request(server.listen())
      .get(url.replace(':certId', '5c0c9dbc0acbe4087f066989'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not revoke already revoked certificate', async () => {
    sinon.stub(Certificate, 'findById')
    Certificate.findById.withArgs('5c0ca0d40acbe4087f0669f1').callsFake(() => ({
      toObject: () => ({
        metadata: {
          revoked: true
        }
      })
    }))
    sinon.stub(certnetContract.getInstance().methods, 'revokedCertificates')
    certnetContract.getInstance().methods.revokedCertificates
      .withArgs('0xa5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e').callsFake(() => ({
        async call () {
          return {
            certificateHash: '0xa5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e'
          }
        }
      }))
    const response = await request(server.listen())
      .get(url.replace(':certId', '5c0ca0d40acbe4087f0669f1'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not revoke certificate that is not revoked on ETH network', async () => {
    sinon.stub(Certificate, 'findById')
    Certificate.findById.withArgs('5c0ca0d40acbe4087f0669f1').callsFake(() => ({
      toObject: () => ({
        metadata: {
          revoked: false
        },
        signature: {
          targetHash: 'a5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e'
        }
      })
    }))
    sinon.stub(certnetContract.getInstance().methods, 'revokedCertificates')
    certnetContract.getInstance().methods.revokedCertificates
      .withArgs('0xa5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e').callsFake(() => ({
        async call () {
          return {
            certificateHash: '0x0000000000000000000000000000000000000000000000000000000000000000'
          }
        }
      }))
    const response = await request(server.listen())
      .get(url.replace(':certId', '5c0ca0d40acbe4087f0669f1'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not revoke certificate that is not revoked on ETH network', async () => {
    sinon.stub(Certificate, 'findById')
    Certificate.findById.withArgs('5c0ca0d40acbe4087f0669f1').callsFake(() => ({
      toObject: () => ({
        metadata: {
          revoked: false
        },
        signature: {
          targetHash: 'a5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e'
        }
      })
    }))

    sinon.stub(certnetContract.getInstance().methods, 'revokedCertificates')
    certnetContract.getInstance().methods.revokedCertificates
      .withArgs('0xa5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e').callsFake(() => ({
        async call () {
          return {
            certificateHash: '0xabcdef'
          }
        }
      }))
    const response = await request(server.listen())
      .get(url.replace(':certId', '5c0ca0d40acbe4087f0669f1'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should not revoke certificate that is not revoked on ETH network', async () => {
    sinon.stub(Certificate, 'findById')
    Certificate.findById.withArgs('5c0ca0d40acbe4087f0669f1').callsFake(() => ({
      toObject: () => ({
        metadata: {
          revoked: false
        },
        signature: {
          targetHash: 'a5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e'
        }
      })
    }))

    sinon.stub(certnetContract.getInstance().methods, 'revokedCertificates')
    certnetContract.getInstance().methods.revokedCertificates
      .withArgs('0xa5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e').callsFake(() => ({
        async call () {
          return {
            certificateHash: '0x123456'
          }
        }
      }))
    const response = await request(server.listen())
      .get(url.replace(':certId', '5c0ca0d40acbe4087f0669f1'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should revoke certificate', async () => {
    sinon.stub(Certificate, 'findById')
    Certificate.findById.withArgs('5c0ca0d40acbe4087f0669f1').callsFake(() => ({
      toObject: () => ({
        metadata: {
          revoked: false
        },
        signature: {
          targetHash: 'a5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e'
        },
        content: {
          recipientInfo: {

          },
          issuerInfo: {

          },
          certName: 'hahaha'
        }
      })
    }))

    sinon.stub(certnetContract.getInstance().methods, 'revokedCertificates')
    certnetContract.getInstance().methods.revokedCertificates
      .withArgs('0xa5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e').callsFake(() => ({
        async call () {
          return {
            certificateHash: '0xa5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e'
          }
        }
      }))
    const response = await request(server.listen())
      .get(url.replace(':certId', '5c0ca0d40acbe4087f0669f1'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should revoke certificate', async () => {
    sinon.stub(Certificate, 'findById')
    Certificate.findById.withArgs('5c0ca0d40acbe4087f0669f1').callsFake(() => ({
      toObject: () => ({
        metadata: {
          revoked: false
        },
        signature: {
          targetHash: 'a5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e'
        },
        content: {
          recipientInfo: {

          },
          issuerInfo: {

          },
          certName: 'hahaha'
        }
      })
    }))

    sinon.stub(certnetContract.getInstance().methods, 'revokedCertificates')
    certnetContract.getInstance().methods.revokedCertificates
      .withArgs('0xa5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e').callsFake(() => ({
        async call () {
          return {
            certificateHash: '0xa5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e'
          }
        }
      }))
    const response = await request(server.listen())
      .get(url.replace(':certId', '5c0ca0d40acbe4087f0669f1'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should revoke certificate', async () => {
    sinon.stub(Certificate, 'findById')
    Certificate.findById.withArgs('5c0ca0d40acbe4087f0669f1').callsFake(() => ({
      toObject: () => ({
        metadata: {
          revoked: false
        },
        signature: {
          targetHash: 'a5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e'
        },
        content: {
          recipientInfo: {

          },
          issuerInfo: {

          },
          certName: 'hahaha'
        }
      })
    }))

    sinon.stub(certnetContract.getInstance().methods, 'revokedCertificates')
    certnetContract.getInstance().methods.revokedCertificates
      .withArgs('0xa5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e').callsFake(() => ({
        async call () {
          return {
            certificateHash: '0xa5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e'
          }
        }
      }))
    const response = await request(server.listen())
      .get(url.replace(':certId', '5c0ca0d40acbe4087f0669f1'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should revoke certificate', async () => {
    sinon.stub(Certificate, 'findById')
    Certificate.findById.withArgs('5c0ca0d40acbe4087f0669f1').callsFake(() => ({
      toObject: () => ({
        metadata: {
          revoked: false
        },
        signature: {
          targetHash: 'a5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e'
        },
        content: {
          recipientInfo: {

          },
          issuerInfo: {

          },
          certName: 'hahaha'
        }
      })
    }))

    sinon.stub(certnetContract.getInstance().methods, 'revokedCertificates')
    certnetContract.getInstance().methods.revokedCertificates
      .withArgs('0xa5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e').callsFake(() => ({
        async call () {
          return {
            certificateHash: '0xa5adfa613e427a87a20a606cd42c86aa7439172b104be47d86be3b4a28a16e2e'
          }
        }
      }))
    const response = await request(server.listen())
      .get(url.replace(':certId', '5c0ca0d40acbe4087f0669f1'))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })
})
