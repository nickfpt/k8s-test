/* eslint-disable no-undef */

'use strict'

const chai = require('chai')
const sinon = require('sinon')
const request = require('super-request')
const server = require('../../../../micro-services/web/server')
const { generateIssuerJWT, createIssuerUser, createABatch, createATemplate } = require('../../../factory')
const User = require('../../../../libs/mongodb/models/user')
const Certificate = require('../../../../libs/mongodb/models/certificate')
const Template = require('../../../../libs/mongodb/models/template')
const Batch = require('../../../../libs/mongodb/models/batch')

const expect = chai.expect

afterEach(() => {
  sinon.restore()
})

describe('/api/v1/issuer/createUnsignedCert', async () => {
  const url = '/api/v1/issuer/createUnsignedCert'
  let issuerToken
  let issuer
  before(async () => {
    issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
  })

  after(async () => {
    await User.remove({})
  })

  afterEach(async () => {
    await Certificate.remove({})
    await Template.remove({})
    await Batch.remove({})
  })

  it('should create a list of unsigned certificate', async () => {
    const batch = await createABatch('Summer 2031', issuer._id)
    const template = await createATemplate(issuer._id)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batchId: batch._id,
        templateId: template._id,
        header: ['Certificate name', 'Roll number', 'Recipient Name', 'Email', 'DoB', 'Ethereum Address', 'PoB', 'Gender', 'Nationality', 'Race', 'Major', 'Graduation Year', 'Graduation Grade', 'Mode of Study', 'Cert Id', 'Decision Id'],
        data: [
          ['Bachelor Degree', 'SE03459', 'Hoàng Đình Quang', 'quang@fpt.edu.vn', 34761, '0x4ead7baca6a23d88d3389876b620ca21b0678e77', 'Thanh Liên, Thanh Chương, Nghệ An', 1, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Good', 'Full-time', 12345, 'QD/FA2018'],
          ['Bachelor Degree', 'SE03460', 'Lê Hà Phan', 'phan@fpt.edu.vn', 35127, '0x607ed77b858d81582cb8732efdef1c6e61a481a0', 'Thanh Liên, Thanh Chương, Nghệ An', 1, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Good', 'Full-time', 12345, 'QD/FA2018'],
          ['Bachelor Degree', 'SE03461', 'Nguyễn Xuân Tiến', 'tien@fpt.edu.vn', 35492, '0xe2e55ea81d4c133b6f3e0ed7033c5746dcf9f05a', 'Thanh Liên, Thanh Chương, Nghệ An', 0, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Fairly Good', 'Full-time', 12345, 'QD/FA2018']
        ]
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('template not found', async () => {
    const batch = await createABatch('Summer 2031', issuer._id)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batchId: batch._id,
        templateId: '5c0f274d8c6d7207c26521c7',
        header: ['Certificate name', 'Roll number', 'Recipient Name', 'Email', 'DoB', 'Ethereum Address', 'PoB', 'Gender', 'Nationality', 'Race', 'Major', 'Graduation Year', 'Graduation Grade', 'Mode of Study', 'Cert Id', 'Decision Id'],
        data: [
          ['Bachelor Degree', 'SE03459', 'Hoàng Đình Quang', 'quang@fpt.edu.vn', 34761, '0x4ead7baca6a23d88d3389876b620ca21b0678e77', 'Thanh Liên, Thanh Chương, Nghệ An', 1, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Good', 'Full-time', 12345, 'QD/FA2018'],
          ['Bachelor Degree', 'SE03460', 'Lê Hà Phan', 'phan@fpt.edu.vn', 35127, '0x607ed77b858d81582cb8732efdef1c6e61a481a0', 'Thanh Liên, Thanh Chương, Nghệ An', 1, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Good', 'Full-time', 12345, 'QD/FA2018'],
          ['Bachelor Degree', 'SE03461', 'Nguyễn Xuân Tiến', 'tien@fpt.edu.vn', 35492, '0xe2e55ea81d4c133b6f3e0ed7033c5746dcf9f05a', 'Thanh Liên, Thanh Chương, Nghệ An', 0, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Fairly Good', 'Full-time', 12345, 'QD/FA2018']
        ]
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('invalid template id', async () => {
    const batch = await createABatch('Summer 2031', issuer._id)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batchId: batch._id,
        templateId: 'xxx',
        header: ['Certificate name', 'Roll number', 'Recipient Name', 'Email', 'DoB', 'Ethereum Address', 'PoB', 'Gender', 'Nationality', 'Race', 'Major', 'Graduation Year', 'Graduation Grade', 'Mode of Study', 'Cert Id', 'Decision Id'],
        data: [
          ['Bachelor Degree', 'SE03459', 'Hoàng Đình Quang', 'quang@fpt.edu.vn', 34761, '0x4ead7baca6a23d88d3389876b620ca21b0678e77', 'Thanh Liên, Thanh Chương, Nghệ An', 1, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Good', 'Full-time', 12345, 'QD/FA2018'],
          ['Bachelor Degree', 'SE03460', 'Lê Hà Phan', 'phan@fpt.edu.vn', 35127, '0x607ed77b858d81582cb8732efdef1c6e61a481a0', 'Thanh Liên, Thanh Chương, Nghệ An', 1, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Good', 'Full-time', 12345, 'QD/FA2018'],
          ['Bachelor Degree', 'SE03461', 'Nguyễn Xuân Tiến', 'tien@fpt.edu.vn', 35492, '0xe2e55ea81d4c133b6f3e0ed7033c5746dcf9f05a', 'Thanh Liên, Thanh Chương, Nghệ An', 0, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Fairly Good', 'Full-time', 12345, 'QD/FA2018']
        ]
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })
})

describe('/api/v1/issuer/signBatch', async () => {
  const url = '/api/v1/issuer/signBatch'
  let issuerToken
  let issuer
  before(async () => {
    issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
  })

  after(async () => {
    await User.remove({})
  })

  afterEach(async () => {
    await Certificate.remove({})
    await Template.remove({})
    await Batch.remove({})
  })

  it('should sign the certificate in batch', async () => {
    const batch = await createABatch('Summer 2031', issuer._id)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batchId: batch._id,
        transactionId: '0x67d35949888f273628754c6918adba637be14cf1f9679b8aa78a1d3807eab3b1',
        sender: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('batch is not found', async () => {
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batchId: '5c0f274d8c6d7207c26521c7',
        transactionId: '0x67d35949888f273628754c6918adba637be14cf1f9679b8aa78a1d3807eab3b1',
        sender: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('invalid batch id', async () => {
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batchId: 'xxx',
        transactionId: '0x67d35949888f273628754c6918adba637be14cf1f9679b8aa78a1d3807eab3b1',
        sender: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })
})

describe('/api/v1/issuer/buildMerkleTree/:batchId', async () => {
  const url = '/api/v1/issuer/buildMerkleTree/:batchId'
  let issuerToken
  let issuer
  before(async () => {
    issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
  })

  after(async () => {
    await User.remove({})
  })

  afterEach(async () => {
    await Certificate.remove({})
    await Template.remove({})
    await Batch.remove({})
  })

  it('should build the merkle tree of all certificates in batch', async () => {
    const batch = await createABatch('Summer 2031', issuer._id)
    const response = await request(server.listen())
      .get(url.replace(':batchId', batch._id))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should build the merkle tree of all certificates in batch', async () => {
    const batch = await createABatch('Summer 2031', issuer._id)
    const response = await request(server.listen())
      .get(url.replace(':batchId', batch._id))
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('batch is not found', async () => {
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batchId: '5c0f274d8c6d7207c26521c7',
        transactionId: '0x67d35949888f273628754c6918adba637be14cf1f9679b8aa78a1d3807eab3b1',
        sender: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('invalid batch id', async () => {
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batchId: 'xxx',
        transactionId: '0x67d35949888f273628754c6918adba637be14cf1f9679b8aa78a1d3807eab3b1',
        sender: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('batch is not found', async () => {
    const response = await request(server.listen())
      .get(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batchId: '5c0f274d8c6d7207c26521c7',
        transactionId: '0x67d35949888f273628754c6918adba637be14cf1f9679b8aa78a1d3807eab3b1',
        sender: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF'
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })
})

describe('/api/v1/issuer/createUnsignedCert', async () => {
  const url = '/api/v1/issuer/createUnsignedCert'
  let issuerToken
  let issuer
  before(async () => {
    issuer = await createIssuerUser()
    issuerToken = await generateIssuerJWT(issuer)
  })

  after(async () => {
    await User.remove({})
  })

  afterEach(async () => {
    await Certificate.remove({})
    await Template.remove({})
    await Batch.remove({})
  })

  it('should create a list of unsigned certificate', async () => {
    const batch = await createABatch('Summer 2031', issuer._id)
    const template = await createATemplate(issuer._id)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batchId: batch._id,
        templateId: template._id,
        header: ['Certificate name', 'Roll number', 'Recipient Name', 'Email', 'DoB', 'Ethereum Address', 'PoB', 'Gender', 'Nationality', 'Race', 'Major', 'Graduation Year', 'Graduation Grade', 'Mode of Study', 'Cert Id', 'Decision Id'],
        data: [
          ['Bachelor Degree', 'SE03459', 'Hoàng Đình Quang', 'quang@fpt.edu.vn', 34761, '0x4ead7baca6a23d88d3389876b620ca21b0678e77', 'Thanh Liên, Thanh Chương, Nghệ An', 1, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Good', 'Full-time', 12345, 'QD/FA2018'],
          ['Bachelor Degree', 'SE03460', 'Lê Hà Phan', 'phan@fpt.edu.vn', 35127, '0x607ed77b858d81582cb8732efdef1c6e61a481a0', 'Thanh Liên, Thanh Chương, Nghệ An', 1, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Good', 'Full-time', 12345, 'QD/FA2018'],
          ['Bachelor Degree', 'SE03461', 'Nguyễn Xuân Tiến', 'tien@fpt.edu.vn', 35492, '0xe2e55ea81d4c133b6f3e0ed7033c5746dcf9f05a', 'Thanh Liên, Thanh Chương, Nghệ An', 0, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Fairly Good', 'Full-time', 12345, 'QD/FA2018']
        ]
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('template not found', async () => {
    const batch = await createABatch('Summer 2031', issuer._id)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batchId: batch._id,
        templateId: '5c0f274d8c6d7207c26521c7',
        header: ['Certificate name', 'Roll number', 'Recipient Name', 'Email', 'DoB', 'Ethereum Address', 'PoB', 'Gender', 'Nationality', 'Race', 'Major', 'Graduation Year', 'Graduation Grade', 'Mode of Study', 'Cert Id', 'Decision Id'],
        data: [
          ['Bachelor Degree', 'SE03459', 'Hoàng Đình Quang', 'quang@fpt.edu.vn', 34761, '0x4ead7baca6a23d88d3389876b620ca21b0678e77', 'Thanh Liên, Thanh Chương, Nghệ An', 1, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Good', 'Full-time', 12345, 'QD/FA2018'],
          ['Bachelor Degree', 'SE03460', 'Lê Hà Phan', 'phan@fpt.edu.vn', 35127, '0x607ed77b858d81582cb8732efdef1c6e61a481a0', 'Thanh Liên, Thanh Chương, Nghệ An', 1, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Good', 'Full-time', 12345, 'QD/FA2018'],
          ['Bachelor Degree', 'SE03461', 'Nguyễn Xuân Tiến', 'tien@fpt.edu.vn', 35492, '0xe2e55ea81d4c133b6f3e0ed7033c5746dcf9f05a', 'Thanh Liên, Thanh Chương, Nghệ An', 0, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Fairly Good', 'Full-time', 12345, 'QD/FA2018']
        ]
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('invalid template id', async () => {
    const batch = await createABatch('Summer 2031', issuer._id)
    const response = await request(server.listen())
      .post(url)
      .headers({
        'x-access-token': issuerToken
      })
      .json(true)
      .body({
        batchId: batch._id,
        templateId: 'xxx',
        header: ['Certificate name', 'Roll number', 'Recipient Name', 'Email', 'DoB', 'Ethereum Address', 'PoB', 'Gender', 'Nationality', 'Race', 'Major', 'Graduation Year', 'Graduation Grade', 'Mode of Study', 'Cert Id', 'Decision Id'],
        data: [
          ['Bachelor Degree', 'SE03459', 'Hoàng Đình Quang', 'quang@fpt.edu.vn', 34761, '0x4ead7baca6a23d88d3389876b620ca21b0678e77', 'Thanh Liên, Thanh Chương, Nghệ An', 1, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Good', 'Full-time', 12345, 'QD/FA2018'],
          ['Bachelor Degree', 'SE03460', 'Lê Hà Phan', 'phan@fpt.edu.vn', 35127, '0x607ed77b858d81582cb8732efdef1c6e61a481a0', 'Thanh Liên, Thanh Chương, Nghệ An', 1, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Good', 'Full-time', 12345, 'QD/FA2018'],
          ['Bachelor Degree', 'SE03461', 'Nguyễn Xuân Tiến', 'tien@fpt.edu.vn', 35492, '0xe2e55ea81d4c133b6f3e0ed7033c5746dcf9f05a', 'Thanh Liên, Thanh Chương, Nghệ An', 0, 'Vietnam', 'Kinh', 'Software Engineering', 2018, 'Fairly Good', 'Full-time', 12345, 'QD/FA2018']
        ]
      })
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })
})
