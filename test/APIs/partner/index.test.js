/* eslint-disable no-undef */

'use strict'

const chai = require('chai')
const sinon = require('sinon')
const request = require('super-request')
const server = require('../../../micro-services/web/server')
const { createIssuerUser } = require('../../factory')
const User = require('../../../libs/mongodb/models/user')

const expect = chai.expect

afterEach(() => {
  sinon.restore()
})

describe('/issuers', async () => {
  const url = '/issuers'
  afterEach(async () => {
    await User.remove({})
  })

  it('should return all valid issuers', async () => {
    await createIssuerUser()
    const response = await request(server.listen())
      .get(url)
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.issuers.length).to.equal(1)
  })

  it('should return 0 issuer', async () => {
    const response = await request(server.listen())
      .get(url)
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.issuers.length).to.equal(0)
  })
})

describe('/issuers/:id', async () => {
  const url = '/issuers/:id'
  afterEach(async () => {
    await User.remove({})
  })

  it('should return all valid issuers', async () => {
    const issuer = await createIssuerUser()
    const response = await request(server.listen())
      .get(url.replace(':id', issuer._id))
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(true)
  })

  it('should return 0 issuer', async () => {
    const response = await request(server.listen())
      .get(url.replace(':id', 'xxx'))
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })

  it('should return 0 issuer', async () => {
    const response = await request(server.listen())
      .get(url.replace(':id', '5be1cf389a406d109d7e369b'))
      .json(true)
      .expect(200)
      .end()
    const { body } = response
    expect(body.ok).to.equal(false)
  })
})
