'use strict'

const chai = require('chai')
const { getHDWalletProvider } = require('../../utils/ethereum')

chai.use(require('chai-bytes'))

const expect = chai.expect

const test = {
  mnemonic: 'width slim violin silver lava this antique enter erase deny beef surge',
  httpEndpoint: 'https://ropsten.infura.io',
  accounts: [
    {
      address: '0x9e772dabc7b4bf6275a26a68645714c0dd8345b5',
      privateKey: '0x5e85492dac502f58a42f23fee737d516ce4a492ffe9eb4b4409346028d80b673'
    },
    {
      address: '0xe918caa44594b8b8d7ed559d6efce5c05e739b73',
      privateKey: '0xf32380655bc60492d1f3d7548c579805a288df639144a4c7fb98a51c51fa7395'
    },
    {
      address: '0x0b34ee8bae26abc7473f60ca9cdd39c6567038e2',
      privateKey: '0xa43bd825d20f1245938d472eea760a1accb65ea73a44fecedb7e211048a1871f'
    },
    {
      address: '0x9a8af754d293b4e130fbbce59b08e3239d46df47',
      privateKey: '0x58fa3643555e693f63369bdb9a138e14be39988a669e4cce689d3d5d02adefcc'
    },
    {
      address: '0x55bf3302cf6fe1af157c42d46b15743d9f52bfaa',
      privateKey: '0xc6c5223270eb18e50b5825bfa5a412f3120a0d68f6316049f3e4a7352c432fa0'
    },
    {
      address: '0x6b9ad9ca70b948d40fd0460175fb8726f48df913',
      privateKey: '0xb6d3682d650eebc8cebd67747cd2022f771dc99a0cf2a8c129194e7e6aa9de96'
    },
    {
      address: '0xa71cba23bb59288d2a294a8c3907a50b389d79dc',
      privateKey: '0xac39a4d10887341abd1db4c0c08de482ff8998c4de00e8692c6e5855e4055361'
    },
    {
      address: '0x638987d48f2d8feb464d215d9ce106c7fe0e5ad0',
      privateKey: '0x7a781cceabbaf063996c0708fab555cee5f4339c808f46a6a68e840b69413136'
    },
    {
      address: '0x232a579cc2b5d7c545885a30ca7c24d688602983',
      privateKey: '0xb78b537e91c63552929a5a9123f9cc48597dfbe1a466a01970ca8791a98d6706'
    },
    {
      address: '0x78ab096f7d2dbc8d18355f948d85a4774e7a8870',
      privateKey: '0x8aec8e2b4b2865833647a4cbcaf17efe2046e23927bfbbdcd47f6b3c429e283c'
    }
  ]
}


describe('utils/ethereum', () => {
  describe('getHDWalletProvider()', () => {
    it('Test', () => {
      const provider = getHDWalletProvider({
        mnemonic: test.mnemonic,
        httpEndpoint: test.httpEndpoint,
        numberOfAddresses: 10,
        defaultAddressIndex: 0
      })
      expect(provider.getAddresses()).to.eql(test.accounts.map((account) => account.address))
    })
  })
})
