'use strict'

const chai = require('chai')
const { toString } = require('../helper')
const { ArgumentError } = require('../../utils/error')
const { hexStringToByteArray, stringToUtf8ByteArray } = require('../../utils/conversion')

chai.use(require('chai-bytes'))

const expect = chai.expect

const tests = {
  hexStringToByteArray: [
    {
      input: '0xabcd',
      expected: Buffer.of(0xab, 0xcd)
    },
    {
      input: 'abcd',
      expected: Buffer.of(0xab, 0xcd)
    },
    {
      input: 'AbcD',
      expected: Buffer.of(0xab, 0xcd)
    },
    {
      input: 0xabcd,
      error: ArgumentError
    },
    {
      input: '',
      error: ArgumentError
    },
    {
      input: { name: 'Nguyen' },
      error: ArgumentError
    },
    {
      input: new Uint8Array([1, 2, 3, 4]),
      error: ArgumentError
    }
  ],
  stringToUtf8ByteArray: [
    {
      input: 'Hello World',
      expected: Buffer.of(0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64)
    },
    {
      input: 'おはようございます',
      expected: Buffer.of(0xE3, 0x81, 0x8A, 0xE3, 0x81, 0xAF, 0xE3, 0x82, 0x88, 0xE3, 0x81, 0x86, 0xE3, 0x81,
        0x94, 0xE3, 0x81, 0x96, 0xE3, 0x81, 0x84, 0xE3, 0x81, 0xBE, 0xE3, 0x81, 0x99)
    },
    {
      input: '',
      expected: Buffer.of()
    },
    {
      input: ['H', 'e', 'l', 'l', 'o'],
      error: ArgumentError
    },
    {
      input: '',
      expected: Buffer.of()
    }
  ]
}

describe('utils/conversion', () => {
  describe('hexStringToByteArray()', () => {
    tests.hexStringToByteArray.forEach((test) => {
      it(toString(test.input), () => {
        if (test.error) {
          expect(() => hexStringToByteArray(test.input)).to.throw(test.error)
        } else {
          expect(hexStringToByteArray(test.input)).to.equalBytes(test.expected)
        }
      })
    })
  })
  describe('stringToUtf8ByteArray()', () => {
    tests.stringToUtf8ByteArray.forEach((test) => {
      it(toString(test.input), () => {
        if (test.error) {
          expect(() => stringToUtf8ByteArray(test.input)).to.throw(test.error)
        } else {
          expect(stringToUtf8ByteArray(test.input)).to.equalBytes(test.expected)
        }
      })
    })
  })
})
