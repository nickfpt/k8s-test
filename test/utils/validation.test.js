'use strict'

const chai = require('chai')
const { toString } = require('../helper')
const { ArgumentError } = require('../../utils/error')
const {
  checkArgument,
  checkIsString,
  checkIsNonEmptyString,
  checkIsHexString,
  isString,
  isNonEmptyString,
  isHexString
} = require('../../utils/validation')

const expect = chai.expect

const tests = [
  {
    input: undefined,
    isTruthy: false,
    isString: false,
    isNonEmptyString: false,
    isHexString: false
  },
  {
    input: null,
    isTruthy: false,
    isString: false,
    isNonEmptyString: false,
    isHexString: false
  },
  {
    input: 0,
    isTruthy: false,
    isString: false,
    isNonEmptyString: false,
    isHexString: false
  },
  {
    input: 123,
    isTruthy: true,
    isString: false,
    isNonEmptyString: false,
    isHexString: false
  },
  {
    input: true,
    isTruthy: true,
    isString: false,
    isNonEmptyString: false,
    isHexString: false
  },
  {
    input: false,
    isTruthy: false,
    isString: false,
    isNonEmptyString: false,
    isHexString: false
  },
  {
    input: '',
    isTruthy: false,
    isString: true,
    isNonEmptyString: false,
    isHexString: false
  },
  {
    input: 'hello',
    isTruthy: true,
    isString: true,
    isNonEmptyString: true,
    isHexString: false
  },
  {
    input: 'abcd',
    isString: true,
    isNonEmptyString: true,
    isHexString: true
  },
  {
    input: '0xabcd',
    isString: true,
    isNonEmptyString: true,
    isHexString: false
  }
]

const argName = 'myArg'
const customErrorMessage = 'Invalid argument.'

describe('utils/validation', () => {
  describe('checkArgument()', () => {
    tests.forEach((test) => {
      it(toString(test.input), () => {
        if (test.isTruthy !== undefined) {
          if (test.isTruthy) {
            expect(() => checkArgument(test.input, argName)).to.not.throw()
          } else {
            expect(() => checkArgument(test.input, argName)).to.throw(ArgumentError, argName)
          }
        }
      })
    })
    it('Custom error message', () => {
      expect(() => checkArgument(false, argName, customErrorMessage)).to.throw(ArgumentError, customErrorMessage)
    })
  })
  describe('checkIsString()', () => {
    tests.filter((test) => test.isString !== undefined).forEach((test) => {
      it(toString(test.input), () => {
        if (test.isString) {
          expect(() => checkIsString(test.input, argName)).to.not.throw()
        } else {
          expect(() => checkIsString(test.input, argName)).to.throw(ArgumentError, argName)
        }
      })
    })
  })
  describe('checkIsNonEmptyString()', () => {
    tests.filter((test) => test.isNonEmptyString !== undefined).forEach((test) => {
      it(toString(test.input), () => {
        if (test.isNonEmptyString) {
          expect(() => checkIsNonEmptyString(test.input, argName)).to.not.throw()
        } else {
          expect(() => checkIsNonEmptyString(test.input, argName)).to.throw(ArgumentError, argName)
        }
      })
    })
  })
  describe('checkIsHexString()', () => {
    tests.filter((test) => test.isHexString !== undefined).forEach((test) => {
      it(toString(test.input), () => {
        if (test.isHexString) {
          expect(() => checkIsHexString(test.input, argName)).to.not.throw()
        } else {
          expect(() => checkIsHexString(test.input, argName)).to.throw(ArgumentError, argName)
        }
      })
    })
  })
  describe('isString()', () => {
    tests.filter((test) => test.isString !== undefined).forEach((test) => {
      it(toString(test.input), () => {
        expect(isString(test.input)).to.equal(test.isString)
      })
    })
  })
  describe('isNonEmptyString()', () => {
    tests.filter((test) => test.isNonEmptyString !== undefined).forEach((test) => {
      it(toString(test.input), () => {
        expect(isNonEmptyString(test.input)).to.equal(test.isNonEmptyString)
      })
    })
  })
  describe('isHexString()', () => {
    tests.filter((test) => test.isHexString !== undefined).forEach((test) => {
      it(toString(test.input), () => {
        expect(isHexString(test.input)).to.equal(test.isHexString)
      })
    })
  })
})
