'use strict'

const chai = require('chai')
const { toString } = require('../helper')
const { ArgumentError } = require('../../utils/error')
const { sha256 } = require('../../utils/crypto')

chai.use(require('chai-bytes'))

const expect = chai.expect

const tests = {
  sha256: [
    {
      input: '',
      expected: Buffer.from('e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 'hex')
    },
    {
      input: 'a',
      expected: Buffer.from('ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb', 'hex')
    },
    {
      input: Buffer.of(1, 2, 3, 4),
      expected: Buffer.from('9f64a747e1b97f131fabb6b447296c9b6f0201e79fb3c5356e6c77e89b6a806a', 'hex')
    },
    {
      input: new Uint8Array([10, 11, 12, 13]),
      expected: Buffer.from('b23549dda157801533d1d272da5ff88683bf1fbe6ee46deb3066bf55f7d05507', 'hex')
    },
    {
      input: 123,
      error: ArgumentError
    },
    {
      input: undefined,
      error: ArgumentError
    },
    {
      input: 'a',
      expected: Buffer.from('ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb', 'hex')
    }
  ]
}

describe('utils/crypto', () => {
  describe('sha256()', () => {
    tests.sha256.forEach((test) => {
      it(toString(test.input), () => {
        if (test.error) {
          expect(() => sha256(test.input)).to.throw(test.error)
        } else {
          expect(sha256(test.input)).to.equalBytes(test.expected)
        }
      })
    })
  })
})

