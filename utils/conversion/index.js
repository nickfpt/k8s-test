'use strict'

const { checkIsString, checkIsHexString } = require('../validation')

/**
 * Convert hex string to byte array.
 * @param {string} hexString The hex string
 * @returns {Buffer} The byte array
 */
function hexStringToByteArray (hexString) {
  checkIsString(hexString, 'hexString')
  if (hexString.startsWith('0x') || hexString.startsWith('0X')) {
    hexString = hexString.slice(2)
  }
  checkIsHexString(hexString, 'hexString')
  return Buffer.from(hexString, 'hex')
}

/**
 * Encode string in UTF8, then return byte array.
 * @param {string} str The input string
 * @returns {Buffer} The output byte array
 */
function stringToUtf8ByteArray (str) {
  checkIsString(str, 'str')
  return Buffer.from(str, 'utf8')
}

module.exports = {
  hexStringToByteArray,
  stringToUtf8ByteArray
}
