'use strict'

const { ArgumentError } = require('../error')

const HEX_STRING_REGEX = /^[a-fA-F0-9]+$/

/**
 * Check if value is a string.
 * @param {string} value The value
 * @returns {boolean} True/False
 */
function isString (value) {
  return typeof (value) === 'string'
}

/**
 * Check if value is a string, and is not empty.
 * @param {string} value The value
 * @returns {boolean} True/False
 */
function isNonEmptyString (value) {
  return isString(value) && value.length > 0
}

/**
 * Check if value is a hexadecimal string.
 * @param {string} value The value
 * @returns {boolean} True/False
 */
function isHexString (value) {
  return isString(value) && HEX_STRING_REGEX.test(value)
}

/**
 * Check if an expression is truthy, otherwise throw {@link ArgumentError}.
 * @param {*|boolean} expression The expression
 * @param {string} argName The argument name
 * @param {string|Error} customError The custom error message or {@link Error} object to be wrapped
 * @throws {ArgumentError} If the expression is falsey
 */
function checkArgument (expression, argName, customError = undefined) {
  argName = typeof (argName) === 'string' ? argName : undefined
  if (!expression) {
    if (customError instanceof Error) {
      throw new ArgumentError(argName, customError.message, customError)
    } else if (typeof (customError) === 'string') {
      throw new ArgumentError(argName, customError)
    } else {
      throw new ArgumentError(argName)
    }
  }
}

/**
 * Check if value is a string, or throw {@link ArgumentError}.
 * @param {string} value The value
 * @param argName The argument name
 * @param customError The custom error message or {@link Error} object to be wrapped
 * @throws {ArgumentError} If value is not a string
 */
function checkIsString (value, argName, customError = undefined) {
  checkArgument(isString(value), argName, customError || `Argument '${argName}' is not a string.`)
}

/**
 * Check if value is a string and is not empty, or throw {@link ArgumentError}.
 * @param {string} value The value
 * @param argName The argument name
 * @param customError The custom error message or {@link Error} object to be wrapped
 * @throws {ArgumentError} If value is either not a string or empty
 */
function checkIsNonEmptyString (value, argName, customError = undefined) {
  checkArgument(isNonEmptyString(value), argName, customError || `Argument '${argName}' is either not a string, or is empty.`)
}

/**
 * Check if value is a hexadecimal string, or throw {@link ArgumentError}.<br/>
 * Note that if the string is prefixed with '0x', it will not be considered as hexadecimal string.
 * @param {string} value The value
 * @param argName The argument name
 * @param customError The custom error message or {@link Error} object to be wrapped
 * @throws {ArgumentError} If value is not a hexadecimal string
 */
function checkIsHexString (value, argName, customError = undefined) {
  checkArgument(isHexString(value), argName, customError || `Argument '${argName}' is not a hexadecimal string.`)
}

module.exports = {
  isString,
  isNonEmptyString,
  isHexString,
  checkArgument,
  checkIsString,
  checkIsNonEmptyString,
  checkIsHexString
}
