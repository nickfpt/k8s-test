'use strict'

const { createHash } = require('crypto')
const { checkArgument } = require('../validation')

/**
 * Compute hash of data using SHA-256 algorithm.
 * @param {string|Buffer|Uint8Array} data The input data
 * @returns {Buffer} The computed hash.
 */
function sha256 (data) {
  checkArgument(typeof (data) === 'string' || data instanceof Buffer || data instanceof Uint8Array,
    'data', 'Argument \'data\' is not a string or byte array.')
  return createHash('sha256').update(data).digest()
}

module.exports = {
  sha256
}
