'use strict'

const stringify = require('json-stable-stringify')
const PasswordValidator = require('password-validator')
const { sha256 } = require('../crypto')
const { stringToUtf8ByteArray } = require('../conversion')

module.exports.isPasswordSatisfyPolicy = (password) => {
  const passwordSchema = new PasswordValidator()
  passwordSchema
      .is().min(8)
      .has()
      .uppercase()
      .has()
      .lowercase()
      .has()
      .digits()
      .has()
      .letters()
  return passwordSchema.validate(password)
}

module.exports.computeHash = (cert) => {
  const orderedCert = stringify(cert)
  return sha256(stringToUtf8ByteArray(orderedCert)).toString('hex')
}
