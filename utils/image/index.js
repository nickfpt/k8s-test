'use strict'

const Jimp = require('jimp')

/**
 * Load image from source.<br/>
 * If <i>source</i> is a Jimp instance, the function will return it. Otherwise it will read image from file or buffer.
 * @param {string|Buffer|Jimp.Jimp} source The source image path, buffer or Jimp instance.
 * @param {boolean} asBitmap If set to <b>true</b>, the function will return a {@link Jimp.Bitmap} object.
 * @returns {Object|Promise<Jimp.Jimp>} The Jimp instance, or {@link Jimp.Bitmap} object.
 */
async function loadImage (source, asBitmap = false) {
  const image = source instanceof Jimp ? source : Jimp.read(source)
  if (asBitmap) {
    return image.bitmap
  }
  return image
}

/**
 * Save image to file.
 * @param {string|Buffer|Jimp.Jimp} source The source image path, buffer or Jimp instance.
 * @param {string} destination The destination path.
 * @param {number} quality The target quality (0 - 100) if save image as JPG.
 * @returns {Promise<Jimp.Jimp>} The Jimp instance.
 */
async function saveImage (source, destination, quality) {
  const image = await loadImage(source)
  if (quality) {
    image.quality(quality)
  }
  await image.writeAsync(destination)
  return source
}

/**
 * Resize image.
 * @param {string|Buffer|Jimp.Jimp} source The source image path, buffer or Jimp instance.
 * @param {Object} options The resize options.
 * @param {number} options.width The target width.
 * @param {number} options.height The target height.
 * @param {number} options.quality The target quality (0 - 100) if save image as JPG.
 * @param {string} options.destination The destination path (if save image).
 * @returns {Promise<Jimp.Jimp>} The Jimp instance.
 */
async function resizeImage (source, options) {
  const { width, height, quality, destination } = options
  const image = await loadImage(source)
  image.resize(width || Jimp.AUTO, height || Jimp.AUTO)
  if (destination) {
    await saveImage(image, destination, quality)
  }
  return image
}

/**
 * Crop image.
 * @param {string|Buffer|Jimp.Jimp} source The source image path, buffer or Jimp instance.
 * @param {Object} options The crop options.
 * @param {number} options.x The starting X.
 * @param {number} options.y The target Y.
 * @param {number} options.width The crop width.
 * @param {number} options.height The crop height.
 * @param {number} options.quality The target quality (0 - 100) if save image as JPG.
 * @param {string} options.destination The destination path (if save image).
 * @returns {Promise<Jimp.Jimp>} The Jimp instance.
 */
async function cropImage (source, options) {
  const { x, y, width, height, quality, destination } = options
  const image = await loadImage(source)
  image.crop(x, y, width, height)
  if (destination) {
    await saveImage(image, destination, quality)
  }
  return image
}

/**
 * Convert image to {@link Buffer}.
 * @param {string|Buffer|Jimp.Jimp} source The source image path, buffer or Jimp instance.
 * @param {Object} options The crop options.
 * @param {string} options.mime The MIME type of the target image.
 * Supported MIME types are image/bmp, image/jpeg & image/png.
 * @param {number} options.quality The target quality (0 - 100) if convert to JPG.
 * @returns {Promise<Buffer>} The image buffer.
 */
async function imageToBuffer (source, options) {
  const image = await loadImage(source)
  const { mime, quality } = options || {}
  if (quality) {
    image.quality(quality)
  }
  return image.getBufferAsync(mime || Jimp.AUTO)
}

module.exports = {
  loadImage,
  saveImage,
  resizeImage,
  cropImage,
  imageToBuffer
}
