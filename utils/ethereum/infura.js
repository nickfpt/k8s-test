/* eslint-disable no-prototype-builtins*/

'use strict'

const util = require('util')
const { checkArgument, isString, isHexString } = require('../validation')

const INFURA_HTTP_ENDPOINT_NOAPIKEY_FORMAT = 'https://%s.infura.io'
const INFURA_HTTP_ENDPOINT_FORMAT = 'https://%s.infura.io/v3/%s'
const INFURA_WS_ENDPOINT_FORMAT = 'wss://%s.infura.io/ws'

const INFURA_NETWORKS = {
  mainnet: 1,
  ropsten: 3,
  rinkeby: 21,
  kovan: 42
}

/**
 * Check whether the argument is a supported network by Infura.
 * @param {string} network The network name
 * @returns {boolean} True/False
 */
function isValidNetwork (network) {
  return isString(network) && INFURA_NETWORKS.hasOwnProperty(network)
}

/**
 * Check whether the argument is a valid Infura API key.
 * @param {string} apiKey The API key
 * @returns {boolean} True/False
 */
function isValidApiKey (apiKey) {
  return isHexString(apiKey) && apiKey.length === 32
}

function checkIsValidNetwork (network, argName) {
  checkArgument(isValidNetwork(network), argName, `Argument '${argName}' is either not valid, or not supported by Infura.`)
}

function checkIsValidApiKey (apiKey, argName) {
  checkArgument(isValidApiKey(apiKey), argName, `Argument '${argName}' iis not a valid Infura API key.`)
}

/**
 * Get the list of supported Infura networks.
 * @returns {Array<string>} The list of network names.
 */
function getSupportedNetworks () {
  return Object.keys(INFURA_NETWORKS)
}

/**
 * Get network ID.
 * @param {string} network The network name
 * @returns {number} The network ID
 */
function getNetworkId (network) {
  checkIsValidNetwork(network, 'network')
  return INFURA_NETWORKS[network]
}

/**
 * Get Infura HTTP endpoint.
 * @param {string} network The network name
 * @param {string} apiKey The API key
 * @returns {string} The HTTP endpoint
 */
function getHttpEndpoint (network, apiKey = undefined) {
  checkIsValidNetwork(network, 'network')
  if (apiKey) {
    checkIsValidApiKey(apiKey, 'apiKey')
  }
  return apiKey
    ? util.format(INFURA_HTTP_ENDPOINT_FORMAT, network, apiKey)
    : util.format(INFURA_HTTP_ENDPOINT_NOAPIKEY_FORMAT, network)
}

/**
 * Get Infura WebSocket endpoint.
 * @param {string} network The network name
 * @param {string} apiKey The API key
 * @returns {string} The WebSocket endpoint
 */
function getWebSocketEndpoint (network) {
  checkIsValidNetwork(network, 'network')
  return util.format(INFURA_WS_ENDPOINT_FORMAT, network)
}

module.exports = {
  isValidNetwork,
  isValidApiKey,
  getSupportedNetworks,
  getNetworkId,
  getHttpEndpoint,
  getWebSocketEndpoint
}
