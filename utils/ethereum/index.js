'use strict'

const HDWalletProvider = require('truffle-hdwallet-provider')
const Joi = require('joi')
const { checkArgument } = require('../validation')
const infura = require('./infura')

/**
 * Create {@link HDWalletProvider} instance.
 * @param {string} params.httpEndpoint The HTTP endpoint
 * @param {string} params.mnemonic The mnemonic
 * @param {Array<string>} params.privateKeys The private keys to be imported if mnemonic does not exist
 * @param {number} params.numberOfAddresses Number of addresses - private keys to be generated from mnemonic
 * @param {number} params.defaultAddressIndex 0-based index of the default address - private key
 * @returns {HDWalletProvider} The provider
 */
function getHDWalletProvider (params) {
  // Validate parameter object
  const result = Joi.validate(params, Joi.object({
    httpEndpoint: Joi.string().uri().required(),
    mnemonic: Joi.string().default(null),
    privateKeys: Joi.array().items(Joi.string().hex().length(64)),
    numberOfAddresses: Joi.number().integer().min(1).default(1),
    defaultAddressIndex: Joi.number().integer().min(0).default(0)
  }))
  checkArgument(result.error === null, 'params', result.error)
  params = result.value
  return params.privateKeys
    ? new HDWalletProvider(params.privateKeys, params.httpEndpoint, params.defaultAddressIndex, params.privateKeys.length)
    : new HDWalletProvider(params.mnemonic, params.httpEndpoint, params.defaultAddressIndex, params.numberOfAddresses)
}

module.exports = {
  infura,
  getHDWalletProvider
}
