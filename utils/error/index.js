'use strict'

const { BaseError } = require('make-error-cause')

/**
 * Class to represent error in function argument.
 */
class ArgumentError extends BaseError {
  constructor (argumentName, message, cause) {
    super(message || `Argument '${argumentName}' does not meet the requirement(s).`, cause)
    if (argumentName) {
      Object.defineProperty(this, 'argumentName', { value: argumentName, writable: false })
    }
  }
}

module.exports = {
  ArgumentError
}
