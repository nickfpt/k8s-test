/* eslint-disable global-require */

'use strict'

const type = process.env.PROCESS_TYPE

console.log(`Starting '${type}' process`, { pid: process.pid })

if (type === 'web') {
  require('./micro-services/web')
} else if (type === 'send-email-worker') {
  require('./micro-services/workers/send-email')
} else if (type === 'delete-expired-token-worker') {
  require('./micro-services/workers/delete-expired-token')
} else if (type === 'generate-visual-certificate-worker') {
  require('./micro-services/workers/generate-visual-certificate')
} else {
  throw new Error(`${type} is an unsupported process type. Use one of:
      - web
      - send-email-worker
      - delete-expired-token-worker
      - generate-visual-certificate-worker
  `)
}
