pragma solidity ^0.4.23;

contract CertNet {
    /*--ERROR CODES--*/

    string constant INVALID_ISSUER_ADDRESS = "INVALID_ISSUER_ADDRESS";
    string constant INVALID_MERKLE_ROOT_HASH = "INVALID_MERKLE_ROOT_HASH";
    string constant INVALID_CERTIFICATE_HASH = "INVALID_CERTIFICATE_HASH";
    string constant INVALID_PROOF_ARGUMENT = "INVALID_PROOF_ARGUMENT";
    string constant CALLER_IS_NOT_OWNER = "CALLER_IS_NOT_OWNER";
    string constant BATCH_ALREADY_ISSUED = "BATCH_ALREADY_ISSUED";
    string constant BATCH_NOT_FOUND = "BATCH_NOT_FOUND";
    string constant BATCH_ALREADY_REVOKED = "BATCH_ALREADY_REVOKED";
    string constant BATCH_NOT_ISSUED_BY_CALLER = "BATCH_NOT_ISSUED_BY_CALLER";
    string constant CERTIFICATE_ALREADY_REVOKED = "CERTIFICATE_ALREADY_REVOKED";
    string constant INCORRECT_CERTIFICATE_PROOF = "INCORRECT_CERTIFICATE_PROOF";

    /*--STRUCTS--*/

    struct CertificateBatchInfo {
        bytes32 merkleRootHash;
        uint createdTimestamp;
        address issuerAddress;
        bool revoked;
        uint revokedTimestamp;
    }

    struct CertificateRevocationInfo {
        bytes32 certificateHash;
        bytes32 merkleRootHash;
        uint revokedTimestamp;
    }

    /*--EVENTS--*/

    event CertificateBatchIssued (
        bytes32 indexed merkleRootHash,
        uint createdTimestamp,
        address issuerAddress
    );

    event CertificateBatchRevoked (
        bytes32 indexed merkleRootHash,
        uint revokedTimestamp,
        address issuerAddress
    );

    event CertificateRevoked (
        bytes32 indexed certificateHash,
        bytes32 merkleRootHash,
        uint revokedTimestamp,
        address issuerAddress
    );

    /*--STORAGE VARIABLES--*/

    /**
     * The owner of the contract.
     */
    address public owner;

    /**
     * Dictionary of issued certificate batches.
     */
    mapping (bytes32 => CertificateBatchInfo) public issuedBatches;

    /**
     * Dictionary of revoked certificates.
     */
    mapping (bytes32 => CertificateRevocationInfo) public revokedCertificates;

    /*--MODIFIERS--*/

    /**
     * Require caller of the method to be contract's owner.
     */
    modifier callerMustBeOwner() {
        require(msg.sender == owner, CALLER_IS_NOT_OWNER);
        _;
    }

    /**
     * Require Merkle root hash must not be zero.
     */
    modifier validateMerkleRootHash(bytes32 merkleRootHash) {
        require(merkleRootHash != bytes32(0), INVALID_MERKLE_ROOT_HASH);
        _;
    }

    /**
     * Require certificate hash must not be zero.
     */
    modifier validateCertificateHash(bytes32 certificateHash) {
        require(certificateHash != bytes32(0), INVALID_CERTIFICATE_HASH);
        _;
    }

    /*--PUBLIC FUNCTIONS--*/

    /**
     * Create the smart contract.
     */
    constructor() public {
        owner = msg.sender;
    }

    /**
     * Issue a certificate batch by storing its Merkle root.
     */
    function issueBatch(bytes32 merkleRootHash) public validateMerkleRootHash(merkleRootHash) {
        require(msg.sender != address(0), INVALID_ISSUER_ADDRESS);
        require(issuedBatches[merkleRootHash].merkleRootHash == 0, BATCH_ALREADY_ISSUED);

        CertificateBatchInfo memory batchInfo = CertificateBatchInfo({
            merkleRootHash: merkleRootHash,
            createdTimestamp: now,
            issuerAddress: msg.sender,
            revoked: false,
            revokedTimestamp: 0
        });
        issuedBatches[merkleRootHash] = batchInfo;

        emit CertificateBatchIssued(merkleRootHash, now, msg.sender);
    }

    /**
     * Revoke a certificate batch.
     */
    function revokeBatch(bytes32 merkleRootHash) public validateMerkleRootHash(merkleRootHash) {
        CertificateBatchInfo storage batchInfo = issuedBatches[merkleRootHash];
        require(batchInfo.merkleRootHash == merkleRootHash, BATCH_NOT_FOUND);
        require(batchInfo.issuerAddress == msg.sender, BATCH_NOT_ISSUED_BY_CALLER);
        require(!batchInfo.revoked, BATCH_ALREADY_REVOKED);

        batchInfo.revoked = true;
        batchInfo.revokedTimestamp = now;

        emit CertificateBatchRevoked(merkleRootHash, now, msg.sender);
    }

    /**
     * Revoke a single certificate.
     */
    function revokeSingleCertificate(bytes32 certificateHash, bytes32 merkleRootHash, bytes proof) public
            validateCertificateHash(certificateHash) validateMerkleRootHash(merkleRootHash) {
        CertificateBatchInfo storage batchInfo = issuedBatches[merkleRootHash];
        require(batchInfo.merkleRootHash == merkleRootHash, BATCH_NOT_FOUND);
        require(batchInfo.issuerAddress == msg.sender, BATCH_NOT_ISSUED_BY_CALLER);
        require(!batchInfo.revoked, BATCH_ALREADY_REVOKED);
        require(revokedCertificates[certificateHash].certificateHash == 0, CERTIFICATE_ALREADY_REVOKED);
        require(verifyProofOrderedSha256(proof, certificateHash, merkleRootHash), INCORRECT_CERTIFICATE_PROOF);

        CertificateRevocationInfo memory revocationInfo = CertificateRevocationInfo({
            certificateHash: certificateHash,
            merkleRootHash: merkleRootHash,
            revokedTimestamp: now
        });
        revokedCertificates[certificateHash] = revocationInfo;

        emit CertificateRevoked(certificateHash, merkleRootHash, now, msg.sender);
    }

    /**
     * Destroy the contract.
     */
    function destroy() public callerMustBeOwner {
        selfdestruct(owner);
    }

    /*--PRIVATE FUNCTIONS--*/

    /**
     * Verify if the proof is valid and correctly connects the target hash to the Merkle root of an ordered Merkle tree.
     *
     * An ordered Merkle tree is a Merkle tree whose leaves remain the order they were added. For this kind of tree, proof must contain
     * the hashes, as well as their relative positions on the leaf-to-root path.
     *
     * Here proof is stored as a byte array whose length is divisible by 33. Each group of 33 bytes represents the combinatory hashes
     * along the path to the root of the Merkle tree. The first byte denotes whether the hash is on the left (0) or right (1)
     * when combine with the current hash during reverse traversal. The remaining 32 bytes contains the hash (using sha256
     * or keccak256 algorithm).
     *
     * @param proof The proof.
     * @param targetHash The target hash.
     * @param merkleRoot The Merkle root.
     * @param useKeccak Determine whether the underlying hash algorithm of the Merkle tree is sha256 (false) or keccak256 (true).
     * @return True if the proof is correct, otherwise false.
     */
    function verifyProofOrdered(bytes proof, bytes32 targetHash, bytes32 merkleRoot, bool useKeccak) private pure returns (bool) {
        uint proofLength = proof.length;
        require(proofLength > 0 && proofLength % 33 == 0, INVALID_PROOF_ARGUMENT);
        bytes memory buffer = new bytes(64);
        bytes32 currentHash = targetHash;
        for (uint i = 0; i < proofLength; i += 33) {
            uint pos = i + 33;
            // If the combinatory hash is on the left, copy it to the left of the buffer, then copy the current hash to the right
            // of the buffer. Otherwise, do the reverse.
            if (proof[i] == 0) {
                assembly {
                    mstore(add(buffer, 32), mload(add(proof, pos)))
                    mstore(add(buffer, 64), currentHash)
                }
            } else {
                assembly {
                    mstore(add(buffer, 32), currentHash)
                    mstore(add(buffer, 64), mload(add(proof, pos)))
                }
            }
            // Compute the current hash while traversing.
            if (useKeccak) {
                currentHash = keccak256(buffer);
            } else {
                currentHash = sha256(buffer);
            }

        }
        return currentHash == merkleRoot;
    }

    /**
     * Verify if the proof is valid and correctly connects the target hash to the Merkle root of an ordered Keccak256-based Merkle tree.
     */
    function verifyProofOrderedKeccak256(bytes proof, bytes32 targetHash, bytes32 merkleRoot) private pure returns (bool) {
        return verifyProofOrdered(proof, targetHash, merkleRoot, true);
    }

    /**
     * Verify if the proof is valid and correctly connects the target hash to the Merkle root of an ordered SHA256-based Merkle tree.
     */
    function verifyProofOrderedSha256(bytes proof, bytes32 targetHash, bytes32 merkleRoot) private pure returns (bool) {
        return verifyProofOrdered(proof, targetHash, merkleRoot, false);
    }
}
