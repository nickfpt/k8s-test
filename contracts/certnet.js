'use strict'

const Web3 = require('web3')
const { getHDWalletProvider } = require('../utils/ethereum')
const { ETHEREUM: { DEFAULT_HTTP_ENDPOINT, DEFAULT_NETWORK_ID } } = require('../constants')
const contractSchema = require('../build/contracts/CertNet.json')

const provider = getHDWalletProvider({
  mnemonic: process.env.MNEMONIC,
  httpEndpoint: DEFAULT_HTTP_ENDPOINT,
  numberOfAddresses: 10,
  defaultAddressIndex: 0
})
const web3 = new Web3(provider)

const Contract = (function () {
  let instance
  function createInstance () {
    return new web3.eth.Contract(contractSchema.abi, contractSchema.networks[DEFAULT_NETWORK_ID].address)
  }

  return {
    getInstance () {
      if (!instance) {
        instance = createInstance()
      }
      return instance
    }
  }
}())

module.exports = Contract
