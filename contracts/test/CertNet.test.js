'use strict'

/* eslint-disable no-undef */

const chai = require('chai')

const assert = chai.assert

chai.use(require('chai-datetime'))

const CertNet = artifacts.require('./CertNet.sol')

const errorRegex = /^VM Exception while processing transaction: revert (.+)$/

const batchData1 = {
  merkleRootHash: '0xd71f8983ad4ee170f8129f1ebcdd7440be7798d8e1c80420bf11f1eced610dba',
  certificates: [
    {
      data: 'a',
      hash: '0xca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb',
      proof: '0x013e23e8160039594a33894f6564e1b1348bbd7a0088d42c4acb73eeaed59c009d01bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b013f79bb7b435b05321651daefd374cdc681dc06faa65e374e38337b88ca046dea'
    },
    {
      data: 'b',
      hash: '0x3e23e8160039594a33894f6564e1b1348bbd7a0088d42c4acb73eeaed59c009d',
      proof: '0x00ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb01bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b013f79bb7b435b05321651daefd374cdc681dc06faa65e374e38337b88ca046dea'
    },
    {
      data: 'c',
      hash: '0x2e7d2c03a9507ae265ecf5b5356885a53393a2029d241394997265a1a25aefc6',
      proof: '0x0118ac3e7343f016890c510e93f935261169d9e3f565436429830faf0934f4f8e400e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a013f79bb7b435b05321651daefd374cdc681dc06faa65e374e38337b88ca046dea'
    },
    {
      data: 'd',
      hash: '0x18ac3e7343f016890c510e93f935261169d9e3f565436429830faf0934f4f8e4',
      proof: '0x002e7d2c03a9507ae265ecf5b5356885a53393a2029d241394997265a1a25aefc600e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a013f79bb7b435b05321651daefd374cdc681dc06faa65e374e38337b88ca046dea'
    },
    {
      data: 'e',
      hash: '0x3f79bb7b435b05321651daefd374cdc681dc06faa65e374e38337b88ca046dea',
      proof: '0x0014ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7'
    }
  ]
}

const batchData2 = {
  merkleRootHash: '0x80285644ea6e999deb6a60f1b4d16d03d611f46ffc1c390a929463cbe1c33c5c',
  certificates: [
    {
      data: '1',
      hash: '0x6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b',
      proof: '0x01d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab350120ab747d45a77938a5b84c2944b8f5355c49f21db0c549451c6281c91ba48d0d01ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d'
    },
    {
      data: '2',
      hash: '0xd4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35',
      proof: '0x006b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b0120ab747d45a77938a5b84c2944b8f5355c49f21db0c549451c6281c91ba48d0d01ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d'
    },
    {
      data: '3',
      hash: '0x4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce',
      proof: '0x014b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a004295f72eeb1e3507b8461e240e3b8d18c1e7bd2f1122b11fc9ec40a65894031a01ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d'
    },
    {
      data: '4',
      hash: '0x4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a',
      proof: '0x004e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce004295f72eeb1e3507b8461e240e3b8d18c1e7bd2f1122b11fc9ec40a65894031a01ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d'
    },
    {
      data: '5',
      hash: '0xef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d',
      proof: '0x00cd53a2ce68e6476c29512ea53c395c7f5d8fbcb4614d89298db14e2a5bdb5456'
    }
  ]
}

const zeroHash = '0x0000000000000000000000000000000000000000000000000000000000000000'

function getErrorCode (err) {
  if (!err || !err.message) {
    return undefined
  }
  const match = err.message.match(errorRegex)
  if (!match) {
    return undefined
  }
  return match[1]
}

function toDate (numberOfSeconds) {
  return new Date(numberOfSeconds * 1000)
}

function toCertificateBatchInfo (callResult) {
  if (callResult[0] === zeroHash) {
    return undefined
  }
  return {
    merkleRootHash: callResult[0],
    createdTimestamp: toDate(callResult[1].toNumber()),
    issuerAddress: callResult[2],
    revoked: callResult[3],
    revokedTimestamp: toDate(callResult[4].toNumber())
  }
}

function toCertificateRevocationInfo (callResult) {
  if (callResult[0] === zeroHash) {
    return undefined
  }
  return {
    certificateHash: callResult[0],
    merkleRootHash: callResult[1],
    revokedTimestamp: toDate(callResult[2].toNumber())
  }
}

function failNoErrorThrown () {
  assert.fail('Expected function to throw error, but no error was thrown.')
}

contract('CertNet', (accounts) => {
  it('owner(): Owner by default must be first account', async () => {
    const instance = await CertNet.new()
    const owner = await instance.owner()
    assert.equal(owner, accounts[0])
  })

  it('owner(): Owner must be set as the deployer', async () => {
    const instance = await CertNet.new({
      from: accounts[1]
    })
    const owner = await instance.owner()
    assert.equal(owner, accounts[1])
  })

  it('issueBatch(): Issue batch success', async () => {
    const issuerAddress = accounts[1]
    const merkleRootHash = batchData1.merkleRootHash
    const instance = await CertNet.new()

    const result = await instance.issueBatch(merkleRootHash, { from: issuerAddress })
    // Assert event emitted
    assert.isNotEmpty(result.logs)
    assert.equal(result.logs.length, 1)
    const log = result.logs[0]
    assert.equal(log.event, 'CertificateBatchIssued')
    assert.equal(log.args.merkleRootHash, merkleRootHash)
    assert.isAbove(log.args.createdTimestamp.toNumber(), 0)
    assert.equal(log.args.issuerAddress, issuerAddress)
    const createdTimestamp = toDate(log.args.createdTimestamp.toNumber())
    // Assert lock status changed
    const batchInfo = toCertificateBatchInfo(await instance.issuedBatches(merkleRootHash))
    assert.equal(batchInfo.merkleRootHash, merkleRootHash)
    assert.equalTime(batchInfo.createdTimestamp, createdTimestamp)
    assert.equal(batchInfo.issuerAddress, issuerAddress)
    assert.equal(batchInfo.revoked, false)
    assert.equalTime(batchInfo.revokedTimestamp, new Date(0))
  })

  it('issueBatch(): Call with zero Merkle root hash must fail', async () => {
    const issuerAddress = accounts[1]
    const instance = await CertNet.new()
    try {
      await instance.issueBatch(zeroHash, { from: issuerAddress })
      failNoErrorThrown()
    } catch (err) {
      const errorCode = getErrorCode(err)
      assert.equal(errorCode, 'INVALID_MERKLE_ROOT_HASH')
    }
  })

  it('issueBatch(): Issue an issued batch must fail', async () => {
    const issuerAddress = accounts[1]
    const merkleRootHash = batchData1.merkleRootHash
    const instance = await CertNet.new()
    await instance.issueBatch(merkleRootHash, { from: issuerAddress })
    try {
      await instance.issueBatch(merkleRootHash, { from: issuerAddress })
      failNoErrorThrown()
    } catch (err) {
      const errorCode = getErrorCode(err)
      assert.equal(errorCode, 'BATCH_ALREADY_ISSUED')
    }
  })

  it('revokeBatch(): Revoke batch success', async () => {
    const issuerAddress = accounts[1]
    const merkleRootHash = batchData1.merkleRootHash
    const instance = await CertNet.new()
    await instance.issueBatch(merkleRootHash, { from: issuerAddress })

    const result = await instance.revokeBatch(merkleRootHash, { from: issuerAddress })
    // Assert event emitted
    assert.isNotEmpty(result.logs)
    assert.equal(result.logs.length, 1)
    const log = result.logs[0]
    assert.equal(log.event, 'CertificateBatchRevoked')
    assert.equal(log.args.merkleRootHash, merkleRootHash)
    assert.isAbove(log.args.revokedTimestamp.toNumber(), 0)
    assert.equal(log.args.issuerAddress, issuerAddress)
    const revokedTimestamp = toDate(log.args.revokedTimestamp.toNumber())
    // Assert lock status changed
    const batchInfo = toCertificateBatchInfo(await instance.issuedBatches(merkleRootHash))
    assert.equal(batchInfo.revoked, true)
    assert.equalTime(batchInfo.revokedTimestamp, revokedTimestamp)
  })

  it('revokeBatch(): Revoke non-existing batch must fail', async () => {
    const issuerAddress = accounts[1]
    const merkleRootHash = batchData1.merkleRootHash
    const otherHash = batchData2.merkleRootHash
    const instance = await CertNet.new()
    await instance.issueBatch(merkleRootHash, { from: issuerAddress })

    try {
      await instance.revokeBatch(otherHash, { from: issuerAddress })
      failNoErrorThrown()
    } catch (err) {
      const errorCode = getErrorCode(err)
      assert.equal(errorCode, 'BATCH_NOT_FOUND')
    }
  })

  it('revokeBatch(): Revoke batch issued by a different issuer must fail', async () => {
    const issuerAddress = accounts[1]
    const otherIssuerAddress = accounts[2]
    const merkleRootHash = batchData1.merkleRootHash
    const instance = await CertNet.new()
    await instance.issueBatch(merkleRootHash, { from: issuerAddress })
    try {
      await instance.revokeBatch(merkleRootHash, { from: otherIssuerAddress })
      failNoErrorThrown()
    } catch (err) {
      const errorCode = getErrorCode(err)
      assert.equal(errorCode, 'BATCH_NOT_ISSUED_BY_CALLER')
    }
  })

  it('revokeBatch(): Revoke already-revoked batch must fail', async () => {
    const issuerAddress = accounts[1]
    const merkleRootHash = batchData1.merkleRootHash
    const instance = await CertNet.new()
    await instance.issueBatch(merkleRootHash, { from: issuerAddress })
    await instance.revokeBatch(merkleRootHash, { from: issuerAddress })
    try {
      await instance.revokeBatch(merkleRootHash, { from: issuerAddress })
      failNoErrorThrown()
    } catch (err) {
      const errorCode = getErrorCode(err)
      assert.equal(errorCode, 'BATCH_ALREADY_REVOKED')
    }
  })

  it('revokeSingleCertificate(): Revoke certificate success', async () => {
    const issuerAddress = accounts[1]
    const merkleRootHash = batchData1.merkleRootHash
    const certificateHash = batchData1.certificates[0].hash
    const proof = batchData1.certificates[0].proof
    const instance = await CertNet.new()
    await instance.issueBatch(merkleRootHash, { from: issuerAddress })

    const result = await instance.revokeSingleCertificate(certificateHash, merkleRootHash, proof, { from: issuerAddress })
    // Assert event emitted
    assert.isNotEmpty(result.logs)
    assert.equal(result.logs.length, 1)
    const log = result.logs[0]
    assert.equal(log.event, 'CertificateRevoked')
    assert.equal(log.args.certificateHash, certificateHash)
    assert.equal(log.args.merkleRootHash, merkleRootHash)
    assert.isAbove(log.args.revokedTimestamp.toNumber(), 0)
    assert.equal(log.args.issuerAddress, issuerAddress)
    const revokedTimestamp = toDate(log.args.revokedTimestamp.toNumber())
    // Assert lock status changed
    const revocationInfo = toCertificateRevocationInfo(await instance.revokedCertificates(certificateHash))
    assert.equal(revocationInfo.certificateHash, certificateHash)
    assert.equal(revocationInfo.merkleRootHash, merkleRootHash)
    assert.equalTime(revocationInfo.revokedTimestamp, revokedTimestamp)
  })

  it('revokeSingleCertificate(): Revoke certificate with zero hash must fail', async () => {
    const issuerAddress = accounts[1]
    const merkleRootHash = batchData1.merkleRootHash
    const certificateHash = zeroHash
    const proof = batchData1.certificates[0].proof
    const instance = await CertNet.new()
    await instance.issueBatch(merkleRootHash, { from: issuerAddress })

    try {
      await instance.revokeSingleCertificate(certificateHash, merkleRootHash, proof, { from: issuerAddress })
      failNoErrorThrown()
    } catch (err) {
      const errorCode = getErrorCode(err)
      assert.equal(errorCode, 'INVALID_CERTIFICATE_HASH')
    }
  })

  it('revokeSingleCertificate(): Revoke certificate of non-existing batch must fail', async () => {
    const issuerAddress = accounts[1]
    const merkleRootHash = batchData1.merkleRootHash
    const certificateHash = batchData1.certificates[0].hash
    const proof = batchData1.certificates[0].proof
    const otherHash = batchData2.merkleRootHash
    const instance = await CertNet.new()
    await instance.issueBatch(merkleRootHash, { from: issuerAddress })

    try {
      await instance.revokeSingleCertificate(certificateHash, otherHash, proof, { from: issuerAddress })
      failNoErrorThrown()
    } catch (err) {
      const errorCode = getErrorCode(err)
      assert.equal(errorCode, 'BATCH_NOT_FOUND')
    }
  })

  it('revokeSingleCertificate(): Revoke certificate issued by a different issuer must fail', async () => {
    const issuerAddress = accounts[1]
    const otherIssuerAddress = accounts[2]
    const merkleRootHash = batchData1.merkleRootHash
    const certificateHash = batchData1.certificates[0].hash
    const proof = batchData1.certificates[0].proof
    const instance = await CertNet.new()
    await instance.issueBatch(merkleRootHash, { from: issuerAddress })
    try {
      await instance.revokeSingleCertificate(certificateHash, merkleRootHash, proof, { from: otherIssuerAddress })
      failNoErrorThrown()
    } catch (err) {
      const errorCode = getErrorCode(err)
      assert.equal(errorCode, 'BATCH_NOT_ISSUED_BY_CALLER')
    }
  })

  it('revokeSingleCertificate(): Revoke certificate of already-revoked batch must fail', async () => {
    const issuerAddress = accounts[1]
    const merkleRootHash = batchData1.merkleRootHash
    const certificateHash = batchData1.certificates[0].hash
    const proof = batchData1.certificates[0].proof
    const instance = await CertNet.new()
    await instance.issueBatch(merkleRootHash, { from: issuerAddress })
    await instance.revokeBatch(merkleRootHash, { from: issuerAddress })
    try {
      await instance.revokeSingleCertificate(certificateHash, merkleRootHash, proof, { from: issuerAddress })
      failNoErrorThrown()
    } catch (err) {
      const errorCode = getErrorCode(err)
      assert.equal(errorCode, 'BATCH_ALREADY_REVOKED')
    }
  })

  it('revokeSingleCertificate(): Revoke already-revoked certificate must fail', async () => {
    const issuerAddress = accounts[1]
    const merkleRootHash = batchData1.merkleRootHash
    const certificateHash = batchData1.certificates[0].hash
    const proof = batchData1.certificates[0].proof
    const instance = await CertNet.new()
    await instance.issueBatch(merkleRootHash, { from: issuerAddress })
    await instance.revokeSingleCertificate(certificateHash, merkleRootHash, proof, { from: issuerAddress })
    try {
      await instance.revokeSingleCertificate(certificateHash, merkleRootHash, proof, { from: issuerAddress })
      failNoErrorThrown()
    } catch (err) {
      const errorCode = getErrorCode(err)
      assert.equal(errorCode, 'CERTIFICATE_ALREADY_REVOKED')
    }
  })

  it('revokeSingleCertificate(): Revoke certificate with invalid proof must fail', async () => {
    const issuerAddress = accounts[1]
    const merkleRootHash = batchData1.merkleRootHash
    const certificateHash = batchData1.certificates[0].hash
    const invalidProof = zeroHash
    const instance = await CertNet.new()
    await instance.issueBatch(merkleRootHash, { from: issuerAddress })
    try {
      await instance.revokeSingleCertificate(certificateHash, merkleRootHash, invalidProof, { from: issuerAddress })
      failNoErrorThrown()
    } catch (err) {
      const errorCode = getErrorCode(err)
      assert.equal(errorCode, 'INVALID_PROOF_ARGUMENT')
    }
  })

  it('revokeSingleCertificate(): Revoke certificate with incorrect proof must fail', async () => {
    const issuerAddress = accounts[1]
    const merkleRootHash = batchData1.merkleRootHash
    const certificateHash = batchData1.certificates[0].hash
    const incorrectProof = batchData1.certificates[1].proof
    const instance = await CertNet.new()
    await instance.issueBatch(merkleRootHash, { from: issuerAddress })
    try {
      await instance.revokeSingleCertificate(certificateHash, merkleRootHash, incorrectProof, { from: issuerAddress })
      failNoErrorThrown()
    } catch (err) {
      const errorCode = getErrorCode(err)
      assert.equal(errorCode, 'INCORRECT_CERTIFICATE_PROOF')
    }
  })
})
