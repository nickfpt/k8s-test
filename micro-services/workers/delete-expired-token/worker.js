'use strict'

const cron = require('cron')
const _ = require('lodash')
const redis = require('../../../libs/redis')

async function start () {
  const cronJob = new cron.CronJob(
    '15 * * * * *',
    main
  )
  cronJob.start()
}

async function main () {
  try {
    console.log('Start delete expired token')
    const activeTokens = await redis.smembers(redis.SET.ACTIVE_TOKENS)
    const pipeline = redis.pipeline()
    activeTokens.forEach((token) => pipeline.hgetall(`${redis.HASH.VERIFICATION_TOKEN}:${token}`))
    const tokenData = await pipeline.exec()
    _.each(tokenData, ([, token], index) => {
      const { expiredTime, owner } = token
      if (new Date(expiredTime).valueOf() < new Date().valueOf()) {
        pipeline.del(`${redis.HASH.VERIFICATION_TOKEN}:${activeTokens[index]}`)
        pipeline.srem(`${redis.SET.USER}:${owner}`, activeTokens[index])
        pipeline.srem(redis.SET.ACTIVE_TOKENS, activeTokens[index])
      }
    })
    await pipeline.exec()
    console.log('Finish delete expired token')
  } catch (e) {
    console.error('Error when delete expired tokens: ', e)
  }
}

start()
