'use strict'

module.exports = {
  1: {
    QR: {
      x: 290,
      y: 1170,
      size: 6,
      margin: 3
    },
    POSITIONS: {
      1: {
        fontSize: 72,
        y: 625,
        x: 1100,
        align: 'center',
        index: 1
      },
      2: {
        fontSize: 72,
        y: 875,
        x: 1100,
        align: 'center',
        index: 2
      }
    }
  },
  2: {
    QR: {
      x: 800,
      y: 1300,
      size: 6,
      margin: 3
    },
    POSITIONS: {
      1: {
        fontSize: 40,
        x: 1750,
        y: 250,
        align: 'center',
        index: 1,
        cap: true
      },
      2: {
        fontSize: 40,
        x: 1765,
        y: 580,
        align: 'center',
        index: 2
      },
      3: {
        fontSize: 50,
        x: 1760,
        y: 780,
        align: 'center',
        index: 3
      },
      4: {
        fontSize: 37,
        x: 1665,
        y: 877,
        align: 'left',
        index: 4
      },
      5: {
        fontSize: 37,
        x: 1665,
        y: 952,
        align: 'left',
        index: 5
      },
      6: {
        fontSize: 37,
        x: 1665,
        y: 1022,
        align: 'left',
        index: 6
      },
      7: {
        fontSize: 37,
        x: 1665,
        y: 1093,
        align: 'left',
        index: 7
      },
      8: {
        fontSize: 37,
        x: 1460,
        y: 1450,
        align: 'left',
        index: 8
      },
      9: {
        fontSize: 37,
        x: 1645,
        y: 1510,
        align: 'left',
        index: 9
      },
      10: {
        fontSize: 40,
        x: 635,
        y: 250,
        align: 'center',
        index: 10,
        cap: true
      },
      11: {
        fontSize: 40,
        x: 630,
        y: 585,
        align: 'center',
        index: 11
      },
      12: {
        fontSize: 50,
        x: 610,
        y: 780,
        align: 'center',
        index: 12
      },
      13: {
        fontSize: 37,
        x: 515,
        y: 880,
        align: 'left',
        index: 13
      },
      14: {
        fontSize: 37,
        x: 515,
        y: 952,
        align: 'left',
        index: 14
      },
      15: {
        fontSize: 37,
        x: 515,
        y: 1022,
        align: 'left',
        index: 15
      },
      16: {
        fontSize: 37,
        x: 515,
        y: 1093,
        align: 'left',
        index: 16
      },
      17: {
        fontSize: 37,
        x: 355,
        y: 1510,
        align: 'left',
        index: 17
      }
    }
  },
  3: {
    QR: {
      x: 600,
      y: 2920,
      size: 10,
      margin: 2
    },
    POSITIONS: {
      1: {
        fontSize: 45,
        x: 2020,
        y: 290,
        align: 'left',
        index: 1
      },
      2: {
        fontSize: 49,
        x: 1240,
        y: 820,
        align: 'center',
        index: 2,
        cap: true
      },
      3: {
        fontSize: 49,
        x: 900,
        y: 1000,
        align: 'left',
        index: 3
      },
      4: {
        fontSize: 49,
        x: 1240,
        y: 1090,
        align: 'center',
        index: 4
      },
      5: {
        fontSize: 49,
        x: 550,
        y: 1180,
        align: 'left',
        index: 5
      },
      6: {
        fontSize: 49,
        x: 1100,
        y: 1180,
        align: 'left',
        index: 6
      },
      7: {
        fontSize: 49,
        x: 1750,
        y: 1180,
        align: 'left',
        index: 7
      },
      8: {
        fontSize: 49,
        x: 1240,
        y: 1270,
        align: 'center',
        index: 8
      },
      9: {
        fontSize: 49,
        x: 1240,
        y: 1450,
        align: 'center',
        index: 9
      },
      10: {
        fontSize: 49,
        x: 1240,
        y: 1720,
        align: 'center',
        index: 10,
        cap: true
      },
      11: {
        fontSize: 49,
        x: 600,
        y: 1810,
        align: 'center',
        index: 11
      },
      12: {
        fontSize: 49,
        x: 1100,
        y: 1810,
        align: 'left',
        index: 12
      },
      13: {
        fontSize: 49,
        x: 1750,
        y: 1810,
        align: 'left',
        index: 13
      },
      14: {
        fontSize: 49,
        x: 1240,
        y: 1900,
        align: 'center',
        index: 14
      },
      15: {
        fontSize: 49,
        x: 1240,
        y: 2080,
        align: 'center',
        index: 15,
        cap: true
      },
      16: {
        fontSize: 49,
        x: 600,
        y: 2170,
        align: 'center',
        index: 16
      },
      17: {
        fontSize: 49,
        x: 1100,
        y: 2170,
        align: 'left',
        index: 17
      },
      18: {
        fontSize: 49,
        x: 1750,
        y: 2170,
        align: 'left',
        index: 18
      },
      19: {
        fontSize: 49,
        x: 1240,
        y: 2260,
        align: 'center',
        index: 19
      },
      20: {
        fontSize: 49,
        x: 1400,
        y: 2440,
        align: 'center',
        index: 20
      },
      22: {
        fontSize: 49,
        x: 1400,
        y: 2620,
        align: 'center',
        index: 22
      },
      23: {
        fontSize: 49,
        x: 1240,
        y: 2800,
        align: 'left',
        index: 23
      }
    }
  }
}
