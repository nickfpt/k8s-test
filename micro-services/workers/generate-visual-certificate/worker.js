'use strict'

const mongoose = require('mongoose')
const amqp = require('../../../libs/amqp')
const config = require('../../../config')
const generateVisualCertificate = require('./generator')

const connectionString = `mongodb://${config.mongodb.host}:${config.mongodb.port}/certnet`
mongoose.connect(connectionString, { useNewUrlParser: true })

const queueOptions = {
  durable: true,
  autoDelete: false
}

function subscribeHelper (amqp, queueName, fn) {
  amqp.queue(queueName, queueOptions, (q) => {
    q.bind(amqp.EXCHANGE_NAME, queueName)
    q.subscribe({ ack: true, prefetchCount: 1 }, async (msg) => {
      fn(msg, () => q.shift(), (requeue) => q.shift(true, requeue))
    })
  })
}

amqp.on('ready', () => {
  subscribeHelper(amqp, amqp.QUEUE.CREATE_VISUAL_CERTIFICATE, generateVisualCertificate)
})
