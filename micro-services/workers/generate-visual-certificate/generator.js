/* eslint-disable no-unused-vars */

'use strict'

const path = require('path')
const _ = require('lodash')
const qr = require('qr-image')
const Jimp = require('jimp')
const { promisify } = require('es6-promisify')
const eachOfLimit = promisify(require('async/eachOfLimit'))
const archiver = require('archiver')
const pixelWidth = require('string-pixel-width')
const { model: { Certificate } } = require('../../../libs/mongodb')
const { generatePermanentToken } = require('../../../controllers/recipient')
const { upload } = require('../../../libs/storage')
const amqp = require('../../../libs/amqp')
const { CERTNET_EMAIL } = require('../../../constants')
const { EMAIL_TO_RECIPIENT, EMAIL_TO_ISSUER } = require('../../../constants/templates')
const TEMPLATE_CONFIG = require('./templateConfigs')

let createMailJobToRecipient = () => {}
let createMailJobToIssuer = () => {}
amqp.on('ready', () => {
  amqp.exchange(amqp.EXCHANGE_NAME, {}, (exchange) => {
    createMailJobToRecipient = (recipient, recipientName, issuerName, certName, certId, fileCert) => {
      // recipient, recipientName, issuerName, certName, certId, buffer
      console.log(`Create job send mail to recipient ${recipient}`)
      exchange.publish(
        amqp.QUEUE.EMAIL_TO_RECIPIENT,
        {
          sender: CERTNET_EMAIL,
          recipient,
          subject: `Certificate from ${issuerName}`,
          content: EMAIL_TO_RECIPIENT(certName, issuerName, certId, recipientName),
          attachment: fileCert
        }
      )
    }
    createMailJobToIssuer = (issuerEmail, issuerName, batchName, link) => {
      console.log(`Create job send mail to issuer ${issuerEmail}`)
      exchange.publish(
        amqp.QUEUE.EMAIL_TO_RECIPIENT,
        {
          sender: CERTNET_EMAIL,
          recipient: issuerEmail,
          subject: `Certificates of batch ${batchName}`,
          content: EMAIL_TO_ISSUER(issuerName, batchName, link)
        }
      )
    }
  })
})

const FONTS = {}

const writeText = async (image, font, x, y, text, fontSize, align, cap) => {
  text = cap ? text.toUpperCase() : text
  const jimFont = FONTS[font]
  if (align === 'center') {
    const textWidth = pixelWidth(text, { size: fontSize })
    await image.print(jimFont, x - (textWidth / 2), y, text)
  } else if (align === 'left') {
    await image.print(jimFont, x, y, text)
  }
  return image
}

const generate = async (certificate, map, link, templateId) => {
  try {
    [FONTS[37], FONTS[40], FONTS[45], FONTS[49], FONTS[50], FONTS[72]] = await Promise.all([
      Jimp.loadFont(`${__dirname}/fonts/37.fnt`),
      Jimp.loadFont(`${__dirname}/fonts/40.fnt`),
      Jimp.loadFont(`${__dirname}/fonts/45.fnt`),
      Jimp.loadFont(`${__dirname}/fonts/49.fnt`),
      Jimp.loadFont(`${__dirname}/fonts/50.fnt`),
      Jimp.loadFont(`${__dirname}/fonts/72.fnt`)
    ])
    const rawCertFile = path.join(__dirname, `templates/temp_${templateId}.png`)
    const qrConfig = TEMPLATE_CONFIG[templateId].QR
    const textConfig = TEMPLATE_CONFIG[templateId].POSITIONS
    const template = await Jimp.read(rawCertFile)
    const qrImage = await Jimp.read(qr.imageSync(link, {
      size: qrConfig.size,
      margin: qrConfig.margin
    }))
    const tasks = [
      template.composite(qrImage, qrConfig.x, qrConfig.y)
    ]
    Object.values(textConfig).forEach((pos) => {
      if (map[pos.index]) {
        tasks.push(writeText(template, pos.fontSize, pos.x, pos.y, _.get(certificate, map[pos.index], ''), pos.fontSize, pos.align, pos.cap))
      }
    })
    await Promise.all(tasks)
    return await template.getBufferAsync(Jimp.MIME_PNG)
  } catch (ex) {
    console.error(ex)
    return null
  }
}

const defaultMap = {
  1: 'content.recipientInfo.name',
  2: 'content.certName'
}

const generateVisualCertificate = async ({ issuerId, issuerName, issuerEmail, batchId, batchTitle, map, templateId }, ack, nack) => {
  console.log(`Receive job ${issuerId} ${batchId}`)
  try {
    // map = defaultMap
    // templateId = 1
    const archive = archiver('zip', {
      zlib: { level: 1 }
    })
    const certificates = await Certificate.find({ 'metadata.batchId': batchId })
    await eachOfLimit(certificates, 10, async (cert) => {
      cert = cert.toObject()
      const recipient = cert.content.recipientInfo.email
      if (recipient.length > 15) {
        const token = await generatePermanentToken(cert)
        const link = `https://www.certnet.site/home/${token}`
        const buffer = await generate(cert, map, link, templateId)
        // send mail
        const recipientName = cert.content.recipientInfo.name
        const { certName } = cert.content
        const issuerName = cert.content.issuerInfo.name
        const certId = cert._id
        createMailJobToRecipient(recipient, recipientName, issuerName, certName, certId, buffer)
        archive.append(buffer, { name: `${cert._id}.png` })
      }
    })
    archive.finalize()
    const { publicUrl } = await upload(archive, `/certificates/${issuerId}/${batchId}.zip`, {
      public: true
    })
    createMailJobToIssuer(issuerEmail, issuerName, batchTitle, publicUrl)
    ack()
  } catch (ex) {
    console.error(ex)
    nack()
  }
}

module.exports = generateVisualCertificate

// const map = {
//   1: 'content.truong',
//   2: 'content.nganh',
//   3: 'content.recipientInfo.name',
//   4: 'content.recipientInfo.dob',
//   5: 'content.graduationYear',
//   6: 'content.xepLoai',
//   7: 'content.hinhThucDaoTao',
//   8: 'content.certId',
//   9: 'content.graduationDecisionId',
//   10: 'content.issuerInfo.name',
//   11: 'content.major',
//   12: 'content.recipientInfo.name',
//   13: 'content.recipientInfo.dob',
//   14: 'content.graduationYear',
//   15: 'content.graduationGrade',
//   16: 'content.studyMode',
//   17: 'content.certId'
// }
// const certificate = {
//   _id: '5bf7953f1046211771307d77',
//   issuerId: '5be6da82358b0347769a5bc8',
//   metadata: {
//     isSigned: true,
//     revoked: false,
//     _id: '5bf7953f1046211771307d78',
//     batchId: '5bf795391046211771307d6e',
//     issueTime: new Date('2018-11-23T05:51:22.736Z')
//   },
//   content: {
//     _id: '5bf7953f1046211771307d79',
//     issuerInfo: {
//       id: '5be6da82358b0347769a5bc8',
//       logo: 'http://storage.googleapis.com/certnet/logo_partners/5be6da82358b0347769a5bc8',
//       name: 'FPT University',
//       ethereumAccounts: [
//         {
//           importDate: new Date('2018-11-21T08:50:07.301Z'),
//           _id: '5bf51c7ab7b97912167a7e97',
//           address: '0x28A59b5a32bF9A8f7a3Bff80Cabe758A3b2f6818'
//         }
//       ]
//     },
//     certName: 'Bachelor Degree',
//     recipientInfo: {
//       _id: '5bf7953f1046211771307d7a',
//       rollNumber: 'SE03459',
//       name: 'Hoàng Đình Quang',
//       title: 'Ông',
//       titleEnglish: 'Mr',
//       email: 'quanghdse03459@fpt.edu.vn',
//       dob: '03/03/1995',
//       ethereumAddress: '0x4ead7baca6a23d88d3389876b620ca21b0678e77',
//       pob: 'Thanh Liên, Thanh Chương, Nghệ An',
//       gender: '1',
//       nationality: 'Vietnam',
//       race: 'Kinh'
//     },
//     truong: 'Đại học FPT',
//     nganh: 'Kỹ thuật phần mềm',
//     major: 'Software Engineering',
//     graduationYear: '2018',
//     xepLoai: 'Giỏi',
//     graduationGrade: 'Good',
//     hinhThucDaoTao: 'Chính quy',
//     studyMode: 'Full-time',
//     certId: '12345',
//     graduationDecisionId: 'QD/FA2018'
//   },
//   __v: 0,
//   signature: {
//     _id: '5bf7953f1046211771307d93',
//     proof: [
//       {
//         _id: '5bf7953f1046211771307d94',
//         left: 'daeb17e4c524971f17a1852d528be2c850103da69f14f844f817c7da7fd38874'
//       }
//     ],
//     merkleRoot: '6ffd137dba4effdc02d1b21c463c32dc4ba11b66a46008aba0b11794ba3ec04a',
//     targetHash: '504bb8bb366664bd6fdefffc1bac76e7509db8833958dbc66a5739886f3285a2',
//     signedBy: '0x28A59b5a32bF9A8f7a3Bff80Cabe758A3b2f6818',
//     transactionId: '0x3a39411a5ba713a702cb9bc234eb167673fdb312288237e256c23a68d9c26799'
//   }
// }


const khaisinh = {
  _id: ('5c0cbd72346f99133bf6d708'),
  issuerId: ('5be6da82358b0347769a5bc8'),
  metadata: {
    isSigned: true,
    revoked: false,
    _id: ('5c0cbd72346f99133bf6d709'),
    batchId: ('5c0caf2bf0645c1041e87892'),
    issueTime: ('2018-12-09T07:00:27.348Z')
  },
  content: {
    _id: ('5c0cbd72346f99133bf6d70a'),
    issuerInfo: {
      id: ('5be6da82358b0347769a5bc8'),
      logo: 'http://storage.googleapis.com/certnet/logo_partners/5be6da82358b0347769a5bc8',
      name: 'Vietnam national university',
      ethereumAccounts: [
        {
          importDate: ('2018-11-21T08:50:07.301Z'),
          _id: ('5bf51c7ab7b97912167a7e97'),
          address: '0x28A59b5a32bF9A8f7a3Bff80Cabe758A3b2f6818'
        }
      ]
    },
    recipientInfo: {
      _id: ('5c0cbd72346f99133bf6d70b'),
      email: 'quanghdse03459@fpt.edu.vn',
      name: 'Hoàng Đình Quang'
    },
    certName: 'Giấy khai sinh',
    additionInfor: {
      soHieu: 'KS-231208',
      ngaySinh: '03-03-1995',
      ngaySinhBangChu: 'Ngày ba, tháng ba, năm một nghìn chín trăm chín mươi lăm',
      gioiTinh: 'Nam',
      danToc: 'Kinh',
      quocTich: 'Việt Nam',
      noiSinh: 'Xã Thanh Liên, huyện Thanh Chương, tỉnh Nghệ An',
      queQuan: 'Xã Thanh Liên, huyện Thanh Chương, tỉnh Nghệ An',
      hoTenMe: 'Trần Thị Giang',
      namSinhMe: 1970,
      danTocMe: 'Kinh',
      quocTichMe: 'Việt Nam',
      noiCuTruMe: 'Xã Thanh Liên, huyện Thanh Chương, tỉnh Nghệ An',
      hoTenBo: 'Hoàng Đình Dũng',
      namSinhBo: 1967,
      danTocBo: 'Kinh',
      quocTichBo: 'Việt Nam',
      noiCuTruBo: 'Xã Thanh Liên, huyện Thanh Chương, tỉnh Nghệ An',
      hoTenNguoiKhaiSinh: 'Trần Thị Giang',
      noiDangKyKhaiSinh: 'Công an tỉnh Nghệ An',
      ngayDangKyKhaiSinh: '10-03-1995'
    }
  },
  __v: 0,
  signature: {
    _id: ('5c0cbd73346f99133bf6d712'),
    proof: [],
    merkleRoot: 'd6655beded22e8ca0f9ef001fef13a2607ccbfcf4b4d0e191eb61096a88a3537',
    targetHash: 'd6655beded22e8ca0f9ef001fef13a2607ccbfcf4b4d0e191eb61096a88a3537',
    signedBy: '0x28A59b5a32bF9A8f7a3Bff80Cabe758A3b2f6818',
    transactionId: '0x5fdc3dcf8a2f25fc07da56dc49ad4b0430e00e111fa2124c514a897373275691'
  }
}

const map = {
  1: 'content.additionInfor.soHieu',
  2: 'content.recipientInfo.name',
  3: 'content.additionInfor.ngaySinh',
  4: 'content.additionInfor.ngaySinhBangChu',
  5: 'content.additionInfor.gioiTinh',
  6: 'content.additionInfor.danToc',
  7: 'content.additionInfor.quocTich',
  8: 'content.additionInfor.noiSinh',
  9: 'content.additionInfor.queQuan',
  10: 'content.additionInfor.hoTenMe',
  11: 'content.additionInfor.namSinhMe',
  12: 'content.additionInfor.danTocMe',
  13: 'content.additionInfor.quocTichMe',
  14: 'content.additionInfor.noiCuTruMe',
  15: 'content.additionInfor.hoTenBo',
  16: 'content.additionInfor.namSinhBo',
  17: 'content.additionInfor.danTocBo',
  18: 'content.additionInfor.quocTichBo',
  19: 'content.additionInfor.noiCuTruBo',
  20: 'content.additionInfor.hoTenNguoiKhaiSinh',
  22: 'content.additionInfor.noiDangKyKhaiSinh',
  23: 'content.additionInfor.ngayDangKyKhaiSinh'
}
// generate(khaisinh, map, 'http://www.certnet.site/home/qxxz12c7opfgqge', 3)
