'use strict'

const amqp = require('../../../libs/amqp')
const { sendEmail } = require('../../../libs/emailer')

const queueOptions = {
  durable: true,
  autoDelete: false
}

function subscribeHelper (amqp, queueName, fn) {
  amqp.queue(queueName, queueOptions, (q) => {
    q.bind(amqp.EXCHANGE_NAME, queueName)
    q.subscribe({ ack: true, prefetchCount: 1 }, async (msg) => {
      fn(msg, () => q.shift(), (requeue) => q.shift(true, requeue))
    })
  })
}

amqp.on('ready', () => {
  subscribeHelper(amqp, amqp.QUEUE.EMAIL_TO_RECIPIENT, sendEmail)
})
