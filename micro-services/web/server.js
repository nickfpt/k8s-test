'use strict'

const createError = require('http-errors')
const express = require('express')
const compression = require('compression')
const bodyParser = require('body-parser')
const expressValidator = require('express-validator')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const mongoose = require('mongoose')
const errorHandler = require('errorhandler')
const config = require('../../config')
const validateAccessToken = require('../../middlewares/validateAccessToken')

const app = express()

const database = process.env.NODE_ENV === 'test' ? 'certnet_test' : 'certnet'
const connectionString = `mongodb://${config.mongodb.host}:${config.mongodb.port}/${database}`
mongoose.connect(connectionString, { useNewUrlParser: true })

app.use(compression())
app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))
app.use(expressValidator())
app.all('/*', (req, res, next) => {
  // CORS headers
  res.header('Access-Control-Allow-Origin', '*') // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
  // Set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token')
  if (req.method === 'OPTIONS') {
    res.status(200).end()
  } else {
    next()
  }
})

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

// Authentication middleware to check if the token is valid
app.all('/api/v1/*', [validateAccessToken])
app.use('/', require('./routes'))

// Catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404))
})

/**
 * Error Handler.
 */
if (process.env.NODE_ENV === 'development' || process.env.SHOW_ERROR_TO_CLIENT === 'true') {
  app.use(errorHandler())
} else {
  app.use((err, req, res) => {
    res.status(err.status).end()
  })
}

module.exports = app
