'use strict'

const express = require('express')
const _mustBe = require('mustbe')
const Multer = require('multer')
const {
  issueManagement, templateManagement, batchManagement, requestManagement
} = require('../../../controllers/issuer')
const { getAllValidIssuers, getIndividualValidIssuer } = require('../../../controllers/partners')
const {
  generateVerifyLink, getAllTokensByUser, disableToken, getCertByEthereumAddress, getCertByUserId, getCertByCertId,
  generateVerifyLinkForGuest, updateEthereumAddresses
} = require('../../../controllers/recipient')
const { verifyToken } = require('../../../controllers/verifier')
const profile = require('../../../controllers/profile')
const admin = require('../../../controllers/admin')
const eventLogger = require('../../../controllers/eventLogger')
const auth = require('../../../controllers/auth')
const { ROLES: { ISSUER, RECIPIENT, ADMIN }, MAX_FILE_SIZE } = require('../../../constants')

_mustBe.configure(require('../../../config/mustbe-config'))

const mustBe = _mustBe.routeHelpers()
const multer = Multer({
  storage: Multer.MemoryStorage,
  limits: {
    fileSize: MAX_FILE_SIZE
  }
})

const router = express.Router()

router.get('/', async (req, res) => {
  res.json({
    status: 'It is ok!'
  })
})

// template
router.get('/api/v1/issuer/templates', mustBe.authorized(ISSUER), templateManagement.listTemplateByIssuer)
router.get('/api/v1/issuer/template/:templateId', mustBe.authorized(ISSUER), templateManagement.getTemplateById)
router.post('/api/v1/issuer/template', mustBe.authorized(ISSUER), templateManagement.createTemplate)
router.get('/api/v1/issuer/deleteTemplate/:templateId', mustBe.authorized(ISSUER), templateManagement.deleteTemplateById)
router.post('/api/v1/issuer/updateTemplate/:templateId', mustBe.authorized(ISSUER), templateManagement.updateTemplateById)

// batch
router.post('/api/v1/issuer/batch', mustBe.authorized(ISSUER), batchManagement.createBatch)
router.get('/api/v1/issuer/batches', mustBe.authorized(ISSUER), batchManagement.listBatchByIssuer)
router.get('/api/v1/issuer/batch/:batchId', mustBe.authorized(ISSUER), batchManagement.listCertByBatchId)
router.post('/api/v1/issuer/updateBatch/:batchId', mustBe.authorized(ISSUER), batchManagement.createBatch)

// issue
router.post('/api/v1/issuer/createUnsignedCert', mustBe.authorized(ISSUER), issueManagement.createUnsignedCerts)
router.get('/api/v1/issuer/buildMerkleTree/:batchId', mustBe.authorized(ISSUER), issueManagement.buildMerkleTree)
router.post('/api/v1/issuer/signBatch', mustBe.authorized(ISSUER), issueManagement.signBatch)
router.post('/api/v1/issuer/requestToBeIssuer', requestManagement.sendRequestToBeIssuer)

// revoke
router.get('/api/v1/issuer/revokeBatch/:batchId', mustBe.authorized(ISSUER), batchManagement.revokeBatch)
router.get('/api/v1/issuer/revokeCertificate/:certId', mustBe.authorized(ISSUER), batchManagement.revokeCert)
// recipient
router.post('/api/v1/recipient/generateShareLink', mustBe.authorized(RECIPIENT), generateVerifyLink)
router.post('/generateShareLinkForGuest', generateVerifyLinkForGuest)
router.get('/api/v1/recipient/getToken', mustBe.authorized(RECIPIENT), getAllTokensByUser)
router.get('/api/v1/recipient', mustBe.authorized(RECIPIENT), getCertByUserId)
router.get('/api/v1/recipient/disableToken/:token', mustBe.authorized(RECIPIENT), disableToken)
router.get('/api/v1/recipient/getCertByEthAdd/:ethAdd', mustBe.authorized(RECIPIENT), getCertByEthereumAddress)
router.get('/recipient/getCertById/:certId', getCertByCertId)
router.post('/api/v1/recipient/updateEthAccount', updateEthereumAddresses)

// authentication
router.post('/login', auth.login)
router.post('/register', auth.register)
router.get('/activate/:token', auth.activateAccount)
router.get('/api/v1/logout', auth.logout)
router.get('/recoverPassword/:email', auth.recoverPassword)

// profile
router.get('/api/v1/profile/me', profile.getCurrentUser)
router.post('/api/v1/profile/changePassword', profile.changePassword)

// admin
router.post('/api/v1/admin/users/:id', mustBe.authorized(ADMIN), admin.updateIssuerInfo)
router.get('/api/v1/admin/users', mustBe.authorized(ADMIN), admin.getAllIssuers)
router.get('/api/v1/admin/users/:id', mustBe.authorized(ADMIN), admin.getIssuerById)
router.get('/api/v1/admin/getRequestToBeIssuer', mustBe.authorized(ADMIN), requestManagement.getRequestToBeIssuer)
router.post('/api/v1/admin/updateRequestToBeIssuer/:requestId', mustBe.authorized(ADMIN), requestManagement.updateRequestToBeIssuer)
router.get('/api/v1/admin/acceptRequestToBeIssuer/:requestId', mustBe.authorized(ADMIN), requestManagement.acceptRequestToBeIssuer)
router.get('/api/v1/admin/rejectRequestToBeIssuer/:requestId', mustBe.authorized(ADMIN), requestManagement.rejectRequestToBeIssuer)
router.get('/api/v1/admin/changeIssuerStatus/:issuerId', mustBe.authorized(ADMIN), admin.changeIssuerStatus)
router.post('/api/v1/admin/uploadImage', mustBe.authorized(ADMIN), multer.single('image'), admin.uploadImage)
router.post('/api/v1/admin/addETHAddress/:issuerId', mustBe.authorized(ADMIN), admin.addEthereumAddress)
router.post('/api/v1/admin/removeETHAddress/:issuerId', mustBe.authorized(ADMIN), admin.removeEthereumAddress)
router.get('/api/v1/admin/checkIfETHAddressIsNotUsed/:address', mustBe.authorized(ADMIN), admin.checkEthereumAddressNotUsed)

// public
router.get('/issuers', getAllValidIssuers)
router.get('/issuers/:id', getIndividualValidIssuer)
router.get('/verify/:token', verifyToken)

// event loggers
router.get('/api/v1/events', eventLogger.listEvents)

module.exports = router
