FROM node:8.11.2
WORKDIR /app

COPY package.json yarn.lock /app/
RUN cd /app && yarn && yarn cache clean

COPY . /app/

EXPOSE 3001

CMD [ "node", "." ]
