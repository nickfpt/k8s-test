'use strict'

module.exports = {
  ROLES: {
    ADMIN: 'admin',
    ISSUER: 'issuer',
    RECIPIENT: 'recipient'
  },
  BECOME_ISSUER_STATUS: {
    ACCEPT: 'accept',
    REJECT: 'reject',
    PENDING: 'pending'
  },
  ETHEREUM: {
    DEFAULT_HTTP_ENDPOINT: 'https://rinkeby.infura.io/v3/d58478ba9eb5499c97715c9b3e992d63',
    DEFAULT_NETWORK_ID: '4'
  },
  GOOGLE_CLOUD_STORAGE: {
    DEFAULT_CREDENTIALS_FILE: './config/credentials/google-cloud-storage.json',
    DEFAULT_BUCKET_NAME: 'certnet'
  },
  MAILGUN: {
    DEFAULT_API_KEY: '213600b001fd03c179a48d0df6c69189-4412457b-0134f364',
    DEFAULT_DOMAIN: 'mg.certnet.site'
  },
  TOKEN_LENGTH: 15,
  MAX_EXPIRED_DAY: 14,
  CERTNET_EMAIL: 'Certnet <contact@certnet.site>',
  NO_REPLY_EMAIL: 'Certnet <noreply@certnet.site>',
  HOSTNAME: process.env.NODE_ENV === 'development' ? 'http://localhost:3001' : 'http://35.229.160.90',
  EVENT_IMPORTANCE: {
    HIGH: 3,
    MEDIUM: 2,
    LOW: 1
  },
  EVENT_OUTCOME: {
    SUCCESS: 'success',
    FAIL: 'fail'
  },
  EVENT: {
    LOGIN: 'login',
    ISSUE: 'issue',
    CREATE_TEMPLATE: 'create template',
    DELETE_TEMPLATE: 'delete template',
    UPDATE_TEMPLATE: 'update template'
  },
  ZERO_ADDRESS: '0x0000000000000000000000000000000000000000',
  ZERO_HASH: '0x0000000000000000000000000000000000000000000000000000000000000000',
  INFINITY_DURATION: 1000, // years
  JWT_TIME_TO_LIVE: 259200, // 3 days
  MAX_FILE_SIZE: 5242880 // 5MB
}
