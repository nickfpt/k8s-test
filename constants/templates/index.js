'use strict'

const EMAIL_TO_RECIPIENT = (certName, issuerName, certId, recipientName) => `
  <html lang="en">
  <head>
      <meta charset="utf-8" />
  </head>
  <body>
  <table cellspacing="0" cellpadding="0" align="center" style="background-color: #ffffff; font-family: Arial; width: 700px;">
      <tbody>
      <tr>
          <td style="background-color: #c0392b; height: 5px; width: 700px">
          </td>
      </tr>
      <td>
          <table cellspacing="0" cellpadding="0" align="center" style="border-left: 1px solid #cccccc; border-right: 1px solid #cccccc; padding: 20px; background-color: #ffffff; font-family: Arial; width: 700px;">              
              <tr>
                  <td bgcolor="#ffffff" style="padding: 20px 30px 20px 30px;">
                      <table cellspacing="0" cellpadding="0" style="background-color: #ffffff" style="">
                          <tbody>
                          <tr>
                              <td>
                                  <p>Dear ${recipientName},</p>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <p>
                                      You have just received <b>${certName}</b> certificate issued by <b>${issuerName}</b>.
                                      Please access the following link to see the certificate:
                                  </p>
                                  <a href="https://www.certnet.site/quickShare/${certId}" target="_blank">https://www.certnet.site/quickShare/${certId}</a>                                  
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <p>
                                      Best regards, <br>
                                      Certnet team.
                                  </p>
                              </td>
                          </tr>
                          </tbody>
                      </table>
                  </td>
              </tr>
          </table>
      </td>
      <tr>
          <td style="background-color: #c0392b; height: 5px; width: 700px">
          </td>
      </tr>
      </tbody>
  </table>
  </body>
  </html>
`

const REVOCATION_EMAIL_TO_RECIPIENT = (certName, issuerName, certId, recipientName) => `
  <html lang="en">
  <head>
      <meta charset="utf-8" />
  </head>
  <body>
  <table cellspacing="0" cellpadding="0" align="center" style="background-color: #ffffff; font-family: Arial; width: 700px;">
      <tbody>
      <tr>
          <td style="background-color: #c0392b; height: 5px; width: 700px">
          </td>
      </tr>
      <td>
          <table cellspacing="0" cellpadding="0" align="center" style="border-left: 1px solid #cccccc; border-right: 1px solid #cccccc; padding: 20px; background-color: #ffffff; font-family: Arial; width: 700px;">              
              <tr>
                  <td bgcolor="#ffffff" style="padding: 20px 30px 20px 30px;">
                      <table cellspacing="0" cellpadding="0" style="background-color: #ffffff" style="">
                          <tbody>
                          <tr>
                              <td>
                                  <p>Dear ${recipientName},</p>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <p>
                                      Your <b>${certName}</b> certificate issued by <b>${issuerName}</b> has been revoked by issuer.
                                      Please contact issuer for more information.
                                  </p>                                                                    
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <p>
                                      Best regards, <br>
                                      Certnet team.
                                  </p>
                              </td>
                          </tr>
                          </tbody>
                      </table>
                  </td>
              </tr>
          </table>
      </td>
      <tr>
          <td style="background-color: #c0392b; height: 5px; width: 700px">
          </td>
      </tr>
      </tbody>
  </table>
  </body>
  </html>
`

const EMAIL_ACCEPT_TO_REQUESTER = (requesterName) => `
  <html lang="en">
  <head>
      <meta charset="utf-8" />
  </head>
  <body>
  <table cellspacing="0" cellpadding="0" align="center" style="background-color: #ffffff; font-family: Arial; width: 700px;">
      <tbody>
      <tr>
          <td style="background-color: #c0392b; height: 5px; width: 700px">
          </td>
      </tr>
      <td>
          <table cellspacing="0" cellpadding="0" align="center" style="border-left: 1px solid #cccccc; border-right: 1px solid #cccccc; padding: 20px; background-color: #ffffff; font-family: Arial; width: 700px;">              
              <tr>
                  <td bgcolor="#ffffff" style="padding: 20px 30px 20px 30px;">
                      <table cellspacing="0" cellpadding="0" style="background-color: #ffffff" style="">
                          <tbody>
                          <tr>
                              <td>
                                  <p>Dear ${requesterName},</p>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <p>
                                      Your request to become an issuer of Certnet has been approved.
                                      Please login to <a href="https://www.certnet.site" target="_blank">Certnet.com</a> to start issue your certificates.
                                  </p>                                  
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <p>
                                      Best regards, <br>
                                      Certnet team.
                                  </p>
                              </td>
                          </tr>
                          </tbody>
                      </table>
                  </td>
              </tr>
          </table>
      </td>
      <tr>
          <td style="background-color: #c0392b; height: 5px; width: 700px">
          </td>
      </tr>
      </tbody>
  </table>
  </body>
  </html>
`

const EMAIL_REJECT_TO_REQUESTER = (requesterName) => `
  <html lang="en">
  <head>
      <meta charset="utf-8" />
  </head>
  <body>
  <table cellspacing="0" cellpadding="0" align="center" style="background-color: #ffffff; font-family: Arial; width: 700px;">
      <tbody>
      <tr>
          <td style="background-color: #c0392b; height: 5px; width: 700px">
          </td>
      </tr>
      <td>
          <table cellspacing="0" cellpadding="0" align="center" style="border-left: 1px solid #cccccc; border-right: 1px solid #cccccc; padding: 20px; background-color: #ffffff; font-family: Arial; width: 700px;">              
              <tr>
                  <td bgcolor="#ffffff" style="padding: 20px 30px 20px 30px;">
                      <table cellspacing="0" cellpadding="0" style="background-color: #ffffff" style="">
                          <tbody>
                          <tr>
                              <td>
                                  <p>Dear ${requesterName},</p>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <p>
                                      After carefully consideration, we are sorry to inform that
                                      your organization is not qualified to become an issuer of Certnet system.                                      
                                  </p>                                  
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <p>
                                      Best regards, <br>
                                      Certnet team.
                                  </p>
                              </td>
                          </tr>
                          </tbody>
                      </table>
                  </td>
              </tr>
          </table>
      </td>
      <tr>
          <td style="background-color: #c0392b; height: 5px; width: 700px">
          </td>
      </tr>
      </tbody>
  </table>
  </body>
  </html>
`

const EMAIL_TO_REGISTER = (link) => `
  <html lang="en">
  <head>
      <meta charset="utf-8" />
  </head>
  <body>
  <table cellspacing="0" cellpadding="0" align="center" style="background-color: #ffffff; font-family: Arial; width: 700px;">
      <tbody>
      <tr>
          <td style="background-color: #c0392b; height: 5px; width: 700px">
          </td>
      </tr>
      <td>
          <table cellspacing="0" cellpadding="0" align="center" style="border-left: 1px solid #cccccc; border-right: 1px solid #cccccc; padding: 20px; background-color: #ffffff; font-family: Arial; width: 700px;">              
              <tr>
                  <td bgcolor="#ffffff" style="padding: 20px 30px 20px 30px;">
                      <table cellspacing="0" cellpadding="0" style="background-color: #ffffff" style="">
                          <tbody>                          
                          <tr>
                              <td>
                                  <p>Your Certnet account is almost ready.</p>
                                  <p>                                  
                                      Please confirm your email address by clicking on this link:                                      
                                  </p>    
                                  <a href=${link} target="_blank">${link}</a>                              
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <p>
                                      Cheers, <br>
                                      Certnet team.
                                  </p>
                              </td>
                          </tr>
                          </tbody>
                      </table>
                  </td>
              </tr>
          </table>
      </td>
      <tr>
          <td style="background-color: #c0392b; height: 5px; width: 700px">
          </td>
      </tr>
      </tbody>
  </table>
  </body>
  </html>
`

const EMAIL_TO_ISSUER = (issuerName, batchName, link) => `
  <html lang="en">
  <head>
      <meta charset="utf-8" />
  </head>
  <body>
  <table cellspacing="0" cellpadding="0" align="center" style="background-color: #ffffff; font-family: Arial; width: 700px;">
      <tbody>
      <tr>
          <td style="background-color: #c0392b; height: 5px; width: 700px">
          </td>
      </tr>
      <td>
          <table cellspacing="0" cellpadding="0" align="center" style="border-left: 1px solid #cccccc; border-right: 1px solid #cccccc; padding: 20px; background-color: #ffffff; font-family: Arial; width: 700px;">              
              <tr>
                  <td bgcolor="#ffffff" style="padding: 20px 30px 20px 30px;">
                      <table cellspacing="0" cellpadding="0" style="background-color: #ffffff" style="">
                          <tbody>
                          <tr>
                              <td>
                                  <p>Dear ${issuerName},</p>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <p>
                                      Your issued certificates of batch ${batchName} is available at                                    
                                      <a href=${link} target="_blank">${link}</a>
                                  </p>                                  
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <p>
                                      Best regards, <br>
                                      Certnet team.
                                  </p>
                              </td>
                          </tr>
                          </tbody>
                      </table>
                  </td>
              </tr>
          </table>
      </td>
      <tr>
          <td style="background-color: #c0392b; height: 5px; width: 700px">
          </td>
      </tr>
      </tbody>
  </table>
  </body>
  </html>
`

const EMAIL_TO_RECOVER_PASSWORD = (newPassword) => `
  <html lang="en">
  <head>
      <meta charset="utf-8" />
  </head>
  <body>
  <table cellspacing="0" cellpadding="0" align="center" style="background-color: #ffffff; font-family: Arial; width: 700px;">
      <tbody>
      <tr>
          <td style="background-color: #c0392b; height: 5px; width: 700px">
          </td>
      </tr>
      <td>
          <table cellspacing="0" cellpadding="0" align="center" style="border-left: 1px solid #cccccc; border-right: 1px solid #cccccc; padding: 20px; background-color: #ffffff; font-family: Arial; width: 700px;">              
              <tr>
                  <td bgcolor="#ffffff" style="padding: 20px 30px 20px 30px;">
                      <table cellspacing="0" cellpadding="0" style="background-color: #ffffff" style="">
                          <tbody>
                          <tr>
                              <td>
                                  <p>Dear customer,</p>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <p>
                                      Here is your temporary password: <b>${newPassword}</b>.
                                 </p>                                  
                                 <p> Please login and change your password as soon as possible.</p>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <p>
                                      Best regards, <br>
                                      Certnet team.
                                  </p>
                              </td>
                          </tr>
                          </tbody>
                      </table>
                  </td>
              </tr>
          </table>
      </td>
      <tr>
          <td style="background-color: #c0392b; height: 5px; width: 700px">
          </td>
      </tr>
      </tbody>
  </table>
  </body>
  </html>
`

const ACTIVATE_ACCOUNT_TEMPLATE = {
  INVALID_TOKEN: '<b>Invalid token</b>',
  REDIRECT: '<b>Your account is ready. Redirecting ...</b><script> setTimeout(() => { window.location.replace("https://www.certnet.site"); }, 3000);</script>'
}

module.exports = {
  EMAIL_TO_RECIPIENT,
  REVOCATION_EMAIL_TO_RECIPIENT,
  EMAIL_ACCEPT_TO_REQUESTER,
  EMAIL_REJECT_TO_REQUESTER,
  EMAIL_TO_REGISTER,
  EMAIL_TO_ISSUER,
  EMAIL_TO_RECOVER_PASSWORD,
  ACTIVATE_ACCOUNT_TEMPLATE
}
