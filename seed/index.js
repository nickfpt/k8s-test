/* eslint-disable */

'use strict'

const path = require('path')
const mongoose = require('mongoose')
const { model: { Template, User, Batch, Certificate } } = require('../libs/mongodb')
const config = require('../config')

const connectionString = `mongodb://${config.mongodb.host}:${config.mongodb.port}/certnet`
mongoose.connect(connectionString, { useNewUrlParser: true })

async function main () {
  // const option = {
  //   upsert: true, new: true, setDefaultsOnInsert: true
  // }
  // const issuer = {
  //   email: 'logan@fpt.edu.vn',
  //   password: '*&(*&)*(!&))**&*&',
  //   ethereumAccounts: {
  //     address: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF'
  //   },
  //   fullname: 'FPT University',
  //   roles: ['issuer'],
  //   issuableStatus: 1,
  //   taxAccount: '120938102381',
  //   establishedDate: new Date(),
  //   ownerName: 'Truong Gia Binh',
  //   phone: '0961912745'
  // }
  // let issuerId
  // let batchId
  // let templateId
  // await User.findOneAndUpdate({}, issuer, option, (err, doc) => {
  //   issuerId = doc._id
  // })
  // const myBatch = {
  //   title: 'Fall 2018',
  //   issuerId,
  //   issueTime: new Date(),
  //   quantity: 3,
  //   tags: [],
  //   description: 'This is the batch for batch-11A students'
  // }
  // await Batch.findOneAndUpdate({}, myBatch, option, (err, doc) => {
  //   batchId = doc._id
  // })
  // const template = {
  //   issuerId,
  //   name: 'My new template',
  //   content: {
  //     'Roll number': 'content.recipientInfo.rollNumber',
  //     'Ethereum Address': 'content.recipientInfo.ethereumAddress',
  //     'Recipient Name': 'content.recipientInfo.name',
  //     DoB: 'content.recipientInfo.dob',
  //     PoB: 'content.recipientInfo.pob',
  //     Email: 'content.recipientInfo.email',
  //     Gender: 'content.recipientInfo.gender',
  //     Nationality: 'content.recipientInfo.nationality',
  //     Race: 'content.recipientInfo.race',
  //     'Decision Id': 'content.graduationDecisionId',
  //     'Cert Id': 'content.certId',
  //     'Certificate name': 'content.certName',
  //     Major: 'content.major',
  //     'Graduation Year': 'content.graduationYear',
  //     'Mode of Study': 'content.studyMode',
  //     'Graduation Grade': 'content.graduationGrade'
  //   }
  // }
  // await Template.findOneAndUpdate({}, template, option, (err, doc) => {
  //   templateId = doc._id
  // })
  // await Certificate.deleteMany({})
  // await insertBatch2MongoDb(path.join(process.env.PWD, 'seed/seed.xlsx'), templateId, issuerId, batchId)

  const recipient = new User({
    email: '1232x3@fpt.edu.vn',
    activeToken: 'blabblab',
    isActive: true,
    password: '*&(*&)*(!&))**&*&',
    ethereumAccounts: [{
      address: '0x4ead7baca6a23d88d3389876b620ca21b0678e77',
      isDefaultAccount: true
    }],
    fullname: 'Thanh',
    roles: ['recipient']
  })
  await recipient.save()
}

main()
