'use strict'

const mongoose = require('mongoose')
const { model: { User } } = require('../libs/mongodb')
const config = require('../config')

const connectionString = `mongodb://${config.mongodb.host}:${config.mongodb.port}/certnet`
mongoose.connect(connectionString, { useNewUrlParser: true })

async function main () {
  console.log('Start insert issuers')
  const issuers = [
    {
      roles: ['issuer'],
      email: 'admin@harvard.com',
      contactEmail: 'admin@harvard.com',
      password: '12345@asw46',
      ownerName: 'Nguyen Xuan Tien',
      taxAccount: '12315113123123123',
      isActive: true,
      logo: 'https://banner2.kisspng.com/20180816/jac/kisspng-logo-university-clip-art-harvard-research-corporat-test-mindkind-institute-executive-coaching-amp-5b759c5c29a8d0.0167442715344343961706.jpg',
      phone: '0961231223',
      establishedDate: new Date(),
      fullname: 'Harvard University',
      address: 'Cambridge, Massachusetts, United States',
      issuableStatus: 1,
      activeToken: '12313312',
      ethereumAccounts: [
        {
          address: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF',
          isDefaultAccount: true,
          importDate: new Date()
        }
      ],
      website: 'https://www.harvard.edu/',
      description: 'Harvard University is a private Ivy League research university in Cambridge, Massachusetts, with about 6,700 undergraduate students and about 15,250 post graduate students. Established in 1636 and named for its first benefactor, clergyman John Harvard, Harvard is the United States\' oldest institution of higher learning,[9] and its history, influence, and wealth have made it one of the world\'s most prestigious universities.'
    },
    {
      roles: ['issuer'],
      email: 'admin@tum.com',
      contactEmail: 'admin@tum.com',
      password: '12345@asw46',
      ownerName: 'Nguyen Xuan Tien',
      taxAccount: '12315113123123123',
      isActive: true,
      logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/TU_Muenchen_Logo.svg/1200px-TU_Muenchen_Logo.svg.png',
      phone: '0961231223',
      establishedDate: new Date(),
      fullname: 'Technical University of Munich',
      address: 'Arcisstraße 21, Munich, Bavaria, 80333, Germany',
      issuableStatus: 1,
      activeToken: '12313312',
      ethereumAccounts: [
        {
          address: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF',
          isDefaultAccount: true,
          importDate: new Date()
        }
      ],
      website: 'https://www.tum.de',
      description: 'Technical University of Munich[4] (TUM) (German: Technische Universität München) is a research university with campuses in Munich, Garching and Freising-Weihenstephan. It is a member of TU9, an incorporated society of the largest and most notable German institutes of technology. TUM is ranked 4th overall in Reuters 2017 European Most Innovative University ranking.'
    },
    {
      roles: ['issuer'],
      email: 'admin@michigan.com',
      contactEmail: 'admin@michigan.com',
      password: '12345@asw46',
      ownerName: 'Hoang Dinh Quang',
      taxAccount: '12315113123123123',
      isActive: true,
      logo: 'https://media2.zipcar.com/drupal-presales/files/hero_img/university-of-michigan-mobile-min.png',
      phone: '0961231223',
      establishedDate: new Date(),
      fullname: 'University of Michigan',
      address: 'Cambridge, Massachusetts, United States',
      issuableStatus: 1,
      activeToken: '12313312',
      ethereumAccounts: [
        {
          address: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF',
          isDefaultAccount: true,
          importDate: new Date()
        }
      ],
      website: 'https://www.harvard.edu/',
      description: 'Located in the city of Ann Arbor, the university is Michigan\'s oldest, having been founded in 1817 in Detroit, as the Catholepistemiad, or University of Michigania, 20 years before the territory became a state. The school was moved to Ann Arbor in 1837 onto 40 acres (16 ha) of what is now known as Central Campus. Since its establishment in Ann Arbor, the university campus has expanded to include more than 584 major buildings with a combined area of more than 34 million gross square feet (780 acres; 3.2 km2) spread out over a Central Campus and North Campus, two regional campuses in Flint and Dearborn, and a Center in Detroit. The University is a founding member of the Association of American Universities.'
    },
    {
      roles: ['issuer'],
      email: 'admin@chula.com',
      contactEmail: 'admin@chula.com',
      password: '12345@asw46',
      ownerName: 'Le Ha Phan',
      taxAccount: '12315113123123123',
      isActive: true,
      logo: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQF599ka9tPjbMTGah3ABhBqEqdtnkk37S6dIhYeDJ58pJ9af6z',
      phone: '0961231223',
      establishedDate: new Date(),
      fullname: 'Chulalongkorn University',
      address: 'Pathum Wan, Bangkok, Thailand',
      issuableStatus: 1,
      activeToken: '12313312',
      ethereumAccounts: [
        {
          address: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF',
          isDefaultAccount: true,
          importDate: new Date()
        }
      ],
      website: 'https://www.chula.ac.th/',
      description: 'Chulalongkorn University (Thai: จุฬาลงกรณ์มหาวิทยาลัย, RTGS: Chulalongkon Mahawitthayalai, pronounced [t͡ɕù.lāː.lōŋ.kɔ̄ːn mā.hǎː.wít.tʰā.jāː.lāj]), abbreviated either CU or Chula (Thai: จุฬาฯ), is a public and autonomous research university in Bangkok, Thailand.[2] The university was originally founded by King Chulalongkorn\'s reign as a school for training royal pages and civil servants in 1899 (B.E. 2442) at the Grand Palace of Thailand. It was later established as a national university in 1917, making it the oldest institute of higher education in Thailand.[3]'
    },
    {
      roles: ['issuer'],
      email: 'admin@kaist.com',
      contactEmail: 'admin@kaist.com',
      password: '12345@asw46',
      ownerName: 'Le Ha Phan',
      taxAccount: '12315113123123123',
      isActive: true,
      logo: 'http://mid.kaist.ac.kr/style/img/kaist_logo.png',
      phone: '0961231223',
      establishedDate: new Date(),
      fullname: 'Korea Advanced Institute of Science and Technology',
      address: 'Pathum Wan, Bangkok, Thailand',
      issuableStatus: 1,
      activeToken: '12313312',
      ethereumAccounts: [
        {
          address: '0x22e63d3F87b82696F293B0D3F32bdC989b2682AF',
          isDefaultAccount: true,
          importDate: new Date()
        }
      ],
      website: 'https://www.chula.ac.th/',
      description: 'KAIST (formally the Korea Advanced Institute of Science and Technology) is a public research university located in Daedeok Innopolis, Daejeon, South Korea. KAIST was established by the Korean government, with the help of American policymakers, in 1971 as the nation\'s first research-oriented science and engineering institution.[3] KAIST also has been internationally accredited in business education.[4] KAIST has approximately 10,200 full-time students and 1,140 faculty researchers and had a total budget of US$765 million in 2013, of which US$459 million was from research contracts.[1] From 1980 to 2008, the institute was known as the Korea Advanced Institute of Science and Technology. In 2008, the name was shortened to "KAIST".'
    }
  ]
  const insertTasks = []
  issuers.forEach((issuer) => {
    const newIssuer = new User(issuer)
    insertTasks.push(newIssuer.save())
  })
  await Promise.all(insertTasks)
  const recipient = new User({
    email: '1232x3@fpt.edu.vn',
    activeToken: 'blabblab',
    isActive: true,
    password: '*&(*&)*(!&))**&*&',
    ethereumAccounts: [{
      address: '0x4ead7baca6a23d88d3389876b620ca21b0678e77',
      isDefaultAccount: true
    }],
    fullname: 'Thanh',
    roles: ['recipient']
  })
  await recipient.save()
  console.log('Finish insert issuers')
}

main()
