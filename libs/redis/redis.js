'use strict'

const Redis = require('ioredis')
const config = require('../../config')

const redis = new Redis(config.redis.uri)

module.exports = Object.assign(redis, {
  HASH: {
    VERIFICATION_TOKEN: 'token'
  },
  SET: {
    USER: 'user',
    ACTIVE_TOKENS: 'active_tokens'
  }
})
