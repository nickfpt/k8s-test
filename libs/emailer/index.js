'use strict'

const Mailgun = require('mailgun-js')
const config = require('../../config')

const mailgun = new Mailgun(config.mailgun)

const sendEmail = ({
  sender, recipient, subject, content, attachment
}, ack, nack) => {
  const att = attachment ? new mailgun.Attachment({
    data: Buffer.from(attachment),
    filename: 'Certificate.png',
    contentType: 'image/png'
  }) : null
  const data = {
    from: sender,
    to: recipient,
    subject,
    html: content,
    attachment: att
  }
  mailgun.messages().send(data, (err) => {
    if (err) {
      console.error('Error when sending email: ', err)
      nack()
    } else {
      console.log(`Sent to ${recipient}: ${subject}`)
      ack()
    }
  })
}

module.exports.sendEmail = sendEmail
module.exports.mailgun = mailgun
