/* eslint-disable no-unused-vars */

'use strict'

const fs = require('fs')
const path = require('path')
const isStream = require('is-stream')
const intoStream = require('into-stream')
const { Storage, File } = require('@google-cloud/storage')
const config = require('../../config').googleCloudStorage
const { loadImage, cropImage, resizeImage, imageToBuffer } = require('../../utils/image')

const storage = new Storage()
const bucket = storage.bucket(config.bucketName)

/**
 * Get Public URL of a file on Google Cloud Storage.<br/>
 * Note: This does not check if the file exists and is public.
 * @param {string} destination The file to get URL.
 * @returns {string} The public URL.
 */
function getPublicUrl (destination) {
  if (destination.startsWith('/')) {
    destination = destination.slice(1)
  }
  return `https://storage.googleapis.com/${config.bucketName}/${destination}`
}

/**
 * Check if file exists on Google Cloud Storage.
 * @param {string} destination The file to check.
 * @returns {Promise<boolean>} True if file exists, otherwise False.
 */
async function isFileExist (destination) {
  const [exists] = await bucket.file(destination).exists()
  return exists
}

/**
 * @typedef {Object} UploadResult
 * @property {File} file The {@link File} object.
 * @property {string} publicUrl The public URL of the file, if file is set to be public.
 */

/**
 * Upload file, data or stream of data to Google Cloud Storage.
 * @param {string|Buffer|ReadableStream} source The data source.
 * @param {string} destination Destination to save file.
 * @param {Object} uploadOptions Configuration options. See {@link File.createWriteStream} for more information.
 * @param {boolean} uploadOptions.public Make the uploaded file public.
 * @returns {Promise<UploadResult>} The upload result.
 */
async function upload (source, destination, uploadOptions) {
  return new Promise((resolve, reject) => {
    try {
      let readStream
      if (isStream.readable(source)) {
        readStream = source
      } else if (source instanceof Buffer) {
        readStream = intoStream(source)
      } else if (typeof (source) === 'string') {
        readStream = fs.createReadStream(source)
      } else {
        throw new Error('\'source\' is not a file path, buffer, or readable stream.')
      }
      // If destination is a folder and source is a file path, append source file name to destination
      if (destination.endsWith('/')) {
        if (typeof (source) === 'string') {
          destination += path.basename(source)
        } else {
          throw new Error('Cannot determine full path to destination file.')
        }
      }
      const file = bucket.file(destination)
      const writeStream = file.createWriteStream(uploadOptions)
      readStream.pipe(writeStream)
        .on('error', (err) => reject(err))
        .on('finish', () => {
          resolve({
            file,
            publicUrl: uploadOptions.public ? getPublicUrl(destination) : undefined
          })
        })
    } catch (err) {
      reject(err)
    }
  })
}

/**
 * Guess MIME type of image, based on file extension.
 * @param destination Destination to save file.
 * @returns {string} The MIME type.
 */
function guessMIMEType (destination) {
  switch (path.extname(destination).toLowerCase()) {
    case '.bmp': return 'image/bmp'
    case '.jpg':
    case '.jpeg':
    case '.jpe': return 'image/jpeg'
    case '.png': return 'image/png'
    default: return undefined
  }
}

/**
 * Upload image to Google Cloud Storage.
 * @param {string|Buffer} source The image source.
 * @param {string} destination Destination to save file.
 * @param {Object} uploadOptions Configuration options. See {@link File.createWriteStream} for more information.
 * @param {boolean} uploadOptions.public Make the uploaded file public.
 * @param {Object} uploadOptions.crop The crop settings. See {@link cropImage} for more information.
 * @param {Object} uploadOptions.resize The resize settings. See {@link resizeImage} for more information.
 * @param {number} uploadOptions.quality The target quality (0 - 100) if save as JPG.
 * @param {number} uploadOptions.contentType The MIME type of the target file.
 * @returns {Promise<UploadResult>} The upload result.
 */
async function uploadImage (source, destination, uploadOptions) {
  const image = await loadImage(source)
  const toBufferOptions = {}
  if (uploadOptions) {
    if (uploadOptions.crop) {
      await cropImage(image, uploadOptions.crop)
    }
    if (uploadOptions.resize) {
      await resizeImage(image, uploadOptions.resize)
    }
    if (uploadOptions.quality) {
      toBufferOptions.quality = uploadOptions.quality
    }
    if (uploadOptions.contentType) {
      toBufferOptions.mime = uploadOptions.contentType
    } else {
      toBufferOptions.mime = guessMIMEType(destination)
    }
  }
  const buffer = await imageToBuffer(image, toBufferOptions)
  return upload(buffer, destination, uploadOptions)
}

module.exports = {
  getPublicUrl,
  isFileExist,
  upload,
  uploadImage
}
