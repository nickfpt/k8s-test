'use strict'

const amqp = require('amqp')
const config = require('../../config')

let connection
if (process.env.NODE_ENV === 'test') {
  connection = {
    on: () => {}
  }
} else {
  connection = amqp.createConnection({ url: config.rabbitmq.uri })
}

connection.on('close', () => {
  console.error('AMQP connection closed')
})

connection.on('ready', () => {
  console.log('AMQP connection ready')
})

connection.on('error', (err) => {
  console.error('Error from AMQP', err)
})

module.exports = Object.assign(connection, {
  EXCHANGE_NAME: 'certnet_amqp',
  QUEUE: {
    EMAIL_TO_RECIPIENT: 'email_to_recipient',
    CREATE_VISUAL_CERTIFICATE: 'create_visual_certificate'
  }
})
