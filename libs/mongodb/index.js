'use strict'

const User = require('./models/user')
const Batch = require('./models/batch')
const Certificate = require('./models/certificate')
const Template = require('./models/template')
const BeIssuerRequest = require('./models/beIssuerRequest')
const Event = require('./models/event')

module.exports = {
  model: {
    User,
    Batch,
    Certificate,
    Template,
    BeIssuerRequest,
    Event
  }
}
