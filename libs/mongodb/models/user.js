'use strict'

const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const { isEmail } = require('validator')
const { ROLES } = require('../../../constants')

const isIssuer = (roles) => roles.includes(ROLES.ISSUER)

const ethereumAccountSchema = new mongoose.Schema({
  address: {
    type: String,
    required: true
  },
  importDate: {
    type: Date,
    required: true,
    default: new Date()
  }
})

const ROLES_ENUM = Object.values(ROLES)

const userSchema = new mongoose.Schema({
  // only accept token verification after this date. For logout purpose, just set to NOW
  acceptTokenAfter: {
    type: Date,
    require: false
  },
  // for email confirm's purpose
  isActive: {
    type: Boolean,
    required: true
  },
  activeToken: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true,
    validate: [isEmail, 'invalid email']
  },
  password: {
    type: String,
    required: true,
    minlength: 8
  },
  roles: {
    type: [String],
    required: true,
    enum: ROLES_ENUM
  },
  // issuer
  contactEmail: {
    type: String,
    required () {
      return isIssuer(this.roles)
    },
    validate: [isEmail, 'invalid email']
  },
  ethereumAccounts: {
    type: [ethereumAccountSchema],
    required () {
      return isIssuer(this.roles)
    }
  },
  recipientEthereumAccounts: {
    type: [ethereumAccountSchema]
  },
  fullname: {
    type: String,
    required () {
      return isIssuer(this.roles)
    }
  },
  address: {
    type: String,
    required () {
      return isIssuer(this.roles)
    }
  },
  logo: {
    type: String,
    required () {
      return isIssuer(this.roles)
    }
  },
  /* 0: not issuable
     1: issuable
     2: under review (pending/ suspend)
     3: banned
   */
  issuableStatus: {
    type: Number,
    required () {
      return isIssuer(this.roles)
    },
    min: 0,
    max: 3
  },
  taxAccount: {
    type: String,
    required () {
      return isIssuer(this.roles)
    }
  },
  establishedDate: {
    type: Date,
    required () {
      return isIssuer(this.roles)
    }
  },
  ownerName: {
    type: String,
    required () {
      return isIssuer(this.roles)
    }
  },
  phone: {
    type: String,
    required () {
      return isIssuer(this.roles)
    }
  },
  website: {
    type: String,
    required () {
      return isIssuer(this.roles)
    }
  },
  description: {
    type: String,
    required () {
      return isIssuer(this.roles)
    }
  }
})

// Do not declare methods using ES6 arrow functions (=>). Arrow functions explicitly prevent binding this.

userSchema.methods.verifyPassword = async function (inputPassword) {
  return bcrypt.compare(inputPassword, this.password)
}

userSchema.methods.verifyRole = async function (roleName) {
  return this.roles.includes(roleName) && this.isActive
}

const IGNORE_ATTRIBUTES = [
  'password',
  'activeToken',
  'createdAt',
  'updateAt',
  'acceptTokenAfter',
  '__v'
]

userSchema.methods.toJSONObj = function () {
  const user = this.toObject()
  IGNORE_ATTRIBUTES.forEach((attr) => {
    delete user[attr]
  })
  return user
}

module.exports = mongoose.model('user', userSchema)
