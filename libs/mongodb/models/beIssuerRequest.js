'use strict'

const mongoose = require('mongoose')
const { isEmail } = require('validator')
const { BECOME_ISSUER_STATUS } = require('../../../constants')

const requestSchema = new mongoose.Schema({
  issuerId: {
    type: 'ObjectId',
    required: true,
    ref: 'users'
  },
  requesterEmail: {
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true,
    enum: Object.values(BECOME_ISSUER_STATUS),
    default: BECOME_ISSUER_STATUS.PENDING
  },
  contactEmail: {
    type: String,
    required: true,
    validate: [isEmail, 'invalid email']
  },
  fullname: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  logo: {
    type: String,
    required: false
  },
  taxAccount: {
    type: String,
    required: true
  },
  establishedDate: {
    type: Date,
    required: true
  },
  ownerName: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  website: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  ethereumAccount: {
    type: String,
    required: true
  }
})

module.exports = mongoose.model('beIssuerRequest', requestSchema)
