'use strict'

const mongoose = require('mongoose')

const templateSchema = new mongoose.Schema({
  issuerId: {
    type: 'ObjectId',
    ref: 'user',
    required: true
  },
  name: {
    type: String,
    required: true
  },
  content: {
    type: Map,
    required: true
  },
  description: {
    type: String,
    required: false,
    default: ''
  }
})

module.exports = mongoose.model('template', templateSchema)
