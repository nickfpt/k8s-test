'use strict'

const mongoose = require('mongoose')
const { EVENT_IMPORTANCE, EVENT_OUTCOME } = require('../../../constants')

const eventSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  actor: {
    type: 'ObjectId',
    ref: 'user'
  },
  verb: {
    type: String,
    required: true
  },
  object: {
    type: String
  },
  outcome: {
    type: String,
    enum: [EVENT_OUTCOME.SUCCESS, EVENT_OUTCOME.FAIL]
  },
  note: {
    type: String
  },
  importance: {
    type: Number,
    min: EVENT_IMPORTANCE.LOW,
    max: EVENT_IMPORTANCE.HIGH,
    default: EVENT_IMPORTANCE.LOW
  }
})

module.exports = mongoose.model('event', eventSchema)
