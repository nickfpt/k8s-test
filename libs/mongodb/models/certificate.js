'use strict'

const mongoose = require('mongoose')

const metadataSchema = new mongoose.Schema({
  batchId: {
    type: 'ObjectId',
    required: true,
    ref: 'batch'
  },
  isSigned: {
    type: Boolean,
    required: true,
    default: false
  },
  issueTime: {
    type: Date,
    required: false
  },
  revoked: {
    type: Boolean,
    required: true,
    default: false
  }
})

const signatureSchema = new mongoose.Schema({
  targetHash: {
    type: String,
    required: true
  },
  proof: {
    type: [{
      left: String,
      right: String
    }],
    required: true
  },
  transactionId: {
    type: String,
    required: true
  },
  merkleRoot: {
    type: String,
    required: true
  },
  signedBy: {
    type: String,
    required: true
  }
})

const recipientSchema = new mongoose.Schema({
  rollNumber: {
    type: String,
    required: false
  },
  ethereumAddress: {
    type: String,
    required: false
  },
  name: {
    type: String,
    required: false
  },
  // date of birth
  dob: {
    type: String,
    required: false
  },
  // place of birth
  pob: {
    type: String,
    required: false
  },
  email: {
    type: String,
    required: false
  },
  gender: {
    type: String,
    required: false
  },
  nationality: {
    type: String,
    required: false
  },
  race: {
    type: String,
    required: false
  }
})

// TODO: remove redundant fields
const badgeSchema = new mongoose.Schema({
  // So quyet dinh tot nghiep
  graduationDecisionId: {
    type: String,
    require: false
  },
  // So hieu van bang
  certId: {
    type: String,
    require: false
  },
  certName: {
    type: String,
    require: false
  },
  recipientInfo: {
    type: recipientSchema,
    require: false
  },
  major: {
    type: String,
    required: false
  },
  graduationYear: {
    type: String,
    required: false
  },
  // hinh thuc dao tao
  studyMode: {
    type: String,
    required: false
  },
  // xep loai tot nghiep
  graduationGrade: {
    type: String,
    required: false
  },
  description: {
    type: String,
    require: false
  },
  image: {
    type: String,
    require: false
  },
  issuedDate: {
    type: String,
    require: false
  },
  expiredDate: {
    type: String,
    require: false
  }
}, { strict: false })

const certSchema = new mongoose.Schema({
  issuerId: {
    type: 'ObjectId',
    ref: 'user',
    required: true
  },
  metadata: {
    type: metadataSchema,
    required: true
  },
  signature: {
    type: signatureSchema,
    required () {
      return this.metadata.isSigned
    }
  },
  content: {
    type: badgeSchema,
    required: true
  }
})

module.exports = mongoose.model('certificate', certSchema)
