'use strict'

const mongoose = require('mongoose')

const batchSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  issuerId: {
    type: 'ObjectId',
    ref: 'user',
    required: true
  },
  header: {
    type: [String],
    required: true
  },
  issueTime: {
    type: Date,
    required: false
  },
  quantity: {
    type: Number,
    required: true,
    min: 0,
    default: 0
  },
  tags: {
    type: [String],
    required: true,
    default: []
  },
  description: {
    type: String,
    require: false
  },
  isSigned: {
    type: Boolean,
    require: true,
    default: false
  },
  merkleRoot: {
    type: String,
    required: false
  },
  revoked: {
    type: Boolean,
    default: false,
    required: true
  }
})

module.exports = mongoose.model('batch', batchSchema)

