## 1. Deploy amqp
    - echo $(openssl rand -base64 32) > erlang.cookie
    - kubectl create secret generic erlang.cookie --from-file=erlang.cookie
    - kubectl create -f ./amqp/service.yaml
    - kubectl create -f ./amqp/deployment.yaml
    
## 2. Deploy send-email-worker
    - kubectl create -f ./send-email-worker/deployment.yaml

## 3. Frontend Ingress and Domain
    - kubectl apply -f devops/k8s/frontend-ingress/ingress.yaml
    - helm install stable/nginx-ingress
    - Add an A record to domain service:
        - name: www
        - value: external IP of frontend service (get this IP by running "kubectl get services" 
